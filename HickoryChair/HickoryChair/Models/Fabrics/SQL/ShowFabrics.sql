﻿Use SCFIWeb;

select *
FROM rpZFABInfo rp  
INNER JOIN CenturyWeb.dbo.Fabrics_Status fs on fs.SKU=rp.MaterialNumber and rp.SOrg=fs.SOrg 
where 1=1  
 AND rp.SOrg = 'HC01' 
 and plantlocation='HC01'
 AND rp.Grade is not null
 and rp.Status not in ('11')
 AND rp.Status not in ('DS','DR','MX','NS','PL','DX','DQ')
AND (rp.Status not in ('DN','MD') OR fs.QtyInStock >= 50)