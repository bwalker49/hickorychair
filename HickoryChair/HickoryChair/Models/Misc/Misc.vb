﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services
Imports SAP.Middleware.Connector
Imports System.Net.Mail
Imports WebSupergoo.ABCpdf9
Imports System.Web.Http

Public Class Misc

    Public Property GallerySlides As List(Of GallerySlide)
    Public Property AsShownData As String

    Public Sub New()
        GallerySlides = GetSlides()
    End Sub

    Public Sub New(ByVal img As String)
        AsShownData = ShowAsShownAbove(img)
    End Sub

    Function GetSlides() As List(Of GallerySlide)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim SlideList As New List(Of GallerySlide)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Misc/SQL/HomeGallery.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each row In tSet.Tables(0).Rows
                    Dim NewSlide As New GallerySlide

                    NewSlide.Img = row("gImgName")
                    NewSlide.description = row("image_description")

                    SlideList.Add(NewSlide)
                Next
            End If
        Catch ex As Exception
            'MiscCode.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return SlideList
    End Function

    <WebMethod()>
    Public Shared Function SendEmail(ByVal ToEmail As String, ByVal FromEmail As String, ByVal Subject As String, ByVal Body As String, ByVal isHtml As Boolean) As String
        Dim retVal As String = ""

        Dim isValid As Boolean = False

        Dim mail As New MailMessage()

        mail.From = New MailAddress(FromEmail)

        mail.To.Add(ToEmail)

        mail.Subject = Subject
        mail.Body = Body
        mail.IsBodyHtml = isHtml

        Dim mailhost As String = ConfigurationManager.AppSettings("MailHost")
        Dim smtp As New SmtpClient(mailhost, 25)
        smtp.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

        Try
            smtp.Send(mail)
            retVal = "Email Successfully Sent!"
        Catch ex As Exception
            retVal = "ERROR:" & ex.ToString()
        End Try

        Return retVal
    End Function

    Public Shared Function DataReaderMapToList(Of T)(ByVal dr As IDataReader) As List(Of T)
        Dim list As New List(Of T)
        Dim obj As T
        While dr.Read()
            obj = Activator.CreateInstance(Of T)()
            For Each prop As Reflection.PropertyInfo In obj.GetType().GetProperties()
                If HasColumn(dr, prop.Name) Then
                    prop.SetValue(obj, dr(prop.Name), Nothing)
                End If
            Next
            list.Add(obj)
        End While
        Return list
    End Function

    Public Shared Function ConvertSAPTable2ADOTable(ByVal inTable As IRfcTable) As DataTable
        Dim retval As New DataTable
        Try

            '***************************************************************************************
            'Convert the SAP IRFCTable to and ADO table that we will return from the function
            '***************************************************************************************

            '************************************
            'Setup the table columns/names
            '************************************
            For i = 0 To inTable.ElementCount - 1
                Dim md = inTable.GetElementMetadata(i)
                retval.Columns.Add(md.Name)
            Next

            '*************************************
            'Add the rows of data
            '*************************************
            Dim row As IRfcStructure
            For Each row In inTable
                Dim ldf As DataRow = retval.NewRow
                For i = 0 To inTable.ElementCount - 1
                    Dim md = inTable.GetElementMetadata(i)
                    ldf(md.Name) = row.GetString(md.Name)
                Next
                retval.Rows.Add(ldf)
            Next

        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Misc.LogError(ex)
        End Try
        Return retval
    End Function

    Public Shared Function HasColumn(Reader As Common.DbDataReader, ColumnName As String) As Boolean
        For Each row As DataRow In Reader.GetSchemaTable().Rows
            If row("ColumnName").ToString() = ColumnName Then
                Return True
            End If
        Next
        'Still here? Column not found. 
        Return False
    End Function

    Public Shared Sub LogError(ByVal ex As Exception)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()
        Try
            Dim inUserID As String = ""
            Dim WebsiteID As String = "HickoryChair"

            Dim inMsg As String = ex.StackTrace & ex.Message
            Dim inURL As String = System.Web.HttpContext.Current.Request.ServerVariables("URL") & "?" & System.Web.HttpContext.Current.Request.ServerVariables("QUERY_STRING")
            Dim errSQL As String
            errSQL = "INSERT INTO PageErrors (PageError,PageURL,UserID,OccuredOn,WebsiteID) VALUES (@PageError,@PageURL,@UserID,getDate(),@WebsiteID)"
            System.Web.HttpContext.Current.Trace.Write(errSQL)

            Dim eC As New SqlCommand(errSQL, dbConn)
            eC.Parameters.Add(New SqlParameter("@PageError", Right(inMsg, 2000)))
            eC.Parameters.Add(New SqlParameter("@PageURL", Right(inURL, 2000)))
            eC.Parameters.Add(New SqlParameter("@UserID", inUserID))
            eC.Parameters.Add(New SqlParameter("@WebsiteID", WebsiteID))
            eC.ExecuteNonQuery()


        Catch Eex As Exception
        Finally
            dbConn.Close()
        End Try
    End Sub

    '**********************************************************************************************
    'Author:    Joe Kelly
    'Date:      7-31-2007
    'Purpose:   To get a value from the Options table. This table holds configuration values 
    '           for the website such as domain, company names, contact email addresses, etc.
    '**********************************************************************************************
    Public Shared Function ReadOption(ByVal OptName As String, ByVal OptDefault As String) As String
        Dim OptValue As String = ""
        Dim sqlOpt As String = ""


        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()
        Try


            sqlOpt = "SELECT [Value] FROM Options where [Name] = '" & OptName & "' "

            Dim myCommand As New SqlCommand(sqlOpt, dbConn)

            Dim myReader As SqlDataReader
            myReader = myCommand.ExecuteReader()

            If myReader.Read() Then
                OptValue = myReader("Value")
            Else
                OptValue = OptDefault
            End If

        Catch ex As Exception
        Finally
            dbConn.Close()
        End Try

        Return OptValue
    End Function 'ReadOption

    Public Shared Function scfiReadOption(ByVal OptName As String, ByVal OptDefault As String) As String
        Dim OptValue As String
        Dim sqlOpt As String

        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
        dbConn.Open()

        sqlOpt = "SELECT [Value] FROM Options where [Name] = '" & OptName & "' "

        Dim myCommand As New SqlCommand(sqlOpt, dbConn)

        Dim myReader As SqlDataReader
        myReader = myCommand.ExecuteReader()

        If myReader.Read() Then
            OptValue = myReader("Value")
        Else
            OptValue = OptDefault
        End If

        myReader.Close()
        dbConn.Close()


        Return OptValue
    End Function

    Private Structure ShownAsData
        Dim ShotDataList As List(Of ShotData)
        Dim OverRide_Image As String
        Dim OverRide_iDesc As String
        Dim OverRide_Fabric As String
        Dim OverRide_Finish As String
    End Structure

    Private Structure ShotData
        Dim photo_ShotFileName As String
        Dim photo_SalesOrder As String
        Dim photo_SalesOrderItem As String
        Dim MaterialNumber As String
        Dim ItemName As String
        Dim ItemID As Integer
        Dim ConfigurationObject As String
        Dim iDesc As String
        Dim Characteristic As String
        Dim WebDesc As String
        Dim CharacteristicValue As String
        Dim Sequence As Integer
        Dim CurrentFabricGrade As String
        Dim CurrentFabricStatus As String

    End Structure

    Private Shared Function GetShownAsData(ByVal img As String) As ShownAsData
        '***************************************************************************************************************************
        'Dev:       Joe Kelly
        'Date:      6/2/16
        'Purpose:   There are a lot of parts and pieces of data that we need to get to build the image info. So this method
        '           puts all those pieces together in a data structure we can use.
        '****************************************************************************************************************************

        Dim AlwaysShowSAPDataForTesting As Boolean = False 'Set this to true, to ignore the over-ride data.. and always show SAP stuff.

        'Data strucures we are going to build to show the data.
        Dim ShownAsDataList As New ShownAsData
        Dim ShotDataList As New List(Of ShotData)

        'The over-ride data from prod-dev Master Image Admin
        Dim OverRideData As DataTable = GetImageOverRideData(img)

        'Add the over-ride data to our data structure
        If OverRideData.Rows.Count > 0 And Not AlwaysShowSAPDataForTesting Then
            ShownAsDataList.OverRide_Image = OverRideData(0)("Image")
            ShownAsDataList.OverRide_iDesc = OverRideData(0)("iDesc")
            ShownAsDataList.OverRide_Fabric = OverRideData(0)("iFabric")
            ShownAsDataList.OverRide_Finish = OverRideData(0)("iFinish")
        End If


        If ShownAsDataList.OverRide_Finish = "" And ShownAsDataList.OverRide_Fabric = "" Then
            'This is the photo data from photo module, sales order info, etc
            Dim PhotoSalesData As DataTable = GetPhotoSalesData(img)

            'This tells us the sap chars Maria tells us are good to show on web.
            Dim ConfigXrefData As DataTable = GetConfigDescXref2()

            'One time call gets all the data we need for all photo line items.
            Dim SAPConfigData As DataTable = GetSAPConfig(PhotoSalesData, ConfigXrefData)


            'So if we have any photo data to begin with?
            If PhotoSalesData.Rows.Count > 0 Then
                'Loop thru each item in the photo data
                For Each pr In PhotoSalesData.Rows
                    'Assign the data to our data structure
                    If SAPConfigData.Rows.Count > 0 Then
                        Dim srARR() As DataRow = SAPConfigData.Select("VBELN='" & Right("0000000000" & pr("photo_SalesOrder"), 10) & "' and POSNR='" & pr("photo_SalesOrderItem") & "'")
                        For Each sR In srARR
                            If sR("ATWRT") <> "NA" Then
                                Dim ckArr() As DataRow = ConfigXrefData.Select("SAPCharacteristic='" & sR("ATNAM") & "'")
                                If ckArr.Length > 0 Then
                                    'System.Web.HttpContext.Current.Trace.Write(sR("ATNAM") & " FOUND, so Show It")
                                    Dim sd As New ShotData
                                    sd.photo_ShotFileName = pr("photo_ShotFileName")
                                    sd.photo_SalesOrder = pr("photo_SalesOrder")
                                    sd.photo_SalesOrderItem = pr("photo_SalesOrderItem")
                                    sd.MaterialNumber = pr("MaterialNumber")
                                    sd.ItemName = pr("ItemName")
                                    sd.ConfigurationObject = pr("ConfigurationObject")
                                    sd.iDesc = pr("iDesc")
                                    sd.Characteristic = sR("ATNAM")
                                    sd.WebDesc = ckArr(0)("WebDesc")
                                    sd.Sequence = ckArr(0)("OrderBy")
                                    sd.ItemID = pr("ItemID")
                                    If sR("ATFLV") > 0 Then
                                        sd.CharacteristicValue = sR("ATFLV")
                                    ElseIf sR("ATWTB") <> "" Then
                                        sd.CharacteristicValue = sR("ATWTB")
                                    Else
                                        sd.CharacteristicValue = sR("ATWRT")
                                    End If

                                    If ckArr(0)("CharType") = "FABRIC" Then
                                        GetFabInfo(sR("ATWRT"), sd.CurrentFabricGrade, sd.CurrentFabricStatus)
                                    End If
                                    ShotDataList.Add(sd)
                                Else
                                    'System.Web.HttpContext.Current.Trace.Write(sR("ATNAM") & " Not FOUND, so don't include it")
                                End If
                            End If
                        Next

                    End If
                Next
            End If

        End If

        ' ShotDataList.Sort()
        Dim SortedList As List(Of ShotData) = ShotDataList.OrderBy(Function(c1) c1.ItemID).ThenBy(Function(c2) c2.photo_SalesOrder).ThenBy(Function(c3) c3.photo_SalesOrderItem).ThenBy(Function(c4) c4.Sequence).ToList
        ShownAsDataList.ShotDataList = SortedList




        'For Each listI In ShownAsDataList.ShotDataList
        '    System.Web.HttpContext.Current.Trace.Write(listI.photo_ShotFileName)
        '    System.Web.HttpContext.Current.Trace.Write(listI.photo_SalesOrder)
        '    System.Web.HttpContext.Current.Trace.Write(listI.photo_SalesOrderItem)
        '    System.Web.HttpContext.Current.Trace.Write(listI.MaterialNumber)
        '    System.Web.HttpContext.Current.Trace.Write(listI.iDesc)
        '    System.Web.HttpContext.Current.Trace.Write(listI.ConfigurationObject)
        '    System.Web.HttpContext.Current.Trace.Write(listI.Characteristic)
        '    System.Web.HttpContext.Current.Trace.Write(listI.Sequence)
        '    System.Web.HttpContext.Current.Trace.Write(listI.CharacteristicValue)
        '    System.Web.HttpContext.Current.Trace.Write(listI.CurrentFabricGrade)
        '    System.Web.HttpContext.Current.Trace.Write(listI.CurrentFabricStatus)

        'Next

        'System.Web.HttpContext.Current.Trace.Write(ShownAsDataList.OverRide_Image)
        'System.Web.HttpContext.Current.Trace.Write(ShownAsDataList.OverRide_iDesc)
        'System.Web.HttpContext.Current.Trace.Write(ShownAsDataList.OverRide_Fabric)
        'System.Web.HttpContext.Current.Trace.Write(ShownAsDataList.OverRide_Finish)







        Return ShownAsDataList






    End Function

    Public Shared Function ShowAsShownAbove(ByVal img As String) As String
        Dim sb As New System.Text.StringBuilder

        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim SAD As ShownAsData = GetShownAsData(img)

        'Trace.Write("testing testing 123", tSet.Tables(0).Rows.Count)

        Try

            If img <> "" Then

                '********************************************************************************************
                'This section is where they've over-ridden info from Image Description Admin (prod mgmt website
                '*************************************************************************************************
                Dim isKeyed As Boolean = False

                If SAD.OverRide_iDesc <> "" Then
                    sb.AppendLine("<div id='confDesc'>" & SAD.OverRide_iDesc & "</div>")
                End If



                If True Then ' Set this to false to test SAP vs Over-ride items
                    If SAD.OverRide_Fabric <> "" Or SAD.OverRide_Finish <> "" Then
                        isKeyed = True
                        If SAD.OverRide_Fabric <> "" Then
                            sb.AppendLine("<div>Fabric: " & SAD.OverRide_Fabric & "</div>")
                        End If
                        If SAD.OverRide_Finish <> "" Then
                            sb.AppendLine("<div>Finish: " & SAD.OverRide_Finish & "</div>")
                        End If
                    End If
                End If

                '*******************************************************************************************************
                'This section is no-over-ride. Data coming from SAP config for the market order of the image showing.
                '********************************************************************************************************
                If SAD.ShotDataList.Count > 0 And isKeyed = False Then
                    'System.Web.HttpContext.Current.Trace.Write("Have some ShownAsListes")
                    Dim i As Integer = 0
                    Dim attr As String = "noClass"
                    Dim lastSKU As String = ""

                    sb.AppendLine("<div class='configWrapper'>")

                    For Each row In SAD.ShotDataList
                        If row.MaterialNumber <> lastSKU Then
                            If i <> 0 Then
                                sb.AppendLine("<div style='padding-top:15px;'>")
                            End If

                            sb.AppendLine("<div class='skuChange''><u><strong>" & row.MaterialNumber & " - " & row.ItemName & "</strong></u></div>")
                        End If

                        If row.CurrentFabricGrade <> "" Then
                            sb.AppendLine("<div>")
                            If row.CurrentFabricStatus = "A" Or row.CurrentFabricStatus = "HA" Or row.CurrentFabricStatus = "DN" Or row.CurrentFabricStatus = "DS" Then
                                sb.AppendLine("<span class='asaDesc'>" & row.WebDesc & ": </span><span class='asaVal'>" & row.CharacteristicValue & "</span>")
                                sb.AppendLine("<span class='asaDesc'> Grade:</span><span class='asaVal'>" & row.CurrentFabricGrade & "</span>")
                            Else
                                sb.AppendLine("<span class='asaDesc'>" & row.WebDesc & ": </span><span class='asaVal'>" & row.CharacteristicValue & "</span>")
                                sb.AppendLine("<span><b>*Discontinued*</b></span>")
                            End If
                            sb.AppendLine("</div>")
                        Else
                            sb.AppendLine("<div><span class='asaDesc'>" & row.WebDesc & ": </span><span class='asaVal'>" & row.CharacteristicValue & "</span></div>")
                        End If
                        i += 1
                        lastSKU = row.MaterialNumber
                    Next
                    sb.AppendLine("</div>")
                End If
            End If


        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return sb.ToString
    End Function

    Private Shared Function GetSAPConfig(ByVal PhotoSalesData As DataTable, ByVal ConfigXrefData As DataTable) As DataTable
        '**********************************************************************************************************
        'Dev:       Joe Kelly
        'Date:      6/2/16
        'Purpose:   For the images, we tag them with a sales order / line in the photo module. So we want to look in 
        '           SAP and get the configuration details of the product that's in that image. This method
        '           allows us to do that by passing in the configuration object as a key 
        '***********************************************************************************************************
        Dim retVal As New DataTable


        Dim SAP_SYS As RfcDestination

        Try
            SAP_SYS = RfcDestinationManager.GetDestination("PRD")
            '*************************************************************
            'Set the name of the RFC in SAP we are going to execute.
            '**************************************************************
            Dim dfAPI As IRfcFunction = SAP_SYS.Repository.CreateFunction("Z_WEB_GET_ITEM_CONFIG")

            Dim IST_SO_DETAILS As IRfcTable = dfAPI.GetTable("IST_SO_DETAILS")
            For Each pR In PhotoSalesData.Rows
                System.Web.HttpContext.Current.Trace.Write("Adding->" & pR("photo_SalesOrder"))
                IST_SO_DETAILS.Append()
                IST_SO_DETAILS.SetValue("VBELN", Right("0000000000" & pR("photo_SalesOrder"), 10))
                IST_SO_DETAILS.SetValue("POSNR", pR("photo_SalesOrderItem"))
            Next
            Dim IST_CHARS As IRfcTable = dfAPI.GetTable("IST_CHARS")
            For Each cR In ConfigXrefData.Rows
                ' System.Web.HttpContext.Current.Trace.Write("Adding->" & pR("photo_SalesOrder"))
                IST_CHARS.Append()
                IST_CHARS.SetValue("ATNAM", cR("SAPCharacteristic"))
            Next








            System.Web.HttpContext.Current.Trace.Write("Before SAP Call")
            dfAPI.Invoke(SAP_SYS)
            System.Web.HttpContext.Current.Trace.Write("After SAP Call")

            Dim IST_VARIANT_CHARS As IRfcTable = dfAPI.GetTable("IST_VARIANT_CHARS")

            retVal = ConvertSAPTable2ADOTable(IST_VARIANT_CHARS)

            System.Web.HttpContext.Current.Trace.Write("Num SAP Data Rows: " & retVal.Rows.Count)

        Catch rfcCommEx As RfcCommunicationException
            System.Web.HttpContext.Current.Trace.Write(rfcCommEx.StackTrace)
            System.Web.HttpContext.Current.Trace.Write(rfcCommEx.Message)
        Catch rfcLogonEx As RfcLogonException
            System.Web.HttpContext.Current.Trace.Write(rfcLogonEx.StackTrace)
            System.Web.HttpContext.Current.Trace.Write(rfcLogonEx.Message)
        Catch rfcAbapRuntimeEx As RfcAbapRuntimeException
            System.Web.HttpContext.Current.Trace.Write(rfcAbapRuntimeEx.StackTrace)
            System.Web.HttpContext.Current.Trace.Write(rfcAbapRuntimeEx.Message)
        Catch rfcAbapBaseEx As RfcAbapBaseException
            If rfcAbapBaseEx.Message.ToString = String.Empty Then
                Dim tEx As SAP.Middleware.Connector.RfcAbapClassicException = CType(rfcAbapBaseEx, SAP.Middleware.Connector.RfcAbapClassicException)
                System.Web.HttpContext.Current.Trace.Write(tEx.Key.ToString())
            Else
                System.Web.HttpContext.Current.Trace.Write(rfcAbapBaseEx.Message.ToString())
            End If
        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.GetType().ToString)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            LogError(ex)
        End Try

        Return retVal
    End Function

    Private Shared Function GetImageOverRideData(ByVal img As String) As DataTable
        '***********************************************************************************************************************
        'Dev:       Joe Kelly
        'Date:      6/2/16
        'Purpose:   The product developers have the ability to over-ride what shows for an image, such as a top or a close up
        '           of the hardware.. even if it's tied to a sales order. They do this from the ProdDev-> Image Description Admin page.
        '           So we need to get that data here... and we'll over-ride what we show if it's populated.
        '******************************************************************************************************************************
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As New DataTable

        'All good data in production.. change back later.
        Dim ProdDevPrefix As String = "ProdMgmtWeb"
        Dim SCFIPrefix As String = "SCFIWeb"
        Dim CentPrefix As String = "CenturyWeb"

        Try

            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT   ")
            sb.AppendLine("id.Image, ")
            sb.AppendLine("isnull(iD.[Desc],'') as iDesc,   ")
            sb.AppendLine("isnull(iD.Fabric,'') as iFabric,  ")
            sb.AppendLine("isnull(iD.Finish,'') as iFinish  ")
            sb.AppendLine("FROM  " & CentPrefix & ".dbo.Image_Details iD   ")
            sb.AppendLine("where Image=@photo; ")

            Dim tC As New SqlDataAdapter(sb.ToString, dbConn)
            tC.SelectCommand.Parameters.Add(New SqlParameter("@photo", img))

            tC.Fill(retVal)

            System.Web.HttpContext.Current.Trace.Write("over-ride", retVal.Rows.Count)

        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            LogError(ex)
        Finally
            dbConn.Close()
        End Try
        Return retVal
    End Function

    Private Shared Function GetPhotoSalesData(ByVal img As String) As DataTable
        '**************************************************************************************************************
        'Dev:       Joe Kelly
        'Date:      6/2/16
        'Purpose:   Take the image name and look in the photo module tables and the order tables and get the info
        '           we need to show the config details for that image.
        '**************************************************************************************************************
        Dim retVal As New DataTable

        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        'All good data in production.. change back later.
        Dim ProdDevPrefix As String = "ProdMgmtWeb"
        Dim SCFIPrefix As String = "SCFIWeb"
        Dim CentPrefix As String = "CenturyWeb"

        Try
            Dim tb As New System.Text.StringBuilder
            tb.AppendLine("select   ")
            tb.AppendLine("	photo_ShotFileName,  ")
            tb.AppendLine("	photo_SalesOrder,  ")
            tb.AppendLine("	photo_SalesOrderItem,  ")
            tb.AppendLine(" oi.MaterialNumber,  ")
            tb.AppendLine(" isnull(it.Name,'') as ItemName, ")
            tb.AppendLine("	oi.ConfigurationObject,  ")
            tb.AppendLine("	isnull(id.[Desc],'') as iDesc, ")
            tb.AppendLine(" i.photo_ItemID as ItemID ")
            tb.AppendLine("FROM " & ProdDevPrefix & ".dbo.photo_ProjectShots proj  ")
            tb.AppendLine("INNER JOIN  " & ProdDevPrefix & ".dbo.photo_ShotItems i on proj.photo_ShotID=i.photo_ShotID  ")
            tb.AppendLine("INNER JOIN " & SCFIPrefix & ".dbo.OrderItems oi on oi.SalesOrderNumber=photo_SalesOrder and oi.SalesOrderItem=photo_SalesOrderItem  ")
            tb.AppendLine("INNER JOIN " & CentPrefix & ".dbo.Items it on it.SKU = oi.MaterialNumber ")
            tb.AppendLine("LEFT JOIN " & CentPrefix & ".dbo.Image_Details id on id.Image = photo_ShotFileName  ")
            tb.AppendLine("where photo_ShotFileName=@photo and proj.photo_ShotIsDeleted = 0 ")
            tb.AppendLine("order by i.photo_ItemID")
            Dim tA As New SqlDataAdapter(tb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@photo", img)
            tA.Fill(retVal)


        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return retVal
    End Function

    Private Shared Function GetConfigDescXref2() As DataTable
        '*************************************************************************************************************************
        'Dev:       Joe Kelly
        'Date:      6/2/16
        'Purpose:   This is a table that Maria usually admins. She places the SAPCharacteristics that need to show for an image.
        '           We only show the ones defined in this table. She also details whether the char is appropriate to show on the
        '           dealer vs consumer websites. The CharType tells us whether it's a fabric or finish char.. so we can do
        '           some further lookups to link to other info.
        '**************************************************************************************************************************
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As New DataTable

        'All good data in production.. change back later.
        Dim ProdDevPrefix As String = "ProdMgmtWeb"
        Dim SCFIPrefix As String = "SCFIWeb"
        Dim CentPrefix As String = "CenturyWeb"

        'System.Web.HttpContext.Current.Trace.Write("WebShowType=" & WebShowType)

        Try
            Dim tb As New System.Text.StringBuilder
            tb.AppendLine("select  ")
            tb.AppendLine("	SAPCharacteristic, ")
            tb.AppendLine("	WebDesc, ")
            tb.AppendLine("	isnull(CharType,'') as CharType, ")
            tb.AppendLine("	ShowConsumer, ")
            tb.AppendLine("	ShowDealer, ")
            tb.AppendLine(" OrderBy")
            tb.AppendLine("FROM " & ProdDevPrefix & ".dbo.photo_ConfigDescXRef2 ")
            tb.AppendLine("Where  ")
            tb.AppendLine("	ShowConsumer=1 ")

            Dim tA As New SqlDataAdapter(tb.ToString, dbConn)
            tA.Fill(retVal)
        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            LogError(ex)
        Finally
            dbConn.Close()
        End Try
        Return retVal
    End Function

    Private Shared Sub GetFabInfo(ByVal inSKU As String, ByRef CurrentFabricGrade As String, ByRef CurrentFabricStatus As String)
        '****************************************************************************************************************************
        'Dev:       Joe Kelly
        'Date:      6/2/16
        'Purpose:   When one of the characteristics is CharType=Fabric, then we know we can look up in the tables below to
        '           see what it's current status and Grade is. The fabric grade can often CHANGE between the time the market order
        '           is produced, and now. Also if the fabric is disco'd, then we want to point that out.
        '*****************************************************************************************************************************
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        'All good data in production.. change back later.
        Dim ProdDevPrefix As String = "ProdMgmtWeb"
        Dim SCFIPrefix As String = "SCFIWeb"
        Dim CentPrefix As String = "CenturyWeb"

        Try
            Dim tb As New System.Text.StringBuilder
            tb.AppendLine("select	sku, ")
            tb.AppendLine("		isnull(Status,'') as Status, ")
            tb.AppendLine("		isnull(Grade,'') as Grade ")
            tb.AppendLine("FROM " & CentPrefix & ".dbo.Fabrics_Status ")
            tb.AppendLine("where SKU=@sku ")
            tb.AppendLine("AND SOrg = 'SCFI' ")
            Dim tC As New SqlCommand(tb.ToString, dbConn)
            tC.Parameters.AddWithValue("@sku", inSKU)
            Dim tR As SqlDataReader
            tR = tC.ExecuteReader
            While tR.Read
                CurrentFabricGrade = tR("Grade")
                CurrentFabricStatus = tR("Status")
            End While

        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            LogError(ex)
        Finally
            dbConn.Close()
        End Try
    End Sub

    'Public Shared Function ProcessPDF2Thumbnail(ByVal FilePath As String, ByVal FileName As String, ByVal DPI As String) As ActionResult
    '    '*********************************************************************************************
    '    'Author:    Joe Kelly
    '    'Date:      1/28/2013
    '    'Purpose:   Take a PDF and return a png thumbnail of the first page.
    '    '*********************************************************************************************
    '    Dim retStream As New MemoryStream
    '    Try
    '        Dim pdfPath As String = FilePath & FileName
    '        Dim theDoc As Doc = New Doc()
    '        theDoc.Read(System.Web.HttpContext.Current.Server.MapPath(pdfPath))
    '        theDoc.Rendering.DotsPerInch = DPI
    '        theDoc.PageNumber = 1
    '        theDoc.Rect.String = theDoc.CropBox.String
    '        theDoc.Rendering.Save("test.jpg", retStream)
    '        theDoc.Dispose()
    '    Catch ex As Exception
    '        Misc.LogError(ex)
    '    End Try
    '    Return File(retStream.ToArray, "image/jpeg")
    'End Function

    Public Class GallerySlide
        Public Property Img As String
        Public Property description As String
    End Class
End Class
