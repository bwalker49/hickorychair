﻿--Declare @UserLat as decimal(18,7)
--Declare @UserLong as decimal(18,7)
--Declare @Distance as decimal(18,0)
--Declare @MaxDealers as int

--Set @UserLat=36.0092311
--Set @UserLong = -81.5596427
--Set @Distance=250
--Set @MaxDealers=10


SELECT top (@MaxDealers)
	d.AccountName
    ,d.Address1
	,isnull(d.Address2, '') AS Address2
    ,d.City
    ,d.State
    ,d.Zip
    ,d.Telephone
	,d.google_Lat as Latitude
	,d.google_Lng as Longitude
	,power(((power((69.1 * (google_Lat - @UserLat)),2)) + (power((69.1 * (google_Lng - @UserLong) * Cos(@UserLat / 57.3)),2))),0.5) as DistFrom 
	,www
	,HC_Showroom
	,HC_Trade_Showroom
	,HC_Boutique
	,HCC_University

from Dealers d

WHERE 
	power(((power((69.1 * (google_Lat - @UserLat)),2)) + (power((69.1 * (google_Lng - @UserLong) * Cos(@userLat / 57.3)),2))),0.5) <  @Distance
	and d.websiteID='HickoryChair'
	and d.ShowOnWebLocator=1
	and d.IsDeleted=0
	and d.google_Lat is not null
	and d.google_lng is not null
	ORDER BY century_showroom DESC