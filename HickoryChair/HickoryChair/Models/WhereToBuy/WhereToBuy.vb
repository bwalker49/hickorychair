﻿
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Web.Mvc
Imports System.Web.Mvc.Ajax
Imports System.Xml

Namespace Models.WhereToBuy
    Public Class WhereToBuy

        Public Property Zip As String
        Public Property Radius As String
        Public Property UserLatitude As Decimal
        Public Property UserLongitude As Decimal

        Public Property MapRequestAO As AjaxOptions
            Get
                Dim opts = New AjaxOptions()
                opts.HttpMethod = "POST"
                opts.UpdateTargetId = "WhereToBuyMap"
                opts.OnSuccess = "CreateDealerMapObjects"
                opts.OnBegin = "return StartValidation();"
                Return opts
            End Get
            Set(value As AjaxOptions)

            End Set
        End Property

        Public Property DealerLocationList As List(Of DealerLocation) = New List(Of DealerLocation)()
        Public Property ApiKey As String = ConfigurationManager.AppSettings("GoogleMapsKey")

        Public Sub GetSearchResults()
            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()


            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT DISTINCT Zip FROM HCWeb.dbo.Showrooms")

            Dim sA As New SqlDataAdapter(sb.ToString, dbConn)
            sA.SelectCommand.Parameters.AddWithValue("@zip", System.Web.HttpContext.Current.Request("Zip"))

            Dim zipSet As New DataSet
            sA.Fill(zipSet)

            'If zipSet.Tables(0).Rows.Count > 0 Then
            '    'HttpContext.Current.Response.Redirect("http://www.google.com", True)
            '    RedirectPage(zipSet.Tables(0).Rows(0).Item("ID"))
            'Else

            Dim zipList As String = ""

            For Each shwrmzip In zipSet.Tables(0).Rows
                zipList = zipList & shwrmzip("Zip") & ","
            Next

            Dim zip As String

            If System.Web.HttpContext.Current.Request("Zip") <> "" Then
                zip = System.Web.HttpContext.Current.Request("Zip")
            Else
                zip = Me.Zip
            End If

            GetLatLong(zip, Me.UserLatitude, Me.UserLongitude)



            Dim pSQL As String = IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/WhereToBuy/GetDealersByAddress.sql"))
            Dim tA As New SqlDataAdapter(pSQL, dbConn)

            tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = UserLatitude, .ParameterName = "@UserLat", .SqlDbType = SqlDbType.Float})
            tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = UserLongitude, .ParameterName = "@UserLong", .SqlDbType = SqlDbType.Float})
            tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = Radius, .ParameterName = "@Distance", .SqlDbType = SqlDbType.Float})
            tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = 25, .ParameterName = "@MaxDealers", .SqlDbType = SqlDbType.Int})

            Dim ds As New DataTable
            tA.Fill(ds)

            For Each item As DataRow In ds.Rows
                Dim dl = New DealerLocation()
                dl.Address = item("Address1")
                dl.Address2 = item("Address2")
                dl.City = item("City")
                dl.Company = item("AccountName")
                dl.DistFrom = item("DistFrom")
                dl.Latitude = item("Latitude")
                dl.Longitude = item("Longitude")
                dl.Phone = item("Telephone")
                dl.State = item("State")
                dl.Zip = item("Zip")
                dl.ShowroomZipList = zipList
                dl.IsShowroom = item("HC_Showroom")
                dl.IsTradeShowroom = item("HC_Trade_Showroom")
                dl.IsBoutique = item("HC_Boutique")
                dl.IsHCCCertified = item("HCC_University")
                dl.URL = item("www")
                Me.DealerLocationList.Add(dl)
            Next
            'End If
        End Sub

        <WebMethod()>
        Public Shared Function GetShowroomID(ByVal zip As String) As String
            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()


            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT ID FROM HCWeb.dbo.Showrooms WHERE Zip = @zip")

            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@zip", zip)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            Dim id As String = tSet.Tables(0).Rows(0).Item("ID")

            Return id

        End Function

        Public Property RadiusSelections As List(Of SelectListItem) = New List(Of SelectListItem)({
                                New SelectListItem With {.Text = "10 miles", .Value = "10"},
                                New SelectListItem With {.Text = "25 miles", .Value = "25", .Selected = True},
                                New SelectListItem With {.Text = "50 miles", .Value = "50"},
                                New SelectListItem With {.Text = "100 miles", .Value = "100"}})

        Private Sub GetLatLong(ByVal inAddress As String, ByRef outLat As Decimal, ByRef outLong As Decimal)
            Try
                Dim webClient As New Net.WebClient
                Dim result As String = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/xml?address=" & inAddress & "&key=" + ApiKey)

                Dim xmlDoc As New XmlDocument()
                xmlDoc.LoadXml(result)
                Dim m_nodelist As XmlNodeList

                m_nodelist = xmlDoc.SelectNodes("/GeocodeResponse/result/geometry/location/lat")
                If m_nodelist.Count > 0 Then
                    'Utility.Runlog(Utility.SubName,m_nodelist(0).InnerText)
                    outLat = m_nodelist(0).InnerText
                End If

                m_nodelist = xmlDoc.SelectNodes("/GeocodeResponse/result/geometry/location/lng")
                If m_nodelist.Count > 0 Then
                    'Utility.Runlog(Utility.SubName,m_nodelist(0).InnerText)
                    outLong = m_nodelist(0).InnerText
                End If

            Catch ex As Exception

            Finally

            End Try
        End Sub

        Public Class DealerLocation
            Public Property Company As String
            Public Property Address As String
            Public Property Address2 As String
            Public Property City As String
            Public Property State As String
            Public Property Zip As String
            Public Property ShowroomZipList As String
            Public Property IsShowroom As Boolean
            Public Property IsTradeShowroom As Boolean
            Public Property IsHCCCertified As Boolean
            Public Property IsBoutique As Boolean
            Public Property Phone As String
            Public Property DistFrom As Decimal
            Public Property Latitude As Decimal
            Public Property Longitude As Decimal
            Public Property URL As String

        End Class



    End Class

End Namespace


