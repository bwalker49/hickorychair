﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Web.Services.WebService
Imports System.ComponentModel.DataAnnotations


Namespace Models
    Public Class FabricDetail


        Public Property FDetail As FabItem





        Public Sub New(ByVal SKU As String)
            FDetail = GetFabDetails(SKU)
        End Sub

        Private Function GetFabDetails(ByVal SKU As String) As FabItem
            Dim retVal As New FabItem

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()
            Try
                Dim likeSKU As String = SKU.Remove(SKU.Length - 2, 2)
                likeSKU = likeSKU.Insert(likeSKU.Length, "__")

                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("Select  ")
                tb.AppendLine("	isnull(fs.SKU,'') as SKU,  ")
                tb.AppendLine(" isnull(zf.MaterialDescription, '') as Name,")
                tb.AppendLine("	isnull(fs.Status,'') as Status,  ")
                tb.AppendLine("	isnull(fs.QtyInStock,0) as QtyInStock,  ")
                tb.AppendLine("    isnull(stk.NextATPQty,0) as NextAtpQty,  ")
                tb.AppendLine("    stk.AvailDate,  ")
                tb.AppendLine("	isnull(fs.UOM,'') as UOM,  ")
                tb.AppendLine("    isnull(fs.WholesaleCost,0) as Wholesale,  ")
                tb.AppendLine("    isnull(fs.RetailPrice,0) as Retail,  ")
                tb.AppendLine("	isnull(zf.PlantLocation,'') as PlantLocation,  ")
                tb.AppendLine("	isnull(zf.Grade,'') as Grade,  ")
                tb.AppendLine(" isnull(c.NewGrade,'') as NewGrade, ")
                tb.AppendLine(" isnull(c.EffectiveDate,'') as EffectiveDate, ")
                tb.AppendLine("	isnull(zf.WidthOfRoll,0) as WidthOfRoll,  ")
                tb.AppendLine("	isnull(zf.HorizontalRepeat,0) as HorizontalRepeat,  ")
                tb.AppendLine("	isnull(zf.VerticalRepeat,0) as VerticalRepeat,  ")
                tb.AppendLine("	isnull(zf.CleaningCode,'') as CleaningCode,  ")
                tb.AppendLine("	isnull(zf.Orientation,'') as Orientation,  ")
                tb.AppendLine("	isnull(zf.FabricFinish,'') as FabricFinish,  ")
                tb.AppendLine("    isnull(zf.FabricFinishDesc,'') as FabricFinishDesc,  ")
                tb.AppendLine("    isnull(zf.FabricRestrictions,'') as FabricRestrictions,  ")
                tb.AppendLine("    isnull(zf.Exclusivity,'') as Exclusivity,  ")
                tb.AppendLine("    isnull(zf.DurabilityCode,'') as DurabilityCode,  ")
                tb.AppendLine("    isnulL(zf.ABCPricingCode,'') as ABCPricingCode,  ")
                tb.AppendLine("    isnull(zf.ColorNumber,'') as ColorNumber,  ")
                tb.AppendLine("    isnull(zf.ColorNumberDescription,'') as ColorNumberDescription,  ")
                tb.AppendLine("    isnull(zf.ColorGroup,'') as ColorGroup,  ")
                tb.AppendLine("    isnull(zf.Class,'') as Class,  ")
                tb.AppendLine("    isnull(zf.FringeHeight,'') as FringeHeight,  ")
                tb.AppendLine("    isnull(zf.ColorGroupDescription,'') as ColorGroupDescription,  ")
                tb.AppendLine("    isnull(zf.MPG, '') as MPG,  ")
                tb.AppendLine("    isnull(zf.MPG, '') as MPG,  ")
                tb.AppendLine(" isnull(zf.PerformanceCode,'') as PerformanceCode, ")
                tb.AppendLine(" isnull(zf.PerformanceDesc,'') as PerformanceDesc, ")
                tb.AppendLine(" isnull(zf.DesignerCode, '') as DesignerCode, ")
                tb.AppendLine(" isnull(zf.DesignerDesc, '') as DesignerDesc,")
                tb.AppendLine("    isnull(zf.DesignerDesc,'') as DesignerDesc, ")
                tb.AppendLine("    isnull(zf.DoubleRubs,0.00) as DoubleRubs")
                tb.AppendLine("FROM CenturyWeb.dbo.Fabrics_Status fs  ")
                tb.AppendLine("LEFT JOIN rpZFABinfo zf on  zf.MaterialNumber = fs.SKU and zf.SOrg = fs.SOrg ")
                tb.AppendLine("LEFT JOIN CenturyWeb.dbo.Items_StockDate stk on fs.sku=stk.sku  ")
                tb.AppendLine(" LEFT JOIN CenturyWeb.dbo.Fabrics_Grade_Changes c on c.SKU=fs.SKU ")
                tb.AppendLine("where  ")
                tb.AppendLine("	replace(fs.SKU,' ','')=replace(@SKU,' ','')  ")
                tb.AppendLine(" AND fs.SOrg = 'HC01' ")


                'Fiber content data.
                tb.AppendLine("Select  ")
                tb.AppendLine("	isnull(fiber,'') as fiber,  ")
                tb.AppendLine("	isnull(FiberPercent,'') as FiberPercent,  ")
                tb.AppendLine("	isnull(FiberDescription,'') as FiberDescription  ")
                tb.AppendLine("FROM rpZFABfabricXref zxref  ")
                tb.AppendLine("where  ")
                tb.AppendLine("	zxref.MaterialNumber=@SKU; ")

                'The warehouse stock data
                tb.AppendLine(" SELECT  Distinct  ")
                tb.AppendLine("	WarehouseNumber, ")
                tb.AppendLine("	StorageBin, ")
                tb.AppendLine("	Storage, ")
                tb.AppendLine("	AvailStock, ")
                tb.AppendLine("	Unit  ")
                tb.AppendLine("FROM  rpFabricInventory   ")
                tb.AppendLine("where  ")
                tb.AppendLine("    MaterialNumber=@SKU; ")

                'Other colorways data
                tb.AppendLine("Select  ")
                tb.AppendLine("	isnull(fs.SKU,'') as SKU  ")
                tb.AppendLine("FROM CenturyWeb.dbo.Fabrics_Status fs  ")
                tb.AppendLine("where fs.SKU like @likeSKU ")
                tb.AppendLine("and fs.Status not in ('DS','DR','MX','NS','PL','DX','11')  ")
                tb.AppendLine("and fs.SKU <> @SKU ")

                tb.AppendLine(" and fs.SOrg = 'HC01' ")
                Dim tA As New SqlDataAdapter(tb.ToString, dbConn)
                tA.SelectCommand.Parameters.AddWithValue("@SKU", SKU)
                tA.SelectCommand.Parameters.AddWithValue("@likeSKU", likeSKU)
                Dim tSet As New DataSet
                tA.Fill(tSet)



                If Not IsNothing(tSet.Tables(0)) Then
                    If tSet.Tables(0).Rows.Count > 0 Then
                        Dim tr As DataRow = tSet.Tables(0)(0)
                        retVal.SKU = tr("SKU")
                        retVal.Name = tr("Name")
                        If System.IO.File.Exists(HttpContext.Current.Server.MapPath("/Content/prod-images/" & tr("SKU") & ".jpg")) Then
                            retVal.FabImage = "/Content/prod-images/" & tr("SKU") & ".jpg"
                            retVal.FabImageHiRes = "/Content/prod-images/" & tr("SKU") & "_hires.jpg"
                        Else
                            retVal.FabImage = "/content/prod-images/noimage_medium.jpg"
                            retVal.FabImageHiRes = "/content/prod-images/noimage.jpg"
                        End If

                        retVal.Status = CalcStatus(tr("Status"), tr("QtyInStock"))
                        retVal.QtyInStock = tr("QtyInStock")
                        retVal.NextATPQty = tr("NextATPQty")
                        If Not IsDBNull(tr("AvailDate")) Then
                            retVal.AvailDate = tr("AvailDate")
                        End If
                        retVal.UOM = tr("UOM")
                        retVal.Wholesale = tr("Wholesale")
                        retVal.Retail = tr("Retail")
                        retVal.PlantLocation = tr("PlantLocation")
                        retVal.FabricDivision = calcDiv(tr("PlantLocation"))
                        retVal.Grade = tr("Grade")
                        retVal.NewGrade = tr("NewGrade")
                        retVal.NewGradeDate = tr("EffectiveDate")
                        retVal.WidthOfRoll = tr("WidthOfRoll")
                        retVal.HorizontalRepeat = tr("HorizontalRepeat")
                        retVal.VerticalRepeat = tr("VerticalRepeat")
                        retVal.CleaningCode = tr("CleaningCode")
                        retVal.Orientation = tr("Orientation")
                        retVal.FabricFinish = tr("FabricFinish")
                        retVal.FabricFinishDesc = tr("FabricFinishDesc")
                        retVal.FabricRestrictions = tr("FabricRestrictions")
                        retVal.Exclusivity = tr("Exclusivity")
                        retVal.FringeHeight = tr("FringeHeight")
                        retVal.MPG = tr("MPG")
                        retVal.FabClass = tr("Class")
                        retVal.ColorGroup = tr("ColorGroup")
                        retVal.ColorGroupDescription = tr("ColorGroupDescription")
                        retVal.ColorNumber = tr("ColorNumber")
                        retVal.ColorNumberDescription = tr("ColorNumberDescription")
                        retVal.Durability = CalcDurability(tr("DurabilityCode"))
                        retVal.PerformanceCode = tr("PerformanceCode")
                        retVal.PerformanceDescription = tr("PerformanceDesc")
                        retVal.DesignerCode = tr("DesignerCode")
                        retVal.DesignerDesc = Replace(tr("DesignerDesc"), "Construction", "")
                        retVal.DoubleRubs = tr("DoubleRubs")
                    End If
                End If

                If Not IsNothing(tSet.Tables(1)) Then
                    For Each fR In tSet.Tables(1).Rows
                        Dim fC As New FabContent
                        fC.Fiber = fR("Fiber")
                        fC.FiberDescription = fR("FiberDescription")
                        fC.FiberPercent = fR("FiberPercent")
                        retVal.FabContent.Add(fC)
                    Next
                End If

                If Not IsNothing(tSet.Tables(2)) Then
                    For Each fsR In tSet.Tables(2).Rows
                        Dim fs As New FabStock
                        fs.WarehouseNumber = fsR("WarehouseNumber")
                        fs.StorageBin = fsR("StorageBin")
                        fs.Storage = fsR("Storage")
                        fs.AvailStock = fsR("AvailStock")
                        fs.Unit = fsR("Unit")
                        retVal.FabStock.Add(fs)
                    Next
                End If

                If Not IsNothing(tSet.Tables(3)) Then
                    For Each cR In tSet.Tables(3).Rows
                        Dim imgPath As String = "/Content/prod-images/" & cR("SKU") & "_small.jpg"
                        If System.IO.File.Exists(HttpContext.Current.Server.MapPath(imgPath)) Then
                            Dim cw As New ColorWay
                            cw.SKU = cR("SKU")
                            cw.ColorWayImage = imgPath
                            retVal.ColorWays.Add(cw)
                        End If

                    Next
                End If

                retVal.reportStatusDate = Misc.ReadOption("FabricStockDbImportTimeStamp", "")

            Catch ex As Exception
                Misc.LogError(ex)
            Finally
                dbConn.Close()
            End Try
            Return retVal
        End Function



        Public Class FabItem
            Property SKU As String
            Property Name As String
            Property Status As String
            Property QtyInStock As Decimal
            Property NextATPQty As Decimal
            Property AvailDate As DateTime
            Property UOM As String
            Property Wholesale As String
            Property Retail As String
            Property PlantLocation As String
            Property Grade As String
            Property NewGrade As String
            Property NewGradeDate As String
            Property WidthOfRoll As String
            Property HorizontalRepeat As String
            Property VerticalRepeat As String
            Property CleaningCode As String
            Property Orientation As String
            Property FabricFinish As String
            Property FabricFinishDesc As String
            Property FabricRestrictions As String
            Property Exclusivity As String
            Property FringeHeight As String
            Property MPG As String
            Property ColorGroup As String
            Property ColorGroupDescription As String
            Property ColorNumber As String
            Property ColorNumberDescription As String
            Property Durability As String
            Property PerformanceCode As String
            Property PerformanceDescription As String
            Property DesignerCode As String
            Property DesignerDesc As String
            Property DoubleRubs As Decimal
            Property FabImage As String
            Property FabClass As String
            Property FabImageHiRes As String
            Property FabricDivision As String
            Property reportStatusDate As String

            Property FabContent As New List(Of FabContent)

            Property FabStock As New List(Of FabStock)

            Property ColorWays As New List(Of ColorWay)



        End Class

        Public Class FabContent
            Property Fiber As String
            Property FiberPercent As String
            Property FiberDescription As String

        End Class

        Public Class FabStock
            Property WarehouseNumber As String
            Property StorageBin As String
            Property Storage As String
            Property AvailStock As String
            Property Unit As String

        End Class

        Public Class ColorWay
            Property SKU As String
            Property ColorWayImage As String

        End Class

        Private Function CalcDurability(ByVal DurabilityCode As String) As String
            Dim retVal As String = ""
            Try
                If DurabilityCode = "H" Then
                    retVal = "Durability: HIGH"
                ElseIf DurabilityCode = "M" Then
                    retVal = "Durability: MEDIUM"
                ElseIf DurabilityCode = "L" Then
                    retVal = "Durability: LOW"
                End If
            Catch ex As Exception
                Misc.LogError(ex)
            End Try
            Return retVal
        End Function

        Private Function CalcStatus(ByVal inStatus As String, ByVal inStockQty As String) As String
            Dim retVal As String = ""
            Try
                If inStatus = "A" Or inStatus = "DN" Or inStatus = "NS" Then
                    retVal = "Active"
                    If inStatus = "NS" Then
                        retVal &= "(NOT SWATCHED)"
                    End If
                ElseIf inStatus = "MD" And inStockQty > 0 Then
                    retVal = "Limited Qtys Available"
                Else
                    retVal = "Discontinued"
                End If
            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retVal

        End Function

        Private Function calcDiv(ByVal inDiv As String) As String
            Dim retVal As String
            If inDiv = "P002" Then
                retVal = "UPH"
            ElseIf inDiv = "P005" Then
                retVal = "SHUFORD"
            ElseIf inDiv = "P007" Then
                retVal = "LTD"
            Else
                retVal = inDiv
            End If

            Return retVal
        End Function



    End Class


    Public Class FabCart

        Public Sub New(SKU)
            Me.FabCartList = GetFabCart()
            Me.SKU = SKU
        End Sub
        Private m_FabCartList As List(Of String)
        Public Property FabCartList As List(Of String)
            Get
                If IsNothing(m_FabCartList) Then
                    m_FabCartList = GetFabCart()
                End If
                Return m_FabCartList
            End Get
            Set(value As List(Of String))
                m_FabCartList = value
            End Set
        End Property

        Private m_SKU As String
        Public Property SKU As String
            Get
                Return m_SKU
            End Get
            Set(value As String)
                m_SKU = value
            End Set
        End Property


        <WebMethod()>
        Public Shared Function GetFabCart() As List(Of String)
            Dim retVal As New List(Of String)
            Try

                Dim myFabCart As String = HttpContext.Current.Profile.GetPropertyValue("FabCart")

                If Not myFabCart Is Nothing Then
                    If myFabCart.Trim <> "" Then
                        Dim fStr As String = myFabCart.ToString
                        Dim f() As String = fStr.Split(",")
                        retVal = f.ToList
                        'Sometimes it puts ,xxxx when removing. Need to loop thru and delete any entries that are blank
                        For Each fItem In retVal
                            If fItem.Trim = "" Then
                                retVal.Remove(fItem)
                            End If
                        Next
                    End If
                End If


            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retVal
        End Function


        Public Shared Function AddFabToCart(ByVal SKU As String) As String
            Dim retVal As String = ""

            Try


                Dim myFabCart As String = HttpContext.Current.Profile.GetPropertyValue("FabCart")

                Dim fabList As New List(Of String)
                If Not myFabCart Is Nothing Then

                    fabList = myFabCart.Split(",").ToList
                End If

                fabList.Add(SKU)

                Dim tmpList As String = String.Join(",", fabList)

                HttpContext.Current.Profile.SetPropertyValue("FabCart", tmpList)

                Dim tmpFabCart As String = HttpContext.Current.Profile.GetPropertyValue("FabCart")

                retVal = "SUCCESS"
            Catch ex As Exception
                Misc.LogError(ex)
            End Try
            Return retVal
        End Function

        Public Shared Sub ClearFabCart()

            Try
                HttpContext.Current.Profile.SetPropertyValue("FabCart", Nothing)


            Catch ex As Exception
                Misc.LogError(ex)
            End Try


        End Sub


        Public Shared Function RemoveFromFabCart(ByVal SKU As String) As String
            Dim retVal As String = ""

            Try

                Dim myFabCart As String = HttpContext.Current.Profile.GetPropertyValue("FabCart")

                Dim fabList As New List(Of String)
                If Not myFabCart Is Nothing Then
                    fabList = myFabCart.Split(",").ToList
                End If

                fabList.Remove(SKU)

                If fabList.Count > 0 Then
                    Dim tmpList As String = String.Join(",", fabList)
                    HttpContext.Current.Profile.SetPropertyValue("FabCart", tmpList)
                Else
                    HttpContext.Current.Profile.SetPropertyValue("FabCart", "")
                End If

                retVal = "SUCCESS"
            Catch ex As Exception
                Misc.LogError(ex)
            End Try
            Return retVal
        End Function





    End Class

    Public Class FabCartView

        Private m_FabViewList As List(Of ViewFabs)
        Property FabViewList As List(Of ViewFabs)
            Get
                If m_FabViewList Is Nothing Then
                    Dim fabSKUs As List(Of String) = Models.FabCart.GetFabCart()
                    m_FabViewList = GetFabViewData(fabSKUs)
                End If
                Return m_FabViewList
            End Get
            Set(value As List(Of ViewFabs))
                m_FabViewList = value
            End Set
        End Property

        Public Class ViewFabs
            Property SKU As String
            Property FabImage As String
            Property Status As String
            Property QtyInStock As String
            Property UOM As String
            Property MPG As String
            Property MPG_Description As String

        End Class

        Private Shared Function GetFabViewData(ByVal fabSKUs As List(Of String)) As List(Of ViewFabs)
            Dim retVal As New List(Of ViewFabs)
            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("scfi").ConnectionString.ToString)
            dbConn.Open()

            Try
                If fabSKUs.Count > 0 Then
                    Dim skuList As String = ""
                    For Each s In fabSKUs
                        If skuList <> "" Then
                            skuList &= ","
                        End If
                        skuList &= "'" & s & "'"

                    Next
                    Dim tb As New System.Text.StringBuilder
                    tb.AppendLine("select  ")
                    tb.AppendLine("	fs.SKU, ")
                    tb.AppendLine("	fs.Status, ")
                    tb.AppendLine("	fs.QtyInStock, ")
                    tb.AppendLine(" isnull(fs.UOM,'') as UOM, ")
                    tb.AppendLine("	fs.CollectionNo as MPG, ")
                    tb.AppendLine("	isnull(mpg.MPG_Description,'') as MPG_Description ")
                    tb.AppendLine("FROM CenturyWeb.dbo.Fabrics_Status fs   ")
                    tb.AppendLine("LEFT JOIN rpZFABinfo zf on  zf.MaterialNumber = fs.SKU and zf.SOrg = 'SCFI'  ")
                    tb.AppendLine("LEFT JOIN CenturyWeb.dbo.Items_StockDate stk on fs.sku=stk.sku   ")
                    tb.AppendLine("LEFT JOIN SISWeb2.dbo.Matl_Pricing_Group mpg on mpg.Matl_Pricing_Group=fs.CollectionNo ")
                    tb.AppendLine("where   ")
                    tb.AppendLine("	fs.SKU in (" & skuList & ")  ")
                    tb.AppendLine(" AND fs.SOrg = 'SCFI' ")
                    Dim tA As New SqlDataAdapter(tb.ToString, dbConn)
                    Dim tSet As New DataSet
                    tA.Fill(tSet)
                    For Each tr In tSet.Tables(0).Rows
                        Dim vf As New ViewFabs
                        If System.IO.File.Exists(HttpContext.Current.Server.MapPath("/Content/prod-images/" & tr("SKU") & "_small.jpg")) Then
                            vf.FabImage = "/Content/prod-images/" & tr("SKU") & "_small.jpg"
                        Else
                            vf.FabImage = "/content/prod-images/noimage_small.jpg"
                        End If
                        vf.SKU = tr("SKU")
                        vf.Status = tr("Status")
                        vf.QtyInStock = tr("QtyInStock")
                        vf.UOM = tr("UOM")
                        vf.MPG = tr("MPG")
                        vf.MPG_Description = tr("MPG_Description")
                        retVal.Add(vf)
                    Next

                End If

            Catch ex As Exception
                Misc.LogError(ex)
            Finally
                dbConn.Close()
            End Try

            Return retVal
        End Function
    End Class



    'Public Class FabRequest
    '    Inherits FabCartView

    '    Private m_UserInfo As FabRequestUserInfo
    '    Property UserInfo As FabRequestUserInfo
    '        Get
    '            If m_UserInfo Is Nothing Then
    '                Dim tUserInfo As FabRequestUserInfo = GetUserInfo()
    '                m_UserInfo = tUserInfo
    '            End If
    '            Return m_UserInfo
    '        End Get
    '        Set(value As FabRequestUserInfo)
    '            m_UserInfo = value
    '        End Set
    '    End Property

    '    'Private Function GetUserInfo() As FabRequestUserInfo
    '    '    Dim retVal As New FabRequestUserInfo

    '    '    Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("scfi").ConnectionString.ToString)
    '    '    dbConn.Open()

    '    '    Try
    '    '        Dim SQL As String = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/RHF/FabricSearch/SQL/FabUserInfo.sql"))
    '    '        Dim tA As New SqlDataAdapter(SQL, dbConn)

    '    '        tA.SelectCommand.Parameters.AddWithValue("@UserID", tUser.UserId)
    '    '        tA.SelectCommand.Parameters.AddWithValue("@WebsiteID", tUser.RHFWebsiteID)

    '    '        Dim tSet As New DataSet
    '    '        tA.Fill(tSet)

    '    '        If tSet.Tables.Count = 2 Then
    '    '            If tSet.Tables(0).Rows.Count > 0 Then
    '    '                Dim repR As DataRow = tSet.Tables(0).Rows(0)
    '    '                retVal.CustomerName = repR("CustomerName")
    '    '                retVal.CustomerAccountNumber = repR("CustomerAccountNumber")
    '    '                retVal.RepAccountNumber = repR("RepAccountNumber")
    '    '                retVal.SalesDistrictName = repR("SalesDistrictName")
    '    '                retVal.RepEmail = repR("RepEmail")
    '    '            End If
    '    '            If tSet.Tables(0).Rows.Count > 0 Then
    '    '                Dim uR As DataRow = tSet.Tables(1).Rows(0)
    '    '                retVal.UserID = uR("UserID")
    '    '                retVal.FirstName = uR("FirstName")
    '    '                retVal.LastName = uR("LastName")
    '    '                retVal.PhoneNumber = uR("PhoneNumber")
    '    '                retVal.CompanyName = uR("CompanyName")
    '    '                retVal.Address1 = uR("Address1")
    '    '                retVal.Address2 = uR("Address2")
    '    '                retVal.City = uR("City")
    '    '                retVal.State = uR("State")
    '    '                retVal.Zip = uR("Zip")
    '    '                retVal.EmailAddress = uR("EmailAddress")
    '    '            End If
    '    '        End If



    '    '    Catch ex As Exception
    '    '        Misc.LogError(ex)
    '    '    Finally
    '    '        dbConn.Close()
    '    '    End Try

    '    '    Return retVal
    '    'End Function



    '    Public Class FabRequestUserInfo
    '        Property CustomerName As String
    '        Property CustomerAccountNumber As String
    '        Property RepAccountNumber As String
    '        Property SalesDistrictName As String
    '        Property RepEmail As String
    '        Property UserID As String
    '        <DisplayName("First Name")>
    '        Property FirstName As String
    '        <DisplayName("Last Name")>
    '        Property LastName As String
    '        <DisplayName("Phone Number")>
    '        Property PhoneNumber As String
    '        <DisplayName("Company Name")>
    '        Property CompanyName As String
    '        <DisplayName("Address 1")>
    '        Property Address1 As String
    '        <DisplayName("Address 2")>
    '        Property Address2 As String
    '        <DisplayName("City")>
    '        Property City As String
    '        <DisplayName("State")>
    '        Property State As String
    '        <DisplayName("Zip")>
    '        Property Zip As String

    '        <Required(ErrorMessage:="Email Address is Required")>
    '        <DisplayName("Email")>
    '        Property EmailAddress As String
    '    End Class
    'End Class
End Namespace
