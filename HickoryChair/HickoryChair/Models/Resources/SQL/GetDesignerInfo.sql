﻿SELECT 
DesignerName,
DesignerBio,
isnull((SELECT TOP 1 CASE WHEN ExternalLink IS NOT NULL THEN ExternalLink ELSE CatalogFile END AS Catalog FROM CenturyWeb.dbo.Catalogs WHERE CatalogType = 'Designer' And Designer = DesignerID AND WebsiteID = 'HickoryChair'), '') AS Catalog,
isnull((SELECT TOP 1 CatalogFile FROM CenturyWeb.dbo.Catalogs WHERE CatalogType = 'Brandbook' AND Designer = DesignerID AND WebsiteID = 'HickoryChair'), '') AS Brandbook
FROM Designers
WHERE CompanyCode = 'HickoryChair'
AND DesignerID = @Designer