﻿--Declare @SKU as varchar(50) = 'HC115-51'


--Keep everything below here
Declare @COM as varchar(50) 
Declare @COL as varchar(50) 
 
Set @COM = (select isnull(COMYardage,'') from Items where sku=@SKU) 
Set @COL = (select isnull(Leather,'') FROM Items where sku=@SKU) 

select * FROM ( 
select  
	fs.sku, 
	fs.QtyInStock, 
	fs.Status, 
	sum(sis.INVOICED_QUANTITY) as Total_Inv_Qty, 
	z.CreatedDate, 
	z.Grade, 
	z.ClassDescription, 
	rank() 
	over (partition by z.ClassDescription 
	      order by sum(sis.INVOICED_QUANTITY) desc) as Rank 
FROM CenturyWeb.dbo.Fabrics_Status fs 
INNER JOIN SISWeb2.dbo.SIS_S815 sis on sis.Material_ID=fs.SKU 
INNER JOIN SCFIWeb.dbo.rpZFABInfo z on z.materialnumber=fs.sku and z.SOrg=fs.SOrg
where  
	fs.Status in ('CA','A') 
	and sis.SIS_WEEK > '200101' 
	and z.ClassDescription is not null 
 and fs.SOrg = 'HC01' 
 and z.SOrg = 'HC01' 
 and PlantLocation='HC01'
	and  ( 
			(@COM <> '' and @COL <> '' AND MatlGroup in  ('MG17','MG54') ) 
			OR 
			(@COM <> '' and MatlGroup in ('MG17') ) 
			OR 
			(@COL <> '' and MatlGroup in ('MG54') ) 
		 ) 
Group By 
	fs.sku, 
	fs.QtyInStock, 
	fs.Status, 
	z.CreatedDate, 
	z.Grade, 
	z.ClassDescription) a 
	where a.Rank <= 10 
ORDER BY  
Case WHEN ClassDescription = 'Plain' THEN '1' 
WHEN classDescription = 'Velvets' THEN '2' 
WHEN classDescription = 'Leather' THEN '3' 
Else ClassDescription End, 
    Total_Inv_Qty desc 