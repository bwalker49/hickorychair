﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

Public Class Designer
    Public Property DesignerDetails As New DesignerInfo

    Public Sub New(ByVal DesignerID As String)
        DesignerDetails = BuildDesignerInfo(DesignerID)
    End Sub

    Public Function BuildDesignerInfo(ByVal DesignerID As String) As DesignerInfo
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("HickoryWeb").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As New DesignerInfo

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Resources/SQL/GetDesignerInfo.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@Designer", DesignerID)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                retVal.DesignerID = DesignerID
                retVal.DesignerName = tSet.Tables(0).Rows(0).Item("DesignerName")
                retVal.DesignerBio = tSet.Tables(0).Rows(0).Item("DesignerBio")
                If tSet.Tables(0).Rows(0).Item("Catalog") <> "" Then
                    If tSet.Tables(0).Rows(0).Item("Catalog").ToString().Contains("http") Then
                        retVal.Catalog = tSet.Tables(0).Rows(0).Item("Catalog")
                    Else
                        retVal.Catalog = "/Documents/Catalogs/" & tSet.Tables(0).Rows(0).Item("Catalog")
                    End If

                Else
                    retVal.Catalog = ""
                End If
                If tSet.Tables(0).Rows(0).Item("Brandbook") <> "" Then
                    retVal.Brandbook = "/Documents/Brandbooks/" & tSet.Tables(0).Rows(0).Item("Brandbook")
                Else
                    retVal.Brandbook = ""
                End If

            End If
        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return retVal

    End Function

    Public Class DesignerInfo
        Public Property DesignerID As String
        Public Property DesignerName As String
        'Public Property DesignerImg As String
        Public Property DesignerBio As String
        'Public Property Collection As String
        Public Property Catalog As String
        Public Property Brandbook As String
    End Class
End Class
