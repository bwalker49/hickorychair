﻿Imports System
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports System.Configuration
Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.IO
Imports SAP.Middleware.Connector


Namespace Models
    Public Class Finishes

        Public Shared WebsiteID As String = "HickoryChair"

        Public Property FinishList As List(Of FinishData)

        Public Sub New()
            FinishList = GetAllFinishes()
        End Sub

        Public Shared Function ShowAllAvailableFinishesResponsive() As String
            '*********************************************************************************************************************
            'Dev:   Joe Kelly (8/3/2017)
            '       This method shows the full finish list for the finish list page. Not filtered based on sku, coll, etc.
            '*********************************************************************************************************************
            Dim retVal As String = ""

            Try
                Dim dataList As List(Of FinishData) = GetAllFinishes()
                retVal = OutputFinishList(dataList)
            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retVal
        End Function

        Private Shared Function GetAllFinishes() As List(Of FinishData)
            '*********************************************************************************************************************
            'Dev:   Joe Kelly (8/3/2017)
            '       Gets the data for all finishes
            '*********************************************************************************************************************
            Dim retVal As New List(Of FinishData)

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
            dbConn.Open()

            Try
                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("Use ProdMgmtWeb;")
                tb.AppendLine("select  ")
                tb.AppendLine("	f.FinishID, ")
                tb.AppendLine("	f.FinishDesc, ")
                tb.AppendLine("	g.GroupID, ")
                tb.AppendLine("	g.GroupName, ")
                tb.AppendLine(" isnull(g.GroupNote,'') as GroupNote ")
                tb.AppendLine("FROM icAvailableFinishes f ")
                tb.AppendLine("INNER JOIN icAvailableFinishes_Website_Xref w on w.FinishID=f.FinishID ")
                tb.AppendLine("INNER JOIN icAvailableFinishes_Groups g on g.GroupID=w.GroupID and w.WebsiteID=g.WebsiteID ")
                tb.AppendLine("WHERE ")
                tb.AppendLine("	f.ShowOnWeb=1 ")
                tb.AppendLine("	and w.WebsiteID=@WebsiteID ")
                tb.AppendLine("ORDER BY ")
                tb.AppendLine("	g.OrderBy, ")
                tb.AppendLine("	f.FinishDesc ")
                Dim sqlCmd As New SqlCommand(tb.ToString, dbConn)
                sqlCmd.Parameters.AddWithValue("@WebsiteID", WebsiteID)
                Dim dR As SqlDataReader
                dR = sqlCmd.ExecuteReader

                retVal = Misc.DataReaderMapToList(Of FinishData)(dR)


            Catch ex As Exception
                Misc.LogError(ex)
            Finally
                dbConn.Close()
            End Try

            Return retVal
        End Function

        Private Shared Function OutputFinishList(ByVal DataList As List(Of FinishData)) As String
            '*******************************************************************************************************************
            'Dev:   Joe Kelly (8/3/2017)
            '       Condensed the output code to this one method for finishes shown in full list, or at the item page.
            '*******************************************************************************************************************
            Dim retVal As String = ""
            Try
                Dim sb As New System.Text.StringBuilder
                Dim lastGrp = ""
                If DataList.Count > 0 Then
                    sb.AppendLine("<p>")
                    sb.AppendLine("Finish panels shown are only to give an approximate representation of the color of the finish")
                    sb.AppendLine("but do not reflect the type or grain of wood used in the actual product.")
                    sb.AppendLine("Please refer to the item image or your local retailer for that information. ")
                    sb.AppendLine("Finishes ordered at different times are not guaranteed to match.")
                    sb.AppendLine("Each piece of Hickory Chair's fine wood furniture is finished by hand, involving many ")
                    sb.AppendLine("steps. Woods have grain and color variations due to growth patterns of each tree. ")
                    sb.AppendLine("The result is a beautiful and unique piece. Due to these wood variations, finishes ")
                    sb.AppendLine("on individual pieces are not guaranteed to match.")
                    sb.AppendLine("On this website, we strive to provide an accurate representation of our finishes; however, due to changes in lighting, ")
                    sb.AppendLine("monitors, limitations of photography, and actual wood used for product - color variation from the actual finish sample")
                    sb.AppendLine("is possible. Not all finishes are available on every item. ")
                    sb.AppendLine("</p>")

                    sb.AppendLine("<ul>")
                    For Each dr In DataList
                        If lastGrp <> dr.GroupID Then
                            sb.AppendLine("</section>")
                            sb.AppendLine("<section class='finish-group'>")
                            sb.AppendLine("<h3>" & dr.GroupName & "</h3>")
                            If dr.GroupNote <> "" Then
                                sb.AppendLine(dr.GroupNote)
                            End If
                            sb.AppendLine("<hr/>")
                        End If
                        Dim imageToShow As String
                        Dim noImage As String = "noImage_small.jpg"
                        Dim imagePath As String

                        Dim finImg As String = dr.FinishID
                        imagePath = "/Content/finish-images/"
                        imageToShow = finImg & "_small.jpg"

                        If Not System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(imagePath & imageToShow)) Then
                            imageToShow = noImage
                        End If

                        sb.AppendLine("<li class='finish-item'>")
                        sb.AppendLine("<a href='/Resources/FinishDetail/" & dr.FinishID & "' style='text-decoration:none;'>")
                        sb.AppendLine("<img src='" & imagePath & imageToShow & "' border='0' alt='Click to view details' title='Click to view details' style='border-radius:5%;' />")
                        sb.Append("<span><strong>" & dr.FinishDesc & "</strong></span>")

                        sb.AppendLine("</a>")
                        sb.AppendLine("</li>")

                        lastGrp = dr.GroupID

                    Next
                    sb.AppendLine("</ul>")
                    sb.AppendLine("</section>")

                End If

                retVal = sb.ToString
            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retVal
        End Function


        Public Shared Function ShowFinishesInTableResponsive(ByVal inSKU As String, ByVal inColl As String, ByVal inCollType As String) As String
            '*****************************************************************************************************************************************
            'Dev:   Joe Kelly (8/3/2017)
            '       This shows the finish list at the sku level. It looks first to see if finishes have been setup at the item level
            '       then at the collection level, then finally it just shows the generic full list.
            '*****************************************************************************************************************************************
            Dim retval As String = ""

            Try
                Dim SKUData As SkuFinishData = GetSKUFinishes(inSKU)

                jkTrace("Finishes at Item Level " & SKUData.ItemLevel.Count)
                jkTrace("Finishes at collection Level " & SKUData.CollectionLevel.Count)
                jkTrace("Finishes all " & SKUData.AllFinishes.Count)

                jkTrace("IsConfigFinishSKU: " & IsConfigurableFinishSKU(inSKU))


                If Not IsFullyUph(inSKU) Then
                    jkTrace("Not Fully Uph")
                    If SKUData.ItemLevel.Count > 0 Then                     'Has Item Level Finishes defined
                        retval = OutputFinishList(SKUData.ItemLevel)
                    ElseIf SKUData.CollectionLevel.Count > 0 Then           'Has Collection Level Finishes Defined

                        If IsConfigurableFinishSKU(inSKU) Then
                            retval = OutputFinishList(SKUData.CollectionLevel)
                        End If
                    ElseIf SKUData.AllFinishes.Count > 0 Then               'Show Full Finish List

                        If IsConfigurableFinishSKU(inSKU) Then
                            retval = OutputFinishList(SKUData.AllFinishes)
                        End If
                    End If
                End If



            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retval
        End Function

        Public Shared Function IsConfigurableFinishSKU(ByVal SKU As String) As Boolean
            Dim retValue As String = False

            Try

                Dim CLASS_001 As DataTable = GetSAPData(SKU, "001")
                Dim CLASS_300 As DataTable = GetSAPData(SKU, "300")

                If CLASS_300.Rows.Count > 0 Then
                    Dim FRAME_COLOR_ARR() As DataRow = CLASS_300.Select("CHARACT = 'FRAME_COLOR' AND VALUE_NEUTRAL <> 'NA'  ")
                    If FRAME_COLOR_ARR.Length > 0 Then
                        retValue = True
                        jkTrace("Showing Finish for: FRAME_COLOR")
                    End If
                    Dim FINISH_AREA_ARR() As DataRow = CLASS_300.Select("CHARACT = 'FINISH_AREA_1' and VALUE_NEUTRAL <> 'NA' ")
                    If FINISH_AREA_ARR.Length > 0 Then
                        retValue = True
                    End If
                    Dim LEISURE_FINISH_ARR() As DataRow = CLASS_300.Select("CHARACT = 'LEISURE_FINISH' and VALUE_NEUTRAL <> 'NONE' ")
                    If LEISURE_FINISH_ARR.Length > 0 Then
                        retValue = True
                    End If
                End If

                If CLASS_001.Rows.Count > 0 Then
                    jkTrace("Have some data")
                    Dim FINISH_CONTROL_DATA_ARR() As DataRow = CLASS_001.Select("CHARACT = 'FINISH_CONTROL_DATA'")
                    Dim FINISH_TYPE_1_ARR() As DataRow = CLASS_001.Select("CHARACT = 'FINISH_TYPE_1' ")

                    If FINISH_CONTROL_DATA_ARR.Count > 0 Then
                        Dim FINISH_CONTROL_DATA = FINISH_CONTROL_DATA_ARR(0)("VALUE_NEUTRAL")
                        If FINISH_CONTROL_DATA = "Y" Then
                            retValue = True
                            jkTrace("Showing Finish for: FINISH_CONTROL_DATA")
                        End If
                    End If

                    If FINISH_TYPE_1_ARR.Count > 0 Then
                        Dim FINISH_TYPE_1 = FINISH_TYPE_1_ARR(0)("VALUE_NEUTRAL")
                        If FINISH_TYPE_1 <> "" And FINISH_TYPE_1 <> "NA" Then
                            retValue = True
                            jkTrace("Showing Finish for: FINISH_TYPE_1")
                        End If
                    End If
                End If




            Catch rfcCommEx As RfcCommunicationException
                System.Web.HttpContext.Current.Trace.Write(rfcCommEx.StackTrace)
                System.Web.HttpContext.Current.Trace.Write(rfcCommEx.Message)

            Catch rfcLogonEx As RfcLogonException
                System.Web.HttpContext.Current.Trace.Write(rfcLogonEx.StackTrace)
                System.Web.HttpContext.Current.Trace.Write(rfcLogonEx.Message)

            Catch rfcAbapRuntimeEx As RfcAbapRuntimeException
                System.Web.HttpContext.Current.Trace.Write(rfcAbapRuntimeEx.StackTrace)
                System.Web.HttpContext.Current.Trace.Write(rfcAbapRuntimeEx.Message)

            Catch rfcAbapBaseEx As RfcAbapBaseException
                If rfcAbapBaseEx.Message.ToString = String.Empty Then
                    Dim tEx As SAP.Middleware.Connector.RfcAbapClassicException = CType(rfcAbapBaseEx, SAP.Middleware.Connector.RfcAbapClassicException)
                    System.Web.HttpContext.Current.Trace.Write(tEx.Key.ToString())
                Else
                    System.Web.HttpContext.Current.Trace.Write(rfcAbapBaseEx.Message.ToString())
                End If

            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retValue
        End Function

        Private Shared Function GetSAPData(ByVal SKU As String, ByVal ClassType As String) As DataTable

            Dim SAP_SYS As RfcDestination
            Dim retVal As New DataTable

            Try
                SAP_SYS = RfcDestinationManager.GetDestination("PRD")

                'SAP_SYS = RfcDestinationManager.GetDestination(ConfigurationManager.AppSettings("SAPSystem"))
                Dim docAPI As IRfcFunction = SAP_SYS.Repository.CreateFunction("Z_WEB_GET_MATERIAL_CLASS_DATA")

                jkTrace("Getting SAP Data for: " & SKU)

                docAPI.SetValue("OBJECTTABLE", "MARA")
                docAPI.SetValue("CLASSTYPE", ClassType)
                docAPI.SetValue("KEYDATE", Today.ToString("yyyyMMdd"))

                Dim MATERIAL_IN As IRfcTable = docAPI.GetTable("MATNR_IN")
                MATERIAL_IN.Append()
                MATERIAL_IN.SetValue("SIGN", "I")
                MATERIAL_IN.SetValue("OPTION", "EQ")

                If Int32.TryParse(SKU, Nothing) Then
                    jkTrace("adding as numberic.")
                    MATERIAL_IN.SetValue("MATNR_LOW", Right("000000000000000000" & SKU, 18))   'If the sku is all numeric, then must be preceeded by zeros. SAP is F%%$%^(^ stupid this way.
                Else
                    jkTrace("Adding as non-numeric")
                    MATERIAL_IN.SetValue("MATNR_LOW", SKU)
                End If



                docAPI.Invoke(SAP_SYS)

                Dim CLASS_DATA_OUT As IRfcTable = docAPI.GetTable("CHAR_VAL_OUT")

                retVal = Misc.ConvertSAPTable2ADOTable(CLASS_DATA_OUT)

            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return retVal
        End Function



        Private Shared Function GetSKUFinishes(ByVal SKU As String) As SkuFinishData
            '*****************************************************************************************************************
            'Dev:   Joe Kelly (8/3/2017)
            '       Gets the finish data for a specific sku. There are 3 objects passed back with data at the item,coll, full
            '*****************************************************************************************************************
            Dim retVal As New SkuFinishData

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
            dbConn.Open()

            Try
                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("--Below here are Vars needed in query ")
                tb.AppendLine("USE ProdMgmtWeb;")
                tb.AppendLine("Declare @MPG as varchar(50) ")
                tb.AppendLine("Set @MPG = (Select top 1 OrigCollectionNo from CenturyWeb.dbo.Items where sku=@sku); ")
                tb.AppendLine(" ")
                tb.AppendLine(" ")
                tb.AppendLine("--These for Finish Groups Available ")
                tb.AppendLine("Declare @GroupAAvailable as bit ")
                tb.AppendLine("Declare @GroupBAvailable as bit ")
                tb.AppendLine("Declare @GroupDAvailable as bit ")
                tb.AppendLine("select   ")
                tb.AppendLine("	@GroupAAvailable=isnull(GroupAFinishAvailable,0),  ")
                tb.AppendLine("	@GroupBAvailable=isnull(GroupBFinishAvailable,0),  ")
                tb.AppendLine("	@GroupDAvailable=isnull(GroupDFinishAvailable,0)  ")
                tb.AppendLine("FROM CenturyWeb.dbo.ITems_characteristics  ")
                tb.AppendLine("where sku=@SKU; ")
                tb.AppendLine(" ")

                tb.AppendLine("--Check at the item level first  ")
                tb.AppendLine("select   ")
                tb.AppendLine("	f.FinishID,  ")
                tb.AppendLine("	f.FinishDesc,  ")
                tb.AppendLine("	g.GroupID,  ")
                tb.AppendLine("	g.GroupName,  ")
                tb.AppendLine("	isnull(g.GroupNote,'') as GroupNote  ")
                tb.AppendLine("FROM icAvailableFinishes_Item_Xref fi  ")
                tb.AppendLine("LEFT JOIN icAvailableFinishes f on f.FinishID=fi.FinishID  ")
                tb.AppendLine("LEFT JOIN icAvailableFinishes_Groups g on g.GroupID=fi.GroupID and g.WebsiteID=@WebsiteID  ")
                tb.AppendLine("where   ")
                tb.AppendLine("	sku=@SKU  ")
                tb.AppendLine("	and f.ShowOnWeb=1  ")
                tb.AppendLine("ORDER BY ")
                tb.AppendLine(" g.OrderBy,f.FinishDesc; ")

                tb.AppendLine(" ")
                tb.AppendLine("----Check at the Collection Level Next ")
                tb.AppendLine("Select  ")
                tb.AppendLine("	f.FinishID, ")
                tb.AppendLine("	f.FinishDesc, ")
                tb.AppendLine("	g.GroupID, ")
                tb.AppendLine("	g.GroupName, ")
                tb.AppendLine("	isnull(g.GroupNote,'') as GroupNote ")
                tb.AppendLine("FROM icAvailableFinishes_Collection_Xref c ")
                tb.AppendLine("INNER JOIN icAvailableFinishes f on f.FinishID=c.FinishID ")
                tb.AppendLine("INNER JOIN icAvailableFinishes_Groups g on g.GroupID=c.GroupID   ")
                tb.AppendLine("where  ")
                tb.AppendLine("	c.CollectionNo=@MPG ")
                tb.AppendLine("	and f.ShowOnWeb=1 ")
                tb.AppendLine("	and g.WebsiteID=@WebsiteID ")
                tb.AppendLine("ORDER BY g.OrderBy,f.FinishDesc; ")
                tb.AppendLine(" ")
                tb.AppendLine("---- Show Full List ")
                tb.AppendLine("select   ")
                tb.AppendLine("	f.FinishID,  ")
                tb.AppendLine("	f.FinishDesc,  ")
                tb.AppendLine("	g.GroupID,  ")
                tb.AppendLine("	g.GroupName,  ")
                tb.AppendLine(" isnull(g.GroupNote,'') as GroupNote ")
                tb.AppendLine("FROM icAvailableFinishes f  ")
                tb.AppendLine("INNER JOIN icAvailableFinishes_Website_Xref w on w.FinishID=f.FinishID  ")
                tb.AppendLine("INNER JOIN icAvailableFinishes_Groups g on g.GroupID=w.GroupID and w.WebsiteID=g.WebsiteID  ")
                tb.AppendLine("WHERE  ")
                tb.AppendLine("	f.ShowOnWeb=1  ")
                tb.AppendLine("	and w.WebsiteID=@WebsiteID ")
                tb.AppendLine("	and (g.GroupID not in ('1','1.5','5') OR @GroupAAvailable=1) ")
                tb.AppendLine("	and (g.GroupID not in ('2','2.5') OR @GroupBAvailable=1) ")
                tb.AppendLine("	and (g.GroupID not in ('3','4') OR @GroupDAvailable=1) ")
                tb.AppendLine(" and (g.GroupID not in ('20') OR @GroupAAvailable = 1 OR @GroupBAvailable =1  OR @GroupDAvailable = 1) ")
                tb.AppendLine("ORDER BY  ")
                tb.AppendLine("	g.OrderBy,  ")
                tb.AppendLine("	f.FinishDesc  ")

                Dim sC As New SqlDataAdapter(tb.ToString, dbConn)
                sC.SelectCommand.Parameters.AddWithValue("@SKU", SKU)
                sC.SelectCommand.Parameters.AddWithValue("@WebsiteID", WebsiteID)
                Dim dSet As New DataSet
                sC.Fill(dSet)

                retVal.ItemLevel = Misc.DataReaderMapToList(Of FinishData)(dSet.Tables(0).CreateDataReader)
                retVal.CollectionLevel = Misc.DataReaderMapToList(Of FinishData)(dSet.Tables(1).CreateDataReader)
                retVal.AllFinishes = Misc.DataReaderMapToList(Of FinishData)(dSet.Tables(2).CreateDataReader)



            Catch ex As Exception
                Misc.LogError(ex)
            Finally
                dbConn.Close()
            End Try

            Return retVal
        End Function



        Public Shared Function ShowFinishesTiny(ByVal inSKU As String, ByVal inColl As String, ByVal inCollType As String, Optional ByVal noPopUp As Boolean = False, Optional ByVal FromDealer As String = "False") As String
            '************************************************************************************************************************
            'Dev:   jkelly (8/3/2017)
            '       This is where we were showing some 25x25 thumbnails on the product listing/search result pages for 
            '       outdoor tables. I'm leaving the call in the pages, but stubbing this out here, in case they can't live
            '       without it. It was throwing off the output.. and now they can click into the item and get finish detail,
            '       so I'm thinking it's not needed.
            '***********************************************************************************************************************

            Return ""


        End Function






        Private Shared Sub IsFinishImageOkToShow(ByVal inFinish As String, ByRef imageToShow As String, ByRef showBigFinish As String, ByVal imagePath As String)


            If inFinish <> "" Then
                imageToShow = inFinish & "_50H.jpg"


                If Not System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(imagePath & imageToShow)) Then
                    imageToShow = ""
                Else
                    showBigFinish = inFinish & ".jpg"
                End If
            Else
            End If




        End Sub



        Private Shared Function IsFullyUph(ByVal inSKU As String) As Boolean

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
            dbConn.Open()

            Dim retval As Boolean = False

            Try
                Dim tSQL As String = ""
                tSQL = "select sku,isnull(Wood,'') as Wood "
                tSQL &= "FROM Items  "
                tSQL &= "where sku=@SKU "

                Dim tC As New SqlCommand(tSQL, dbConn)
                tC.Parameters.Add(New SqlParameter("@SKU", inSKU))
                Dim tR As SqlDataReader
                tR = tC.ExecuteReader
                If tR.Read Then
                    If UCase(tR("Wood")) = "FULLY UPH." Or UCase(tR("Wood")) = "FULLY UPHOLSTERED" Then
                        retval = True
                    ElseIf Left(inSKU, 3) = "MAT" Then
                        retval = True
                    End If
                End If

            Catch ex As Exception
                Misc.LogError(ex)
            Finally
                dbConn.Close()
            End Try
            Return retval

        End Function





        Private Shared Sub jkTrace(ByVal instring As String)

            HttpContext.Current.Trace.Write(instring)
        End Sub


        Public Class FinishData
            Public Property FinishID As String
            Public Property FinishDesc As String
            Public Property GroupID As String
            Public Property GroupName As String
            Public Property GroupNote As String

        End Class


        Public Class SkuFinishData
            Public Property ItemLevel As New List(Of FinishData)
            Public Property CollectionLevel As New List(Of FinishData)
            Public Property AllFinishes As New List(Of FinishData)
        End Class

        Public Class FinishDetail
            Public Property FinishID As String
            Public Property FinishDesc As String
            Public Property GroupName As String

        End Class


    End Class

End Namespace
