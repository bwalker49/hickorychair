﻿Namespace Models
    Public Class NailTrim
        Property NailTrimList As New List(Of NailTrimItem)

        Public Sub New()
            Try
                '*******************************************************************************************************
                'jkelly - 10/12/2021
                'Hardcode for now. Will discuss with Maria if there is a way to pull from SAP
                'NOTE: Images -> \\centimages\Images_Production\nail-images
                '*******************************************************************************************************
                ' HC
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS-00", .NailDesc = "ANTIQUE BRASS NAIL TRIM", .StyleNumber = "NS-00", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_01", .NailDesc = "GILTED BRASS NAIL TRIM", .StyleNumber = "NS-01", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_02", .NailDesc = "PEWTER SMALL NAIL TRIM", .StyleNumber = "NS-02", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_05", .NailDesc = "NATURAL SMALL NAIL TRIM", .StyleNumber = "NS-05", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_06", .NailDesc = "HAMMERED BRASS NAIL TRIM", .StyleNumber = "NS-06", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_08", .NailDesc = "OLD BRASS SMALL NAIL TRIM", .StyleNumber = "NS-08", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_09", .NailDesc = "ANTIQUE NICKEL SMALL NAIL TRIM", .StyleNumber = "NS-09", .NailGroup = "Basic", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS-10", .NailDesc = "BLACK PEARL SMALL NAIL TRIM", .StyleNumber = "NS-10", .NailGroup = "Basic", .ImgSize = "1/2"})
                ' PEARSON
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON ANTIQUE BRASS SMALL NAIL TRIM", .NailGroup = "Basic", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON BRIGHT BRASS SMALL NAIL TRIM", .NailGroup = "Basic", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON PEWTER SMALL NAIL TRIM", .NailGroup = "Basic", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON CHROME SMALL NAIL TRIM", .NailGroup = "Basic", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON WEATHERED BRASS SMALL NAIL TRIM", .NailGroup = "Basic", .ImgSize = "3/8"})

                ' HC
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNP-00", .NailDesc = "ANTIQUE BRASS PETITE NAIL TRIM", .StyleNumber = "NP-00", .NailGroup = "Premium", .ImgSize = "3/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNP-03", .NailDesc = "CHROME PETITE NAIL TRIM", .StyleNumber = "NP-03", .NailGroup = "Premium", .ImgSize = "3/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNP-05", .NailDesc = "NATURAL PETITE NAIL TRIM", .StyleNumber = "NP-05", .NailGroup = "Premium", .ImgSize = "3/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNP-08", .NailDesc = "OLD BRASS PETITE NAIL TRIM", .StyleNumber = "NP-08", .NailGroup = "Premium", .ImgSize = "3/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNP-09", .NailDesc = "ANTIQUE NICKEL PETITE NAIL TRIM", .StyleNumber = "NP-09", .NailGroup = "Premium", .ImgSize = "3/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNM-05", .NailDesc = "NATURAL MEDIUM NAIL TRIM", .StyleNumber = "NM-05", .NailGroup = "Premium", .ImgSize = "5/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNM-08", .NailDesc = "OLD BRASS MEDIUM NAIL TRIM", .StyleNumber = "NM-08", .NailGroup = "Premium", .ImgSize = "9/16"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNS_99", .NailDesc = "ANTIQUE ROSETTE NAIL TRIM", .StyleNumber = "NS-99", .NailGroup = "Premium", .ImgSize = "1/2"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNM-09", .NailDesc = "ANTIQUE NICKEL MEDIUM NAIL TRIM", .StyleNumber = "NM-09", .NailGroup = "Premium", .ImgSize = "5/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNM-10", .NailDesc = "BLACK PEARL MEDIUM NAIL TRIM", .StyleNumber = "NM-10", .NailGroup = "Premium", .ImgSize = "5/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNL-04", .NailDesc = "AGED ANTIQUE LARGE NAIL TRIM", .StyleNumber = "NL-04", .NailGroup = "Premium", .ImgSize = "7/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNL_05", .NailDesc = "NATURAL LARGE NAIL TRIM", .StyleNumber = "NL-05", .NailGroup = "Premium", .ImgSize = "7/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNL_09", .NailDesc = "ANTIQUE NICKEL LARGE NAIL TRIM", .StyleNumber = "NL-09", .NailGroup = "Premium", .ImgSize = "7/8"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNL_10", .NailDesc = "BLACK PEARL LARGE NAIL TRIM", .StyleNumber = "NL-10", .NailGroup = "Premium", .ImgSize = "7/8"})
                ' PEARSON
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON ANTIQUE BRASS LARGE NAIL TRIM", .NailGroup = "Premium", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON BRIGHT BRASS LARGE NAIL TRIM", .NailGroup = "Premium", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON PEWTER LARGE NAIL TRIM", .NailGroup = "Premium", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON CHROME LARGE NAIL TRIM", .NailGroup = "Premium", .ImgSize = "3/8"})
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON WEATHERED BRASS LARGE NAIL TRIM", .NailGroup = "Premium", .ImgSize = "3/8"})

                ' HC
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNM-07", .NailDesc = "WEATHERED MEDIUM NAIL TRIM", .StyleNumber = "NM-07", .NailGroup = "Deluxe", .ImgSize = "9/16"})
                Me.NailTrimList.Add(New NailTrimItem With {.NailID = "HCNL-07", .NailDesc = "WEATHERED LARGE NAIL TRIM", .StyleNumber = "NL-07", .NailGroup = "Deluxe", .ImgSize = "7/8"})
                ' PEARSON
                'Me.NailTrimList.Add(New NailTrimItem With {.NailID = "", .NailDesc = "PEARSON PATINATED BRASS NAIL TRIM", .NailGroup = "Deluxe", .ImgSize = "3/8"})

            Catch ex As Exception
                Misc.LogError(ex)
            End Try
        End Sub


        Public Class NailTrimItem
            Property NailID As String
            Property NailDesc As String
            Property NailGroup As String
            Property ImgSize As String
            Property StyleNumber As String
        End Class
    End Class




End Namespace