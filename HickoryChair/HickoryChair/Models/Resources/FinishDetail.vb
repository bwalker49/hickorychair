﻿Imports System.ComponentModel
Imports System.Data.SqlClient

Public Class FinishDetail
    Public Shared WebsiteID As String = "HickoryChair"
    Property Finish As New SingleFinish
    Property SimilarFinishes As New List(Of SingleFinish)
    Property ItemsFinishedIn As New List(Of String)
    Public Sub New(ByVal ID As String)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ProdMgmtWeb").ConnectionString.ToString)
        dbConn.Open()

        Try
            ' --- Single Finish Data ---
            Dim query As New System.Text.StringBuilder
            query.AppendLine("SELECT top 1 ")
            query.AppendLine(" f.FinishID,  ")
            query.AppendLine(" f.FinishDesc,  ")
            query.AppendLine(" f.FinishLongDesc,  ")
            query.AppendLine(" coalesce(gWeb.GroupID,gItem.GroupID,gColl.GroupID) as GroupID, ")
            query.AppendLine(" coalesce(gWeb.GroupName,gitem.GroupName,gColl.GroupName) as GroupName, ")
            query.AppendLine(" f.HandRubbingAvailable,  ")
            query.AppendLine(" f.Leafing_BrushingAvailable,  ")
            query.AppendLine(" f.AvailableOnlinetoOrder,  ")
            query.AppendLine(" isnull(f.DefaultSheen, '') as DefaultSheen,  ")
            query.AppendLine("	f.Status,  ")
            query.AppendLine(" ifs.StatusDesc,  ")
            query.AppendLine(" isnull(ifs.StatusWebDesc, '') as StatusWebDesc,  ")
            query.AppendLine(" isnull(f.FinishNote,'') as FinishNote,  ")
            query.AppendLine(" isnull(f.RangeCode,'') as RangeCode  ")
            query.AppendLine("INTO #T  ")
            query.AppendLine("FROM icAvailableFinishes f  ")
            query.AppendLine("INNER JOIN icAvailableFinishes_Statuses ifs ON ifs.Status=f.Status  ")
            query.AppendLine("LEFT JOIN icAvailableFinishes_Website_Xref x ON x.FinishID=f.FinishID  ")
            query.AppendLine("LEFT JOIN icAvailableFinishes_Groups gWeb ON gWeb.GroupID=x.GroupID AND x.WebsiteID=gWeb.WebsiteID  ")
            query.AppendLine("LEFT JOIN icAvailableFinishes_Collection_Xref ic on ic.FinishID=f.FinishID ")
            query.AppendLine("LEFT JOIN icAvailableFinishes_Groups gColl on gColl.GroupID=ic.GroupID ")
            query.AppendLine("LEFT JOIN icAvailableFinishes_Item_Xref ix on ix.FinishID=f.FinishID ")
            query.AppendLine("LEFT JOIN icAvailableFinishes_Groups gItem ON gItem.GroupID=x.GroupID ")
            query.AppendLine("WHERE  ")
            query.AppendLine(" f.FinishID=@FinishID  ")
            query.AppendLine(" and f.ShowOnWeb=1  ")
            query.AppendLine(" and (gWeb.WebsiteID=@WebsiteID OR gItem.WebsiteID=@WebsiteID OR gColl.WebsiteID=@WebsiteID) ")

            query.AppendLine("SELECT * FROM #T; ")

            query.AppendLine("SELECT  ")
            query.AppendLine("	f.FinishID, ")
            query.AppendLine("	f.FinishDesc ")
            query.AppendLine("FROM icAvailableFinishes f ")
            query.AppendLine("INNER JOIN icAvailableFinishes_Website_Xref x ON x.FinishID=f.FinishID ")
            query.AppendLine("WHERE  ")
            query.AppendLine("	f.RangeCode=(SELECT RangeCode FROM #T) ")
            query.AppendLine(" and isnull(f.RangeCode,'') != '' ")
            query.AppendLine(" and f.FinishID != @FinishID ")
            query.AppendLine("	and x.WebsiteID=@WebsiteID ")
            query.AppendLine("	and f.ShowOnWeb=1; ")

            query.AppendLine("DROP TABLE #T; ")

            Dim sqlDataAdapter As New SqlDataAdapter(query.ToString, dbConn)
            sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@FinishID", ID)
            sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@websiteID", WebsiteID)

            Dim dataSet As New DataSet
            sqlDataAdapter.Fill(dataSet)

            Dim dataRow As DataRow

            If dataSet.Tables(0).Rows.Count > 0 Then
                dataRow = dataSet.Tables(0).Rows(0)

                Dim handRubbingAvailable As Boolean = False
                If dataRow.Item("HandRubbingAvailable") Then
                    handRubbingAvailable = True
                End If

                Dim leafingBrushingAvailable As Boolean = False
                If dataRow.Item("Leafing_BrushingAvailable") Then
                    leafingBrushingAvailable = True
                End If

                Dim status As String = ""
                If dataRow.Item("StatusWebDesc") <> "" Then
                    status = dataRow.Item("StatusWebDesc")
                Else
                    status = dataRow.Item("StatusDesc")
                End If

                Dim availableOnlineToOrder As Boolean = False
                If dataRow.Item("AvailableOnlinetoOrder") Then
                    availableOnlineToOrder = True
                End If

                ' Create new SingleFinish and add it to FinishDetail property
                Dim finish As New SingleFinish With {
                    .ID = dataRow.Item("FinishID"),
                    .Group = dataRow.Item("GroupName"),
                    .Name = dataRow.Item("FinishDesc"),
                    .LongDescription = dataRow.Item("FinishLongDesc"),
                    .Status = status,
                    .HandRubbingAvailable = handRubbingAvailable,
                    .LeafingBrushingAvailable = leafingBrushingAvailable,
                    .Sheen = dataRow.Item("DefaultSheen"),
                    .Note = dataRow.Item("FinishNote"),
                    .AvailableOnlineToOrder = availableOnlineToOrder
                }

                Me.Finish = finish

                ' --- Similar Finishes ---
                If dataSet.Tables(1).Rows.Count > 0 Then
                    For Each finishRow In dataSet.Tables(1).Rows
                        Dim similarFinish As New SingleFinish With {
                            .ID = finishRow("FinishID"),
                            .Name = finishRow("FinishDesc")
                        }

                        Me.SimilarFinishes.Add(similarFinish)
                    Next
                End If

            End If
        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        ' --- Items Finished In ---
        Dim dbConn2 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn2.Open()

        Try
            Dim query2 As New System.Text.StringBuilder
            query2.AppendLine("SELECT DISTINCT TOP 15 ")
            query2.AppendLine(" i.sku, ")
            query2.AppendLine("	id.image as finishImage ")
            query2.AppendLine("INTO #T ")
            query2.AppendLine("FROM ItemsGoodToShowHCDealer ig ")
            query2.AppendLine("INNER JOIN Items i on i.sku=ig.sku ")
            query2.AppendLine("JOIN image_details id ON i.image = id.image  ")
            query2.AppendLine("OR i.image = id.image  ")
            query2.AppendLine("OR i.extraImages like '%' + id.image + '%'   ")
            query2.AppendLine("WHERE id.mainFinishNumber = @finish;   ")

            query2.AppendLine("SELECT DISTINCT ")
            query2.AppendLine("	finishImage, ")
            query2.AppendLine("	SKU = (select top 1 sku from #T tmp where tmp.finishImage=#T.finishImage) ")
            query2.AppendLine("FROM #T ")
            query2.AppendLine("ORDER BY ")
            query2.AppendLine("	finishImage; ")

            query2.AppendLine("DROP TABLE #T; ")


            Dim sqlDataAdapter2 As New SqlDataAdapter(query2.ToString, dbConn2)
            sqlDataAdapter2.SelectCommand.Parameters.AddWithValue("@finish", ID)

            Dim dataSet2 As New DataSet
            sqlDataAdapter2.Fill(dataSet2)

            If dataSet2.Tables(0).Rows.Count > 0 Then
                For Each row In dataSet2.Tables(0).Rows
                    Me.ItemsFinishedIn.Add(row("SKU") & "~" & row("finishImage"))
                Next
            End If
        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn2.Close()
        End Try
    End Sub
    Public Class SingleFinish
        Property ID As String
        Property Group As String
        Property Name As String
        Property LongDescription As String
        Property Status As String
        Property HandRubbingAvailable As Boolean
        Property LeafingBrushingAvailable As Boolean
        Property Sheen
        Property Note
        Property AvailableOnlineToOrder As Boolean
    End Class
End Class