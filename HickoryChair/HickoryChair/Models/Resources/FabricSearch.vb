﻿
Imports System.ComponentModel
Imports System.Data.SqlClient


Namespace Models
    Public Class FabricSearch

        Private Property m_TypeList() As IEnumerable(Of SelectListItem)
        Private Property m_GradeList() As IEnumerable(Of SelectListItem)
        Private Property m_ColorList() As IEnumerable(Of SelectListItem)
        Private Property m_PatternList() As IEnumerable(Of SelectListItem)
        Private Property m_PageList() As IEnumerable(Of SelectListItem)
        Private Property m_TotalPages As Integer = 0



        Dim DSFabNewIntroStart As String = Misc.ReadOption("DSFabNewIntroStart", "")
        Dim DSFabNewIntroEnd As String = Misc.ReadOption("DSFabNewIntroEnd", "")


        Private Property m_TypeList_Selected As String = String.Empty
        Public Property TypeList_Selected As String
            Get
                Return m_TypeList_Selected
            End Get
            Set(value As String)
                m_TypeList_Selected = value
            End Set
        End Property

        Private m_GradeList_Selected As String = String.Empty
        Public Property GradeList_Selected As String
            Get
                Return m_GradeList_Selected
            End Get
            Set(value As String)
                m_GradeList_Selected = value
            End Set
        End Property

        Private m_ColorList_Selected As String = String.Empty
        Public Property ColorList_Selected As String
            Get
                Return m_ColorList_Selected
            End Get
            Set(value As String)
                m_ColorList_Selected = value
            End Set
        End Property

        Private m_PatternList_Selected As String = String.Empty
        Public Property PatternList_Selected As String
            Get
                Return m_PatternList_Selected
            End Get
            Set(value As String)
                m_PatternList_Selected = value
            End Set
        End Property


        Private m_OnlyShowFabsWithImages As Boolean = False
        <DisplayName("Only Show Fabrics w/Images")>
        Public Property OnlyShowFabsWithImages As Boolean
            Get
                Return m_OnlyShowFabsWithImages
            End Get
            Set(value As Boolean)
                m_OnlyShowFabsWithImages = value
            End Set
        End Property


        Private m_OnlyShowNewIntros As Boolean = False

        Public Sub New()
            Me.ColorList_Selected = ""
            Me.GradeList_Selected = ""
            Me.MatlBox = ""
            Me.TypeList_Selected = ""
            Me.PatternList_Selected = ""
        End Sub

        Public Property FabSearchItems As List(Of FabItem)

        Private m_PageSize As Integer = 300
        Public Property PageSize As Integer
            Get
                Return m_PageSize
            End Get
            Set(value As Integer)
                m_PageSize = value
            End Set
        End Property
        Private m_PageIndex As Integer = 1
        Public Property PageIndex As Integer
            Get
                Return m_PageIndex
            End Get
            Set(value As Integer)
                m_PageIndex = value
            End Set
        End Property



        Public Sub New(ByVal fabSearch As Models.FabricSearch)
            FabSearchItems = DoSearch(fabSearch)
        End Sub

        <DisplayName("Only Show New Introductions")>
        Public Property OnlyShowNewIntros As Boolean
            Get
                Return m_OnlyShowNewIntros
            End Get
            Set(value As Boolean)
                m_OnlyShowNewIntros = value
            End Set
        End Property

        <DisplayName("Page:")>
        Public Property PageList() As IEnumerable(Of SelectListItem)
            Get
                If m_PageList Is Nothing Then
                    Dim Pages As New List(Of PageNumber)
                    Pages = GetPages(m_TotalPages)
                    m_PageList = New SelectList(Pages, "pNum", "pNum")
                End If
                Return m_PageList
            End Get
            Set(value As IEnumerable(Of SelectListItem))

            End Set
        End Property
        <DisplayName("Fabric / Leather #")>
        Public Property MatlBox As String

        <DisplayName("Type")>
        Public Property TypeList() As IEnumerable(Of SelectListItem)
            Get
                If m_TypeList Is Nothing Then
                    Dim tList As New List(Of FabTypes)
                    tList.Add(New FabTypes("ALL", ""))
                    'tList.Add(New FabTypes("Bella Dura", "Bella Dura"))
                    'tList.Add(New FabTypes("Crypton", "Crypton"))
                    tList.Add(New FabTypes("Leather", "Leather"))
                    'tList.Add(New FabTypes("Outdoor", "Outdoor Fabrics"))
                    tList.Add(New FabTypes("Fabric", "Fabrics"))
                    tList.Add(New FabTypes("Ultrafabric", "Ultrafabric"))
                    'tList.Add(New FabTypes("Frinier", "Richard Frinier Fabrics"))
                    'tList.Add(New FabTypes("Sunbrella", "Sunbrella"))
                    tList.Add(New FabTypes("Hable", "Hable"))
                    tList.Add(New FabTypes("Performance", "Performance"))
                    tList.Add(New FabTypes("Trims", "Trims"))
                    m_TypeList = New SelectList(tList, "TypeName", "TypeDesc")

                End If
                Return m_TypeList
            End Get
            Set(value As IEnumerable(Of SelectListItem))

            End Set
        End Property

        <DisplayName("Grade")>
        Public Property GradeList() As IEnumerable(Of SelectListItem)
            Get
                If m_GradeList Is Nothing Then
                    Dim Grades As New List(Of FabGrades)
                    Grades = GetGrades()
                    m_GradeList = New SelectList(Grades, "Grade", "Grade")
                End If
                Return m_GradeList
            End Get
            Set(value As IEnumerable(Of SelectListItem))

            End Set
        End Property

        <DisplayName("Color")>
        Public Property ColorList() As IEnumerable(Of SelectListItem)
            Get
                If m_ColorList Is Nothing Then
                    Dim Colors As New List(Of ColorGroups)
                    Colors = GetColors()
                    m_ColorList = New SelectList(Colors, "ColorGroup", "ColorGroupDescription")
                End If
                Return m_ColorList
            End Get
            Set(value As IEnumerable(Of SelectListItem))

            End Set
        End Property

        <DisplayName("Pattern")>
        Public Property PatternList() As IEnumerable(Of SelectListItem)
            Get
                If m_PatternList Is Nothing Then
                    Dim Pattern As New List(Of Patterns)
                    Pattern = GetPatterns()
                    m_PatternList = New SelectList(Pattern, "PatternName", "PatternDescription")
                End If
                Return m_PatternList
            End Get
            Set(value As IEnumerable(Of SelectListItem))

            End Set
        End Property







        Private Function DoSearch(ByVal fabSearch As Models.FabricSearch) As List(Of FabItem)
            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()

            Dim imgPath As String = "~/Content/prod-images/"

            Dim FabItems As New List(Of FabItem)
            Try
                If String.IsNullOrEmpty(fabSearch.MatlBox) Then
                    fabSearch.MatlBox = ""
                End If
                If String.IsNullOrEmpty(fabSearch.TypeList_Selected) Then
                    fabSearch.TypeList_Selected = ""
                End If
                If String.IsNullOrEmpty(fabSearch.GradeList_Selected) Then
                    fabSearch.GradeList_Selected = ""
                End If
                If String.IsNullOrEmpty(fabSearch.ColorList_Selected) Then
                    fabSearch.ColorList_Selected = ""
                End If
                If String.IsNullOrEmpty(fabSearch.PatternList_Selected) Then
                    fabSearch.PatternList_Selected = ""
                End If

                Dim patternSQL As String = ""
                Dim color As String = ""
                Dim grade As String = ""
                Dim MPG As String = ""


                Dim matl As String = ""
                If Not String.IsNullOrEmpty(fabSearch.MatlBox) Then
                    Dim formattedMatl As String = fabSearch.MatlBox
                    Dim prefix As String = Mid(fabSearch.MatlBox, 1, 2).ToUpper()
                    If prefix <> "HC" And prefix <> "PE" Then
                        formattedMatl = "*" + fabSearch.MatlBox.TrimStart("0") + "*" ' Strip leading zeros, add wildcard to front and end
                    End If

                    matl = Replace(formattedMatl.Trim, "*", "%")
                    fabSearch.PatternList_Selected = ""
                    fabSearch.ColorList_Selected = ""
                    fabSearch.OnlyShowFabsWithImages = False
                Else
                    color = fabSearch.ColorList_Selected
                    grade = fabSearch.GradeList_Selected

                    If fabSearch.TypeList_Selected = "Bella Dura" Then
                        patternSQL = "AND FabricFinish = 'BD'"
                    ElseIf fabSearch.TypeList_Selected = "Crypton" Then
                        patternSQL = "AND FabricFinish = 'CR'"
                    ElseIf fabSearch.TypeList_Selected = "Sunbrella" Then
                        patternSQL = "AND FabricFinish = 'SU'"
                    ElseIf fabSearch.TypeList_Selected = "Outdoor" Then
                        MPG = "LF"
                    ElseIf fabSearch.TypeList_Selected = "Frinier" Then
                        patternSQL = " AND Designer='Z16' "
                    ElseIf fabSearch.TypeList_Selected = "Leather" Then
                        patternSQL = " AND Class in ('LTHR') "
                        fabSearch.PatternList_Selected = ""
                    ElseIf fabSearch.TypeList_Selected = "Trims" Then
                        patternSQL = " AND Class in ('FRNG','TASS','BRDR','BTTN','CORD','BULL') "
                        fabSearch.PatternList_Selected = ""
                    ElseIf fabSearch.TypeList_Selected = "Fabric" Then
                        patternSQL = " AND Class not in ('LTHR','FRNG','TASS','BRDR','BTTN','CORD','BULL') "
                        patternSQL = " AND MPG <> 'LF' "
                        patternSQL &= " AND MaterialDescription not like 'Ultrafabric%' "
                        If fabSearch.PatternList_Selected <> "" Then
                            patternSQL = " AND Class='" & fabSearch.PatternList_Selected & "' "
                        End If
                    ElseIf fabSearch.TypeList_Selected = "Ultrafabric" Then
                        patternSQL = " AND MaterialDescription like 'Ultrafabric%' "
                    ElseIf fabSearch.TypeList_Selected = "Performance" Then
                        patternSQL = " AND (PerformanceCode IS NOT NULL OR PerformanceDesc IS NOT NULL) "
                    ElseIf fabSearch.TypeList_Selected = "Hable" Then
                        patternSQL = " AND (isnull(DesignerCode, '') = 'HC' )"
                    ElseIf fabSearch.PatternList_Selected <> "" Then
                        patternSQL = " AND Class='" & fabSearch.PatternList_Selected & "' "
                    End If
                End If


                'Dim RHFWebsiteID As String = CType(HttpContext.Current.User, CustomPrincipal).RHFWebsiteID
                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("WITH fabItems AS ( ")
                tb.AppendLine("SELECT Distinct ROW_NUMBER() OVER (ORDER BY rp.materialnumber) AS ROW, ")
                tb.AppendLine("	rp.MaterialNumber,  ")
                tb.AppendLine("	isnull(rp.MaterialDescription,'') as MaterialDescription,  ")
                tb.AppendLine("	isnull(rp.Status,'') as Status,  ")
                tb.AppendLine(" isnull(rp.CreatedDate,'') as CreatedDate, ")
                tb.AppendLine(" isnull(rp.FabricRestrictions,'') as FabricRestrictions, ")
                tb.AppendLine(" isnull(rp.FabricFinish,'') as FabricFinish, ")
                tb.AppendLine("	isnull(rp.Class,'') as Class,  ")
                tb.AppendLine("	isnull(rp.ClassDescription,'') as ClassDescription,  ")
                tb.AppendLine(" isnull(rp.Grade,'') as Grade, ")
                tb.AppendLine(" isnull(c.NewGrade,'') as NewGrade, ")
                tb.AppendLine(" isnull(c.EffectiveDate,'') as EffectiveDate, ")
                tb.AppendLine("	isnull(rp.ColorGroup,'') as ColorGroup,  ")
                tb.AppendLine("	isnull(rp.ColorGroupDescription,'') as ColorGroupDescription ,isnull(rp.Exclusivity,'') as Exclusivity,  ")
                tb.AppendLine(" isnull(rp.MPG, '') as MPG")
                tb.AppendLine("	  ")
                tb.AppendLine("FROM rpZFABInfo rp  ")
                tb.AppendLine("INNER JOIN CenturyWeb.dbo.Fabrics_Status fs on fs.SKU=rp.MaterialNumber and rp.SOrg=fs.SOrg ")
                tb.AppendLine(" LEFT JOIN CenturyWeb.dbo.Fabrics_Grade_Changes c on c.SKU=fs.SKU ")
                tb.AppendLine("where 1=1  ")
                tb.AppendLine(" AND rp.SOrg = 'HC01' ")
                tb.AppendLine(" AND fs.SOrg = 'HC01' ")
                'If matl = "" Then
                tb.AppendLine("	AND rp.Status not in ('DS','DR','MX','NS','PL','DX','DQ')  ")
                    tb.AppendLine(" AND (rp.Status not in ('DN','MD') OR fs.QtyInStock >= 50) ")
                'End If
                tb.AppendLine("	AND rp.Grade is not null  ")
                tb.AppendLine(" and rp.Status not in ('11') ")  'Always exclude status 11 jkelly 9/6/2012

                tb.AppendLine(patternSQL)
                tb.AppendLine("	AND isnull(rp.ColorGroup,'')=coalesce(nullif(@ColorGroup,''),isnull(rp.ColorGroup,''))  ")
                If grade <> "" Then
                    tb.AppendLine("	AND rp.Grade = '" & grade.ToString & "' ")
                End If
                tb.AppendLine("	AND (rp.MaterialNumber like @MaterialNumber OR nullif(@MaterialNumber,'') is null)  ")
                tb.AppendLine(" AND rp.MPG=coalesce(nullif(@MPG,''),rp.MPG)  ")
                tb.AppendLine(" and plantlocation='HC01' ")

                If fabSearch.OnlyShowNewIntros Then
                    'This hard coded for emergency hide at Market per Comer Wear and Wali.
                    tb.AppendLine(GetDateRange())
                    tb.AppendLine("AND rp.MaterialNumber not in('ULEA-300-4458','ULEA-300-5716','71115L85') ")
                End If

                tb.AppendLine(" ")
                tb.AppendLine(") ")
                tb.AppendLine(" ")
                tb.AppendLine("select ")
                tb.AppendLine(" rCount=(Select Case when (count(*) % @PageSize) > 0 THEN (count(*)/@PageSize) + 1 ELSE (count(*)/@PageSize) END  FROM fabItems) ")
                tb.AppendLine(",* ")
                tb.AppendLine("FROM fabItems ")
                tb.AppendLine("WHERE")
                tb.AppendLine(" Row between (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize")

                Dim tC As New SqlDataAdapter(tb.ToString, dbConn)
                Dim tmpFabTable As New DataTable
                tC.SelectCommand.Parameters.AddWithValue("@ColorGroup", color)
                tC.SelectCommand.Parameters.AddWithValue("@MaterialNumber", matl)
                tC.SelectCommand.Parameters.AddWithValue("@MPG", MPG)
                tC.SelectCommand.Parameters.AddWithValue("@PageSize", fabSearch.PageSize)
                tC.SelectCommand.Parameters.AddWithValue("@PageIndex", fabSearch.PageIndex)
                tC.Fill(tmpFabTable)

                For Each tR In tmpFabTable.Rows
                    Dim fItem As New FabItem
                    m_TotalPages = tR("rCount")
                    'Dim ck As String = HttpContext.Current.Server.MapPath("~" & imgPath & tR("MaterialNumber") & "_tiny.jpg")

                    If System.IO.File.Exists(HttpContext.Current.Server.MapPath(imgPath & tR("MaterialNumber") & "_tiny.jpg")) Then
                        fItem.FabImage = imgPath & tR("MaterialNumber") & "_tiny.jpg"
                    Else
                        fItem.FabImage = imgPath & "noImage_tiny.jpg"
                    End If

                    fItem.MaterialNumber = tR("MaterialNumber")
                    fItem.MaterialDescription = tR("MaterialDescription")
                    fItem.Status = tR("Status")
                    fItem.CreatedDate = tR("CreatedDate")
                    fItem.FabricRestrictions = tR("FabricRestrictions")
                    fItem.FabricFinish = tR("FabricFinish")
                    fItem.FabClass = tR("Class")
                    fItem.FabClassDescription = tR("ClassDescription")
                    fItem.Grade = tR("Grade")
                    fItem.NewGrade = tR("NewGrade")
                    fItem.NewGradeDate = tR("EffectiveDate")
                    fItem.ColorGroup = tR("ColorGroup")
                    fItem.ColorGroupDescription = tR("ColorGroupDescription")
                    fItem.Exclusivity = tR("Exclusivity")
                    fItem.MPG = tR("MPG")
                    fItem.IsNewIntro = IsNewIntro(tR("CreatedDate"))
                    FabItems.Add(fItem)
                Next

            Catch ex As Exception
                Misc.LogError(ex)
            Finally
                dbConn.Close()
            End Try

            Return FabItems
        End Function

        Private Function GetDateRange() As String
            '******************************************************************************************************
            'Author:    Joe Kelly
            'Date:      2/9/2012
            'Purpose:   To get the date range of fabrics that are to be considered "New Intros". Our first crack
            '           at this involved trying to calculate this based on the current month and trying to determine
            '           what to show via that. Wali decided it would be better if she could manually enter dates in
            '           an admin. So I am going to look in the Options table and see if we have values. If we do, then
            '           use those dates. If not then revert back to calculated way of getting dates.
            '*******************************************************************************************************
            Dim retVal As String = ""

            Try
                Dim DSFabNewIntroStart As String = Misc.ReadOption("DSFabNewIntroStart", "")
                Dim DSFabNewIntroEnd As String = Misc.ReadOption("DSFabNewIntroEnd", "")

                If DSFabNewIntroStart <> "" And DSFabNewIntroEnd <> "" Then
                    retVal = "And (rp.CreatedDate between '" & DSFabNewIntroStart & "' AND '" & DSFabNewIntroEnd & "') "
                Else
                    Dim OctLastYear As New Date(Today.Year - 1, 10, 1)
                    Dim OctThisYear As New Date(Today.Year, 10, 1)
                    Dim AprilLastYear As New Date(Today.Year - 1, 4, 1)
                    Dim SeptLastYear As New Date(Today.Year - 1, 10, 30)

                    Dim MarchThisYear As New Date(Today.Year, 3, 30)
                    Dim AprilThisYear As New Date(Today.Year, 4, 1)
                    Dim AprilNextYear As New Date(Today.Year + 1, 4, 1)

                    Dim currMonth As Integer = Today.Month

                    If currMonth >= 4 And currMonth < 10 Then
                        retVal = "AND (( rp.CreatedDate between '" & OctLastYear.ToShortDateString & "' AND '" & MarchThisYear.ToShortDateString & "') "
                        retVal &= "   OR (rp.CreatedDate between '" & AprilThisYear.ToShortDateString & "' and '" & OctThisYear.ToShortDateString & "')) "
                    ElseIf currMonth >= 10 And currMonth <= 12 Then
                        retVal = "AND (( rp.CreatedDate between '" & AprilThisYear.ToShortDateString & "' AND '" & OctThisYear.ToShortDateString & "') "
                        retVal &= "OR (rp.CreatedDate between '" & OctThisYear.ToShortDateString & "' and '" & AprilNextYear.ToShortDateString & "')) "
                    Else
                        retVal = "AND (( rp.CreatedDate between '" & AprilLastYear.ToShortDateString & "' AND '" & OctLastYear.ToShortDateString & "') "
                        retVal &= "OR (rp.CreatedDate between '" & OctLastYear.ToShortDateString & "' and '" & AprilThisYear.ToShortDateString & "')) "
                    End If
                End If



            Catch ex As Exception
                Misc.LogError(ex)
            End Try
            Return retVal
        End Function

        Private Function IsNewIntro(ByVal CreatedDate As String) As Boolean
            Dim retVal As Boolean = False

            Try


                If DSFabNewIntroStart <> "" And DSFabNewIntroEnd <> "" Then
                    Dim sDate As Date = CDate(DSFabNewIntroStart)
                    Dim eDate As Date = CDate(DSFabNewIntroEnd)

                    Dim ctDate As Date = CDate(CreatedDate)

                    If ctDate >= sDate And ctDate <= eDate Then
                        retVal = True
                    End If
                Else
                    Dim OctLastYear As New Date(Today.Year - 1, 10, 1)
                    Dim OctThisYear As New Date(Today.Year, 10, 1)
                    Dim AprilLastYear As New Date(Today.Year - 1, 4, 1)
                    Dim SeptLastYear As New Date(Today.Year - 1, 10, 30)

                    Dim MarchThisYear As New Date(Today.Year, 3, 30)
                    Dim AprilThisYear As New Date(Today.Year, 4, 1)
                    Dim AprilNextYear As New Date(Today.Year + 1, 4, 1)

                    Dim currMonth As Integer = Today.Month
                    Dim ctDate As Date = CDate(CreatedDate)


                    If currMonth >= 4 And currMonth < 10 And ((ctDate >= OctLastYear And ctDate <= MarchThisYear) Or (ctDate >= AprilThisYear And ctDate <= OctThisYear)) Then
                        retVal = True
                    ElseIf currMonth >= 10 And currMonth <= 12 And ((ctDate >= AprilThisYear And ctDate <= OctThisYear) Or (ctDate >= OctThisYear And ctDate <= AprilNextYear)) Then
                        retVal = True
                    ElseIf ((ctDate >= AprilLastYear And ctDate <= OctLastYear) Or (ctDate >= OctLastYear And ctDate <= AprilThisYear)) Then
                        retVal = True
                    End If
                End If

            Catch ex As Exception
                Misc.LogError(ex)
            End Try
            Return retVal
        End Function


        Public Class FabItem
            Property FabImage As String
            Property MaterialNumber As String
            Property MaterialDescription As String
            Property Status As String
            Property CreatedDate As String
            Property FabricRestrictions As String
            Property FabricFinish As String
            Property FabClass As String
            Property FabClassDescription As String
            Property Grade As String
            Property NewGrade As String
            Property NewGradeDate As String
            Property ColorGroup As String
            Property ColorGroupDescription As String
            Property Exclusivity As String
            Property MPG As String
            Property IsNewIntro As Boolean


        End Class


        Private Class FabTypes
            Property TypeName As String
            Property TypeDesc As String

            Public Sub New(ByVal TypeName As String, ByVal TypeDesc As String)
                Me.TypeName = TypeName
                Me.TypeDesc = TypeDesc

            End Sub
        End Class

        Private Class FabGrades
            Property Grade As String
            Property GradeDesc As String
        End Class

        Private Shared Function GetGrades() As List(Of FabGrades)
            Dim retVal As New List(Of FabGrades)

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()

            Try
                'Dim RHFWebsiteID As String = CType(HttpContext.Current.User, CustomPrincipal).RHFWebsiteID

                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("select Distinct ")
                tb.AppendLine("	Grade, Centuryweb.dbo.fnAlphaSort(Grade) ")
                tb.AppendLine("FROM rpZFABInfo ")
                tb.AppendLine("where  ")
                tb.AppendLine("	Status Not in ('DS','DR','MX','NS','PL','DX','11') ")
                tb.AppendLine("	AND Grade is not null ")
                'tb.AppendLine(" AND MPG in " & GetFabMPGs(RHFWebsiteID) & " ")
                tb.AppendLine(" AND SOrg = 'HC01' ")
                tb.AppendLine("ORDER BY Centuryweb.dbo.fnAlphaSort(Grade) ")
                Dim gC As New SqlCommand(tb.ToString, dbConn)
                Dim gr As SqlDataReader
                gr = gC.ExecuteReader
                If gr.HasRows Then
                    Dim fg As New FabGrades
                    fg.Grade = ""
                    fg.GradeDesc = "ALL"
                    retVal.Add(fg)
                End If
                While gr.Read
                    Dim fG As New FabGrades
                    fG.Grade = gr("Grade")
                    fG.GradeDesc = gr("Grade")
                    retVal.Add(fG)
                End While
            Catch ex As Exception
                Misc.LogError(ex)
                System.Web.HttpContext.Current.Trace.Write(ex.Message)
                System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Finally
                dbConn.Close()
            End Try

            Return retVal
        End Function


        Private Class ColorGroups
            Property ColorGroup As String
            Property ColorGroupDescription As String
        End Class


        Private Shared Function GetColors() As List(Of ColorGroups)
            Dim retVal As New List(Of ColorGroups)

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()

            Try
                'Dim RHFWebsiteID As String = CType(HttpContext.Current.User, CustomPrincipal).RHFWebsiteID


                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("select Distinct ")
                tb.AppendLine("	ColorGroup, ")
                tb.AppendLine("	ColorGroupDescription ")
                tb.AppendLine("FROM rpZFABInfo ")
                tb.AppendLine("where  ")
                tb.AppendLine("	Status not in ('DS','DR','MX','NS','PL','DX','11') ")
                tb.AppendLine("	AND ColorGroup is not null ")
                tb.AppendLine("	AND ColorGroupDescription is not null ")
                'tb.AppendLine(" AND MPG in " & GetFabMPGs(RHFWebsiteID) & " ")
                tb.AppendLine(" AND SOrg = 'HC01' ")
                tb.AppendLine("ORDER BY ColorGroupDescription ")
                Dim gC As New SqlCommand(tb.ToString, dbConn)
                Dim gr As SqlDataReader
                gr = gC.ExecuteReader
                If gr.HasRows Then
                    Dim fG As New ColorGroups
                    fG.ColorGroup = ""
                    fG.ColorGroupDescription = "ALL"
                    retVal.Add(fG)
                End If
                While gr.Read
                    Dim fG As New ColorGroups
                    fG.ColorGroup = gr("ColorGroup")
                    fG.ColorGroupDescription = gr("ColorGroupDescription")
                    retVal.Add(fG)
                End While
            Catch ex As Exception
                Misc.LogError(ex)
                System.Web.HttpContext.Current.Trace.Write(ex.Message)
                System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Finally
                dbConn.Close()
            End Try

            Return retVal
        End Function

        Private Class Patterns
            Property PatternName As String
            Property PatternDescription As String

        End Class
        Private Shared Function GetPatterns() As List(Of Patterns)
            Dim retVal As New List(Of Patterns)

            Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
            dbConn.Open()

            Try
                'Dim RHFWebsiteID As String = CType(HttpContext.Current.User, CustomPrincipal).RHFWebsiteID
                Dim tb As New System.Text.StringBuilder
                tb.AppendLine("select ")
                tb.AppendLine("	Distinct ")
                tb.AppendLine("	Class, ")
                tb.AppendLine("	ClassDescription ")
                tb.AppendLine("FROM rpZFABInfo ")
                tb.AppendLine("where  ")
                tb.AppendLine("	Status not in ('DS','DR','MX','NS','PL','DX','11') ")
                tb.AppendLine("	AND Class is not null ")
                tb.AppendLine("	AND ClassDescription is not null ")
               ' tb.AppendLine(" AND MPG in " & GetFabMPGs(RHFWebsiteID) & " ")
                'I know this sucks but knock leather out here. We putting it in the typelist.
                tb.AppendLine(" AND Class not in ('LTHR','FRNG','TASS','BRDR','BTTN','CORD','BULL') ")
                tb.AppendLine(" AND SOrg = 'HC01' ")
                tb.AppendLine("ORDER BY ClassDescription ")
                Dim gC As New SqlCommand(tb.ToString, dbConn)
                Dim gr As SqlDataReader
                gr = gC.ExecuteReader
                If gr.HasRows Then
                    Dim fG As New Patterns
                    fG.PatternName = ""
                    fG.PatternDescription = "ALL"
                    retVal.Add(fG)
                End If
                While gr.Read
                    Dim fG As New Patterns
                    fG.PatternName = gr("Class")
                    fG.PatternDescription = gr("ClassDescription")
                    retVal.Add(fG)
                End While
            Catch ex As Exception
                Misc.LogError(ex)
                System.Web.HttpContext.Current.Trace.Write(ex.Message)
                System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Finally
                dbConn.Close()
            End Try

            Return retVal
        End Function


        Public Shared Function GetFabMPGs(ByVal RHFWebsiteID As String)
            Dim retVal As String = ""

            Select Case RHFWebsiteID
                Case "Century"
                    retVal = " ('LF','RF','90') "
                Case "Highland"
                    retVal = " ('HF') "
                Case "Jessica"
                    retVal = " ('M9') "
                Case "Hancock"
                    retVal = " ('MF') "
                Case Else
                    retVal = " ('') "
            End Select

            Return retVal
        End Function

        Private Class PageNumber
            Property pNum As Integer

        End Class

        Private Shared Function GetPages(ByVal totalPages As Integer) As List(Of PageNumber)
            Dim p As New List(Of PageNumber)

            For t = 1 To totalPages
                Dim pg As New PageNumber
                pg.pNum = t
                p.Add(pg)
            Next

            Return p
        End Function

    End Class






End Namespace

