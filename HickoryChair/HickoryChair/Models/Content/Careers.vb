﻿Imports System.Data.SqlClient

Public Class Careers

    Public Property CareerList As List(Of Career)

    Public Sub New()
        CareerList = GetCareers()
    End Sub

    Public Sub New(ByVal jobid As String)
        CareerList = GetCareers(jobid)
    End Sub

    Public Function GetCareers(Optional ByVal jobid As String = "") As List(Of Career)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SCFI").ConnectionString.ToString)
        dbConn.Open()

        Dim CareerList As New List(Of Career)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Content/SQL/Careers.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@JobID", jobid)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each row In tSet.Tables(0).Rows
                    Dim NewCareer As New Career

                    NewCareer.JobID = row("JobID")
                    NewCareer.Qty = row("Qty")
                    NewCareer.Title = row("JobTitle")
                    NewCareer.Doc = "~/Documents/Careers/" & row("JobDescriptionFile")

                    CareerList.Add(NewCareer)
                Next
            End If
        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return CareerList
    End Function


    Public Class Career
        Public Property JobID As String
        Public Property Qty As String
        Public Property Title As String
        Public Property Doc As String
    End Class
End Class
