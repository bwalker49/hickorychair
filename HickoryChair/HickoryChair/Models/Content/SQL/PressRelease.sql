﻿SELECT 
PRName,
isnull(PRDesc, '') AS PRDesc,
PRLink,
isnull(MagMonth, '') AS MagMonth,
isnull(MagYear, '') AS MagYear
FROM PressRelease
WHERE WebsiteID = 'HickoryChair' And PRType = @prtype
ORDER BY MagYear DESC, MagMonth DESC