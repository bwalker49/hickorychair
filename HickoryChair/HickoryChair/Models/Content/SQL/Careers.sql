﻿SELECT
JobID,
Qty,
JobTitle,
JobDescriptionFile
FROM Careers
WHERE IsActive = 1
AND (nullif(@JobID, '') is null OR JobID = @JobID)
ORDER BY JobID