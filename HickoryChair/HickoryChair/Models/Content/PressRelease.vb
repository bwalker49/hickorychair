﻿Imports System.Data.SqlClient
Imports System.Globalization

Public Class PressRelease
    Public Property PressReleases As List(Of PressRelease)

    Public Sub New(ByVal presstype As String)
        PressReleases = GetPressReleases(presstype)
    End Sub

    Public Function GetPressReleases(ByVal presstype As String) As List(Of PressRelease)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim PressList As New List(Of PressRelease)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Content/SQL/PressRelease.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@prtype", presstype)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each row In tSet.Tables(0).Rows
                    Dim NewPR As New PressRelease

                    NewPR.Title = row("PRName")
                    NewPR.Text = row("PRDesc")
                    NewPR.Link = row("PRLink")
                    NewPR.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(row("MagMonth"))
                    NewPR.Year = row("MagYear")
                    NewPR.CoverImg = row("MagMonth") & row("MagYear")

                    PressList.Add(NewPR)
                Next
            End If
        Catch ex As Exception
            'MiscCode.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return PressList
    End Function

    Public Class PressRelease
        Public Property Title As String
        Public Property Text As String
        Public Property Link As String
        Public Property Month As String
        Public Property Year As String
        Public Property CoverImg As String
    End Class
End Class
