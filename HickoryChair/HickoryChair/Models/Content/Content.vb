﻿Public Class Content

    Public Property CurrContentPage As ContentPage

    Public Sub New(ByVal page As String)
        CurrContentPage = GetContentInfo(page)
    End Sub

    Public Function GetContentInfo(ByVal page As String)
        Dim newpage As New ContentPage

        If page = "CustomersOwn" Then
            newpage.PageTitle = "Customer's Own Hardware and Veneer"
            newpage.PageImg = "~/Images/layout/ContentPageImages/CustomersOwn.jpg"
            newpage.ContentTitle = "A One-Of-A-Kind <i>Hardware and Veneer Program</i>"
            newpage.ContentText = "<p>There’s no better example of Hickory Chair’s accommodating spirit than our Customer’s Own Hardware® program. Whether you use your own vintage knobs or antique pulls, or you pick from our extensive inventory, the artisans in our North Carolina workroom make it easy to make your furniture truly personal.</p><p>In response to wishes from Hickory Chair enthusiasts, we offer options in the actual species that some of our pieces are made from.  The 1911 Collection is offered in your choice of fine Mahogany or American Ash veneers and solids.  Most of our M2M® Made to Measure tables are designed with a choice five different veneer species for the top.  Perhaps you would like us to upholster the back panel of a display cabinet with your wallpaper, fabric or a mirror as a special construction.  Perhaps you would like us to replace the standard glass with mirrors or wood panels in a china cabinet.  Either way, it is Customer’s Own Veneer® by Hickory Chair. </p>"
        ElseIf page = "CentennialStory" Then
            newpage.PageTitle = "Our Centennial Story"
            newpage.PageImg = "~/Images/layout/ContentPageImages/PastPresent.jpg"
            newpage.ContentTitle = "Timeless Style <i>since 1911</i>"
            newpage.ContentText = "<p>The Hickory Chair Furniture Co. began one hundred and ten years ago in Hickory, North Carolina with a single product and a simple vision. The product was a made-to-order dining chair. The vision was to combine the authenticity of classic craftsmanship with the efficiency of modern manufacturing. In the decades since, our product range has evolved into an assortment of timeless designs, drawn from significant periods and places, and from the talents of some of the world’s most respected designers.</p><p>Our original vision, however, remains unchanged. Our company is still guided by a genuine commitment to custom craftsmanship and timely service. Our style is still distinguished by an authentic, timeless spirit. And our furniture is still made to order and made to last, by a team of talented and attentive artisans. We are proud that almost 90% of our furnishings are made in our Hickory, North Carolina workroom.</p>"
        ElseIf page = "Snapshot" Then
            newpage.PageTitle = "Historic Hickory Snapshot"
            newpage.PageImg = "~/Images/layout/ContentPageImages/Snapshot.jpg"
            newpage.ContentTitle = "<i>1959</i>"
            newpage.ContentText = "<p>An upholsterer carefully seams the back of a handmade loveseat.  This time-honored practice of meticulous handwork is still practiced today on all of Hickory's fine upholstery.</p>"
        ElseIf page = "ArtisanalUpholstery" Then
            newpage.PageTitle = "Artisanal Upholstery"
            newpage.PageImg = "~/Images/layout/ContentPageImages/ArtisanalUpholstery.jpg"
            newpage.ContentTitle = "Your unique style <i>executed by our talented artisans.</i>"
            newpage.ContentText = "<p>Our craftsmen take great pride in accommodating a wide range of requests to enhance and personalize our upholstery designs. They also spend hours on details that few will ever see: custom-engineering each seat to ensure it provides years of consistent, luxurious comfort. From pillow and cushion options to nail head trim, swivel bases to bullion fringes, we don’t just allow customers to personalize our timeless upholstery, we encourage it.</p>"
        ElseIf page = "Woodworking" Then
            newpage.PageTitle = "Classic, Custom Woodworking"
            newpage.PageImg = "~/Images/layout/ContentPageImages/Woodworking.jpg"
            newpage.ContentTitle = "Made for generations <i>by generations of craftspeople.</i>"
            newpage.ContentText = "<p>Hickory Chair Furniture Co.’s high-quality casegoods still begin as they did decades ago: with thoughtfully harvested and carefully milled wood from around the world. These fine materials are then sanded, shaped and assembled in our workshops, where traditional techniques are applied alongside today’s leading technology. Multi-layer finishes are created by hand, for example, while veneers are precision-cut using state-of-the-art lasers. The result is fine wood furniture that reflects the hand of a craftsperson, the quality standards of an industry leader, and the personal vision of each and every customer.</p>"
        ElseIf page = "Environment" Then
            newpage.PageTitle = "Environmental Responsibility"
            newpage.PageImg = "~/Images/layout/ContentPageImages/Environment.jpg"
            newpage.ContentTitle = "Responsibility to the <i>Environment</i>"
            newpage.ContentText = "<p>A great deal has changed during our first one hundred years. But like our handcrafted quality and timeless style, our commitment to responsible business practices has not wavered over the last century. In fact, we continue to develop new ways to empower consumers and employees, while minimizing our long-term impact on the environment.</p>"
        ElseIf page = "Edge" Then
            newpage.PageTitle = "Employee Empowerment (Edge)"
            newpage.PageImg = "~/Images/layout/ContentPageImages/Edge.jpg"
            newpage.ContentTitle = "Responsibility to <i>Employees</i>"
            newpage.ContentText = "<p>Just as customers are invited to personalize our fine furniture, our employees are encouraged to customize processes to ensure maximum value, efficiency and quality. The model and culture, which is rooted in the collaborative spirit of a classic furniture workroom, was formalized in 1996 as a ground-breaking program called EDGE (Employees Dedicated to Growth and Excellence).</p><p>In the last ten years alone, hundreds of EDGE teams have suggested and implemented thousands of improvements. The result is a best-in-class capacity for quick response to everything from customer requests to manufacturing challenges. A few of the many success stories of the employee empowerment efforts include:</p><ul><li>Cross-functional teams reducing lead times to a few weeks instead of months — the fastest in the category.</li><li>Resourceful, responsive artisans developing a scalable strategy for adding on-demand customization options.</li><li>Company-wide commitment to safety, quality and profitability, with results that set pace for the industry.</li></ul>"
        ElseIf page = "Monogramming" Then
            newpage.PageTitle = "Monogramming"
            newpage.PageImg = "~/Images/layout/ContentPageImages/Monogram.jpg"
            newpage.ContentTitle = "Our furniture. With your name all over it."
            newpage.ContentText = "<p>The fact that monograms are one of our personalization options is a testament to the care and precision that goes into every stage of our upholstery production. Indeed, these are not just items of upholstery; they are hand-made heirlooms to be passed down for generations.</p><p><a href='/Documents/Resources/MonogramProgram.pdf' target='_blank' style='color:black;'><i>Click here to view PDF.</i></a>"
        ElseIf page = "ArtistStudio" Then
            newpage.PageTitle = "Artist's Studio"
            newpage.PageImg = "~/Images/layout/ContentPageImages/ArtistStudio.jpg"
            newpage.ContentTitle = "Artist's <i>Studio</i>"
            newpage.ContentText = "<p>Just as Hickory Chair’s custom finish and upholstery options have evolved and expanded in response to years of special requests, the company’s on-site studio of award-winning artists have also been inspired by designer’s requests to broaden their repertoire of hand-painted accents. Now available as an optional application on an inspiring range of pieces, our remarkable Artist’s Studio program makes hand painted custom decoration as easy to specify as our other personalization options. From textile-inspired patterns on chests to classic trompe l’oeil follies on cabinets, designs can be scaled and modified to suit your tastes in the palette of your choice. An assortment of motifs have been designed as a starting point for your one-of-a- kind painted piece, but our talented artists are also happy to work with you individually — from interpreting your design inspiration to executing samples and strike offs — to apply your artistic vision to our hand-crafted furniture.</p><p><a href='/Products/ShowResults?WildCard=Artist%20Studio&SearchName=Artist%27s%20Studio'>View some of our Artist Studio items.</a></p>"
        ElseIf page = "NailTrim" Then
            newpage.PageTitle = "Nail Trim"
            newpage.PageImg = "~/Images/layout/ContentPageImages/NailTrim.jpg"
            newpage.ContentTitle = "Nailhead <i>trim</i>"
            newpage.ContentText = "<p>Available in a range of shapes and sizes, our nailhead trims are a striking way to accent our stylish upholstery.</p><p><a href='/Resources/Nailhead'>Click here for our library of nailhead trims.</a></p><p><a href='/Documents/Resources/NailTrim.pdf' target='_blank'>Click here to view PDF.</a></p>"
        ElseIf page = "Striping" Then
            newpage.PageTitle = "Striping Colors"
            newpage.PageImg = "~/Images/layout/ContentPageImages/Striping.jpg"
            newpage.ContentTitle = "Outline and accent with hand-painted striping."
            newpage.ContentText = "<p>Hand-applied striping is available on any Hickory items that are offered in our wide range of optional finishes. Twelve striping colors are featured in two techniques: The Antique Rub technique features translucent rub-through in areas that reveal the underlying finish while the Solid technique is applied evenly. Please note that striping colors may look different on each base finish. Solid Striping is a solid color stripe applied evenly. Antique Rub Striping is a solid color stripe with random areas that have been rubbed through revealing the underlying finish. Solid striping is standard.</p><p><a href='/Documents/Resources/StripingColors.pdf' target='_blank'>Click here to view PDF.</a></p>"
        End If

        Return newpage
    End Function


    Public Class ContentPage
        Public Property PageTitle As String
        Public Property PageImg As String
        Public Property ContentTitle As String
        Public Property ContentText As String
    End Class


End Class
