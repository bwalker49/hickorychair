﻿--DECLARE @TypeID AS varchar(10)
--DECLARE @SubTypeID AS varchar(10)
--DECLARE @CollectionID AS varchar(10)
--DECLARE @DesignerID AS varchar(10)
--DECLARE @WildCard AS varchar(20)
--DECLARE @RoomID AS varchar(20)

--SET @TypeID = ''
--SET @SubTypeID = ''
--SET @CollectionID = ''
--SET @DesignerID = ''
--SET @WildCard = ''
--SET @RoomID = ''

--Leave with Query
Declare @WidCard4SQL as varchar(50) = null
if @WildCard <> ''
BEGIN
	set @WidCard4SQL = '%' + replace(@WildCard,'*','') + '%';
END

SELECT 
ig.SKU,
isnull(t.TypeName, '') As TypeName,
isnull(s.SubTypeName, '') As SubTypeName,
isnull(i.Name, '') AS Name,
isnull(i.WebRomance, '') AS LongDesc,
CASE 
WHEN i.Image IS NOT NULL THEN
i.Image
WHEN i.ImageAlt IS NOT NULL THEN
i.ImageAlt
ELSE
''
END AS Image,
its.QtyInStock,
isnull(minWidth, 0) As minWidth,
isnull(maxWidth, 0) As maxWidth,
isnull(minDepth, 0) As minDepth,
isnull(maxDepth, 0) As maxDepth,
isnull(minHeight, 0) As minHeight,
isnull(maxHeight, 0) As maxHeight,
isnull(Designer_Name, '') AS Designer
FROM ItemsGoodToShowHCConsumer ig
INNER JOIN Items i ON i.SKU = ig.SKU
LEFT JOIN Types t ON i.WebTypeID = t.TypeID
LEFT JOIN SubTypes s ON i.SubTypeID = s.SubTypeID
LEFT JOIN Items_Status its ON ig.SKU = its.SKU
LEFT JOIN Items_Designers_2 d ON i.Designer = d.Designer_Code AND Designer_Type = 'DESIGNER_1'
WHERE (i.WebTypeID IN (select value from STRING_SPLIT(@TypeID,',')) OR nullif(@TypeID,'') is null)
AND (i.SubTypeID IN (select value from STRING_SPLIT(@SubTypeID,',')) OR nullif(@SubTypeID,'') is null)
AND (i.CollectionNo IN (select value from STRING_SPLIT(@CollectionID,',')) OR nullif(@CollectionID,'') is null)
AND (i.RoomID IN (select value from STRING_SPLIT(@RoomID,',')) OR nullif(@RoomID,'') is null)
AND (i.Designer IN (select value from STRING_SPLIT(@DesignerID,',')) OR nullif(@DesignerID,'') is null)
AND (i.PRODUCT_LINE_3 IN (select value from STRING_SPLIT(@ProductLine,',')) OR nullif(@ProductLine,'') is null)


AND (
	@WidCard4SQL is null 
     OR i.sku like @WidCard4SQL 
	 OR i.MatlDescription like @WidCard4SQL 
	 OR i.Name like @WidCard4SQL
	 OR (
			i.WebRomance like @WidCard4SQL 
			and @WidCard4SQL = '%Artist Studio%'
		)
	)
