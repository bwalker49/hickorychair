﻿SELECT 
igts.SKU,
isnull(i.Name, '') AS Name,
isnull(i.WebTypeID, '') As WebTypeID,
isnull(t.TypeName, '') As TypeName,
isnull(s.SubTypeID, '') As SubTypeID,
isnull(i.Description, '') AS Description,
isnull(i.WebRomance, '') AS LongDesc,
isnull(i.Notes, '') AS Notes,
isnull(c.CollectionType, '') As CollectionType,
isnull(i.CollectionNo, '') As CollectionNo,
isnull(c.CollectionName, '') AS CollectionName,
CASE 
WHEN i.Image IS NOT NULL THEN
i.Image
WHEN i.ImageAlt IS NOT NULL THEN
i.ImageAlt
ELSE
''
END AS Image,
isnull(i.ExtraImages, '') As ExtraImages,
CAST(isnull(minWidth, 0) AS float) AS minWidth,
CAST(isnull(maxWidth, 0) AS float) AS maxWidth,
CAST(isnull(minDepth, 0) AS float) AS minDepth,
CAST(isnull(maxDepth, 0) AS float) AS maxDepth,
CAST(isnull(minHeight, 0) AS float) AS minHeight,
CAST(isnull(maxHeight, 0) AS float) AS maxHeight,
isnull(InsideWidth, 0) As InsideWidth,
isnull(InsideDepth, 0) As InsideDepth,
isnull(InsideHeight, 0) As InsideHeight,
isnull(SeatHeight, 0) As SeatHeight,
isnull(ArmHeight, 0) As ArmHeight,
isnull(ComYardage, 0) AS COM,
isnull(Leather, '') AS Leather,
isnull(ist.GrossWeight, 0) AS Weight,
isnull(ist.Volume, 0) AS Volume,
isnull(FINISH_TYPE_1,'') as FINISH_TYPE_1,
isnull(UPHOLSTERY_1, '') AS UPHOLSTERY_1,
isnull(ist.Status,'') as Status,
isnull(ist.QtyInStock,0) as QtyInStock,
isnull(iSD.ATPQty,0) as ATPQty,
isnull(iSD.NextATPQty,0) as NextATPQty,
--isnull(NumCartons, '') AS Cartons,
--ist.QtyInStock
CASE WHEN EXISTS (SELECT SKU FROM WishListItems WHERE wID = (SELECT wID FROM WishList WHERE UserID = @user) AND SKU = @sku) THEN 1
ELSE 0
END AS InWishList
--isnull(d.DesignerName, '') As Designer
FROM ItemsGoodToShowHCConsumer igts
INNER JOIN Items i ON i.SKU = igts.SKU
LEFT JOIN Items_Status ist on i.sku=ist.sku
LEFT JOIN Items_StockDate iSD on i.SKU=iSD.SKU
LEFT JOIN Collections c ON c.CollectionNo = i.CollectionNo
--LEFT JOIN HCWeb.dbo.Designers d ON i.Designer = d.DesignerID
LEFT JOIN Types t ON i.WebTypeID = t.TypeID
LEFT JOIN SubTypes s ON i.SubTypeID = s.SubTypeID
WHERE igts.SKU = @sku

SELECT
x.SKU,
x.IconID,
ii.IconName,
isnull(ii.IconDesc,'') as IconDesc
FROM Items_Icon_SKU_Xref x
INNER JOIN Items_Icon ii on ii.IconID=x.IconID and x.WebsiteID=ii.WebsiteID
WHERE
x.SKU=@SKU
AND x.WebsiteID=@WebsiteID

SELECT
AlsoAvailableAsSKU,
(SELECT Name FROM Items WHERE SKU = AlsoAvailableAsSKU) AS Name
FROM Items_AlsoAvailableAs
WHERE SKU = @SKU