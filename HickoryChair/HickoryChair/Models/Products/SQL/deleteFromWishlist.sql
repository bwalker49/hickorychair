﻿IF EXISTS (SELECT SKU FROM WishListItems WHERE SKU = @sku AND wID = (SELECT wID FROM WishList WHERE UserID = @user AND WebsiteID = 'HickoryChair'))
BEGIN
DELETE FROM WishListItems WHERE wID = (SELECT wID FROM WishList WHERE UserID = @user AND WebsiteID = 'HickoryChair') AND SKU = @sku
SELECT 'Success' AS Deleted
END