﻿IF NOT EXISTS (SELECT wName FROM WishList WHERE UserID = @wID) 
BEGIN

INSERT INTO WishList(UserID, wName, CreatedOn, Comments, WebsiteID)
VALUES (@wID, 'My WishList', GETDATE(), '', 'HickoryChair');

INSERT INTO WishListItems(wID, sku, CreatedOn, WebsiteID)
VALUES ((SELECT wID FROM WishList WHERE UserID = @wID), @sku, GETDATE(), 'HickoryChair')

SELECT @@ROWCOUNT AS Updated

END

ELSE

IF NOT EXISTS (SELECT sku FROM WishListItems WHERE wID = (SELECT wID FROM WishList WHERE UserID = @wID) AND sku = @sku)
INSERT INTO WishListItems(wID, sku, CreatedOn, WebsiteID)
VALUES ((SELECT wID FROM WishList WHERE UserID = @wID), @sku, GETDATE(), 'HickoryChair')

SELECT @@ROWCOUNT AS Updated