﻿SELECT 
wli.SKU,
t.TypeName,
--s.SubTypeName,
isnull(i.Name, '') AS Name,
isnull(i.WebRomance, '') AS LongDesc,
CASE 
WHEN i.Image IS NOT NULL THEN
i.Image
WHEN i.ImageAlt IS NOT NULL THEN
i.ImageAlt
ELSE
''
END AS Image,
its.QtyInStock,
minWidth,
maxWidth,
minDepth,
maxDepth,
minHeight,
maxHeight,
isnull(DesignerName, '') AS Designer
FROM WishListItems wli
INNER JOIN ItemsGoodToShowHCConsumer ig ON ig.SKU = wli.SKU
INNER JOIN Items i ON wli.sku = i.sku
INNER JOIN Types t ON i.TypeID = t.TypeID
--INNER JOIN SubTypes s ON i.SubTypeID = s.SubTypeID
INNER JOIN Items_Status its ON ig.SKU = its.SKU
LEFT JOIN HCWeb.dbo.Designers d ON i.Designer = d.DesignerID
WHERE wID = (SELECT wID FROM WishList WHERE UserID = @user)