﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Services
Imports System.IO

Public Class CustomProgram
    Public Property CustomProgramOutput As String

    Public Class options
        Public Name As String
        Public Code As String
        Public image As String
        Public cushionType As String
        Public baseType As String
        Public backType As String
    End Class

    Public Class arminfo
        Public armtext As String
        Public availablearms As String
        Public hastcushion As Boolean
        Public hasstraightcushion As Boolean
    End Class

    Public Sub New(ByVal program As String)
        CustomProgramOutput = buildSourceProgram(program)
    End Sub

    <WebMethod()>
    Public Shared Function buildSourceProgram(ByVal subcoll As String) As String
        Dim retVal As String = ""
        Dim tSet As New DataSet
        tSet = getProgramDetails(subcoll)

        Try
            Dim name As String = tSet.Tables(2).Rows(0).Item("Name").ToString.Replace(" ", "").Replace("&", "")
            Dim origname As String = tSet.Tables(2).Rows(0).Item("Name")
            Dim description As String = tSet.Tables(2).Rows(0).Item("Description")

            Dim categories As DataTable = tSet.Tables(4)

            Dim dimensionsDirectory As New System.IO.DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("/" & name.ToString.Replace(" ", "").Replace("&", "").ToLower & "/documents/dimensions/"))

            Dim sb As New System.Text.StringBuilder

            'sb.AppendLine("<div style='position: relative; background: linear-gradient(rgba(20, 20, 20, 0.5), rgba(20, 20, 20, 0.5)), url(/Source/" & name.ToString.Replace(" ", "").Replace("&", "").ToLower & "/images/program-background-img.jpg); background-size: contain; background-repeat: no-repeat; width:  100%; height: 0; padding-top: 34.3%; border-bottom: 3px solid white;' >")

            'sb.AppendLine("<div class='source-logo-wrapper'>")
            'sb.AppendLine("<div class='source-logo-text'>" & origname.ToUpper & "</div>")
            'sb.AppendLine("<div class='source-logo-letter'><span></span><span>" & origname.Substring(0, 1).ToUpper & "</span></div>")
            'sb.AppendLine("</div>")
            'sb.AppendLine("</div>")


            'sb.AppendLine("<div style='display:none;' data-id='" & name & "'>" & buildProgramGallery(name) & "</div>")

            sb.AppendLine("<div id='program-top-wrapper'>")
            sb.AppendLine("<div id='program-top-img-wrapper'>")
            sb.AppendLine("<img style='width:100%;' src ='/" & name.ToString.Replace(" ", "").Replace("&", "").Replace("/", "").ToLower & "/images/program-background-img.jpg'/>")
            sb.AppendLine("</div>")
            sb.AppendLine("<div id='program-top-text-wrapper'>")
            sb.AppendLine("<div id='mfy-tagline'><img src='/Images/layout/MFY_Logo_Black.jpg'></div>")
            sb.AppendLine("<div id='program-top-name'>" & origname.ToUpper & "</div>")
            sb.AppendLine("<div id='program-top-description'>" & description & "</div>")
            If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/Documents/Tear-Sheets/ProgramDocs/" & name.ToString.Replace(" ", "").Replace("&", "").Replace("/", "").ToLower & ".pdf")) Then
                sb.AppendLine("<a href='/documents/tear-sheets/programdocs/" & name.ToString.Replace(" ", "").Replace("&", "").Replace("/", "").ToLower & ".pdf' style='padding:10px 15px;border:1px solid black;margin-bottom:15px;width:143px;' target='_blank'>View Program</a>")
            End If

            If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/Documents/Tear-Sheets/" & name.ToString.Replace(" ", "").Replace("&", "").Replace("/", "").ToLower & ".pdf")) Then
                sb.AppendLine("<a href='/documents/tear-sheets/" & name.ToString.Replace(" ", "").Replace("&", "").Replace("/", "").ToLower & ".pdf' style='padding:10px 15px;border:1px solid black;' target='_blank'>View Tear Sheet</a>")
            End If
            'If galleryCount(name) > 2 Then
            '    sb.AppendLine("<div id='program-top-inspiration'><a href='/Source/" & name & "/#" & name & "-1'>View Gallery</a></div>")
            'ElseIf galleryCount(name) = 0 Then

            'Else
            '    sb.AppendLine("<div id='program-top-inspiration'><a href='/Source/" & name & "/#" & name & "'>View Gallery</a></div>")
            'End If

            sb.AppendLine("</div>")
            sb.AppendLine("</div>")

            sb.AppendLine("<div Class='source-section'>")
            sb.AppendLine("<div Class='source-section-title'>OPTIONS<div style='font-size:9pt;font-weight:bold;'>")
            sb.AppendLine("</div>")
            sb.AppendLine("</div>")
            sb.AppendLine("")
            sb.AppendLine("<div id='tabs'>")
            sb.AppendLine("<ul>")

            For Each category In categories.Rows
                If category("Category") = "FINISH" Then
                    sb.AppendLine("<li><a href='#" & category("Category") & "' class='tab'>" & category("Category") & "ES</a></li>")
                ElseIf category("Category") = "STRIPING" Or category("Category") = "HARDWARE" Then
                    sb.AppendLine("<li><a href='#" & category("Category") & "' class='tab'>" & category("Category") & "</a></li>")
                Else
                    If category("TabOverride") <> "" Then
                        If category("TabOverride") <> "HARDWARE" Or category("TabOverride") <> "PERSONAL PREFERENCE CHART" Then
                            sb.AppendLine("<li><a href='#" & category("Category") & "' class='tab'>" & category("TabOverride") & "S</a></li>")
                        Else
                            sb.AppendLine("<li><a href='#" & category("Category") & "' class='tab'>" & category("TabOverride") & "</a></li>")
                        End If
                    Else
                            sb.AppendLine("<li><a href='#" & category("Category") & "' class='tab'>" & category("Category") & "S</a></li>")
                    End If

                End If
            Next

            'If dimensionsDirectory.GetFiles.Count > 0 Then
            '    sb.AppendLine("<li><a href='#dimensions' class='tab'>Dimensions</a></li>")
            'End If

            sb.AppendLine("</ul>")

            For Each category In categories.Rows
                sb.AppendLine("<div id='" & category("Category") & "'>")
                If category("StepDescription") <> "" Then
                    sb.AppendLine("<div id='stepdescription'>" & category("StepDescription") & "</div>")
                End If
                sb.AppendLine(buildOptionSection(subcoll, name, category("Category")))
                sb.AppendLine("</div>")
            Next

            'If dimensionsDirectory.GetFiles.Count > 0 Then
            '    sb.AppendLine("<div id='dimensions'>")
            '    For Each file In dimensionsDirectory.GetFiles
            '        sb.AppendLine("<div class='dimension-img'><img src='/" & name & "/documents/dimensions/" & file.Name & "'/></div>")
            '    Next
            '    sb.AppendLine("</div>")
            'End If

            sb.AppendLine("</div>")



            sb.AppendLine("</div>")
            retVal = sb.ToString

        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Misc.LogError(ex)
        End Try

        Return retVal
    End Function

    Public Shared Function getItems(ByVal subcoll As String) As DataSet
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As New DataSet

        Try
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT   ")
            sb.AppendLine("ig.SKU,  ")
            sb.AppendLine("Name,  ")
            sb.AppendLine("CASE WHEN Image <> '' THEN  ")
            sb.AppendLine("	Image  ")
            sb.AppendLine("ELSE  ")
            sb.AppendLine("	ImageAlt  ")
            sb.AppendLine("END AS Image,  ")
            sb.AppendLine("DisplayCode, ")
            sb.AppendLine("DisplayOrder ")
            sb.AppendLine("FROM ItemsGoodToShowConsumer ig ")
            sb.AppendLine("LEFT JOIN Source_Items sc ON ig.SKU = sc.SKU ")
            sb.AppendLine("WHERE ig.SKU IN (SELECT SKU FROM Source_Items WHERE Program_ID = @subcoll )  ")
            sb.AppendLine("ORDER By DisplayCode, DisplayOrder")


            sb.AppendLine("SELECT DISTINCT DisplayCode FROM Source_Items WHERE Program_ID = @subcoll")


            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@subcoll", subcoll)
            tA.Fill(retVal)

        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return retVal

    End Function

    Public Shared Function getProgramDetails(ByVal subcoll As String) As DataSet
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As New DataSet

        Try
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT   ")
            sb.AppendLine("sc.SKU,  ")
            sb.AppendLine("CASE  ")
            sb.AppendLine("WHEN sc.Name Is Not NULL THEN ")
            sb.AppendLine("sc.Name ")
            sb.AppendLine("Else ")
            sb.AppendLine("i.Name ")
            sb.AppendLine("End As Name, ")
            'sb.AppendLine("CASE WHEN Image <> '' THEN  ")
            'sb.AppendLine("	Image  ")
            'sb.AppendLine("ELSE  ")
            'sb.AppendLine("	ImageAlt  ")
            'sb.AppendLine("END AS Image,  ")
            sb.AppendLine("DisplayCode, ")
            sb.AppendLine("DisplayOrder ")
            sb.AppendLine("FROM Source_Items sc ")
            sb.AppendLine("LEFT JOIN Items i ON i.SKU = sc.SKU ")
            sb.AppendLine("WHERE Program_ID = @subcoll  ")
            'sb.AppendLine("ORDER By DisplayCode, DisplayOrder;")

            sb.AppendLine("SELECT DISTINCT sx.DisplayCode, spo.DisplayOrder FROM Source_Items sx INNER JOIN Source_Program_Options spo ON sx.DisplayCode = spo.OptionID WHERE Program_ID = @subcoll ORDER BY spo.DisplayOrder, sx.DisplayCode")

            sb.AppendLine("SELECT  ")
            sb.AppendLine("Name, ")
            sb.AppendLine("isnull(Description, '') AS Description, ")
            sb.AppendLine("TearSheetLink")
            sb.AppendLine("FROM Source_Programs ")
            sb.AppendLine("WHERE ID = @subcoll;")

            sb.AppendLine("SELECT ")
            sb.AppendLine("spod.OptionID, ")
            sb.AppendLine("Name, ")
            sb.AppendLine("spod.Category, ")
            sb.AppendLine("isnull(Type, '') AS Type ")
            sb.AppendLine("FROM Source_Program_Options spo ")
            sb.AppendLine("LEFT JOIN Source_Program_Option_Description spod ON spo.OptionID = spod.OptionID AND spo.Category = spod.Category  ")
            sb.AppendLine("WHERE ID = @subcoll ")
            sb.AppendLine("ORDER BY Category, Type ;")

            sb.AppendLine("SELECT Category, isnull(TabOverride, '') AS TabOverride, isnull(StepDescription, '') AS StepDescription FROM Source_Program_Category_Order WHERE ID=@subcoll ORDER BY DisplayOrder")


            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@subcoll", subcoll)
            tA.Fill(retVal)
        Catch ex As Exception
            System.Web.HttpContext.Current.Trace.Write(ex.Message)
            System.Web.HttpContext.Current.Trace.Write(ex.StackTrace)
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return retVal

    End Function

    '<WebMethod()>
    Public Shared Function buildTabs(ByVal id As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Dim sb As New System.Text.StringBuilder
        sb.AppendLine("<ul>")
        sb.AppendLine("<li><a href='#tabs-1'> Frames</a></li>")
        sb.AppendLine("<li><a href='#tabs-2'> Arms</a></li>")
        sb.AppendLine("<li><a href='#tabs-3'> Bases</a></li>")
        sb.AppendLine("</ul>")

        sb.AppendLine("<div id='tabs-1'>")
        sb.AppendLine("Bacon ipsum dolor amet meatloaf meatball tongue turducken buffalo frankfurter flank brisket drumstick jowl bacon pastrami. Turducken bacon ham hock chicken prosciutto tongue jerky porchetta biltong rump cow Short ribs strip steak meatball. Meatball pork belly tongue swine buffalo pastrami salami cow shankle doner rump alcatra ham. Venison pig buffalo shank andouille burgdoggen corned beef pork chop bresaola. Boudin beef jerky ham kevin, corned beef turkey leberkas frankfurter alcatra filet mignon andouille tri-tip. Buffalo biltong pork belly pork loin venison.")
        sb.AppendLine("</div>")

        sb.AppendLine("<div id='tabs-2'>")
        sb.AppendLine("Picanha Short ribs hamburger, doner Short loin tongue brisket chicken beef andouille t-bone alcatra tail. Boudin biltong corned beef alcatra cow, turkey prosciutto flank salami pancetta sausage capicola. Jowl ham hock salami picanha pork belly bresaola. Pig bresaola salami swine boudin corned beef meatball leberkas beef turducken andouille fatback. Pork belly shank picanha corned beef, Short loin ribeye chicken venison.")
        sb.AppendLine("</div>")

        sb.AppendLine(" <div id='tabs-3'>")
        sb.AppendLine("Landjaeger spare ribs bacon alcatra, cow hamburger pig t-bone chuck flank salami Short ribs. Chicken landjaeger Short ribs shoulder. Pancetta kevin alcatra, shank tri-tip porchetta kielbasa capicola pastrami meatball shankle rump strip steak burgdoggen pig. Turkey cupim boudin alcatra strip steak tri-tip t-bone. Venison ribeye spare ribs pastrami.")
        sb.AppendLine("</div>")



        Return retVal
    End Function

    <WebMethod()>
    Public Shared Function buildOptionSection(ByVal id As String, ByVal collname As String, ByVal selection As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Dim sb As New System.Text.StringBuilder
        sb.AppendLine("SELECT  ")
        sb.AppendLine("spo.Category, ")
        sb.AppendLine("spo.OptionID, ")
        sb.AppendLine("spod.Name, ")
        sb.AppendLine("CASE ")
        sb.AppendLine("WHEN spo.AppendType Is Not NULL And spod.Type Is Not NULL THEN ")
        sb.AppendLine(" spo.AppendType + ' ' + spod.Type ")
        sb.AppendLine("WHEN spo.AppendType IS NOT NULL THEN")
        sb.AppendLine(" spo.AppendType")
        sb.AppendLine("ELSE ")
        sb.AppendLine(" isnull(spod.Type, '') ")
        sb.AppendLine("END AS Type,  ")
        sb.AppendLine("isnull(spo.Note, '') AS Note,")
        sb.AppendLine("DisplayOrder, ")
        sb.AppendLine("isnull(Link, '') AS Link")
        sb.AppendLine("FROM Source_Program_Options spo ")
        sb.AppendLine("LEFT JOIN Source_Program_Option_Description spod ON spo.OptionID = spod.OptionID AND spo.Category = spod.Category  ")
        sb.AppendLine("WHERE ID = @id ")
        If selection = "FRAME" Then
            sb.AppendLine("ORDER BY DisplayOrder, OptionID, Type")
        Else
            sb.AppendLine("ORDER BY Type, DisplayOrder, Name ")
        End If

        sb.AppendLine("SELECT  ")
        sb.AppendLine("sx.SKU, ")
        sb.AppendLine("CASE WHEN sx.Name IS NOT NULL THEN ")
        sb.AppendLine("sx.Name ")
        sb.AppendLine("ELSE ")
        sb.AppendLine("(SELECT Name FROM Items WHERE SKU = sx.SKU) ")
        sb.AppendLine("END AS Name,")
        sb.AppendLine("isnull((SELECT Image FROM Items WHERE SKU = sx.SKU), '') AS Image,")
        'sb.AppendLine("isnull(minWidth, 0) AS minWidth, ")
        'sb.AppendLine("isnull(maxWidth, 0) AS maxWidth, ")
        'sb.AppendLine("isnull(minDepth, 0) AS minDepth, ")
        'sb.AppendLine("isnull(maxDepth, 0) AS maxDepth, ")
        'sb.AppendLine("isnull(minHeight, 0) AS minHeight, ")
        'sb.AppendLine("isnull(maxHeight, 0) AS maxHeight, ")
        sb.AppendLine("isnull(DisplayCode, '') AS DisplayCode, ")
        sb.AppendLine("DisplayOrder, ")
        sb.AppendLine("isnull(Note, '') AS Note")
        sb.AppendLine("FROM Source_Items sx ")
        'sb.AppendLine("INNER JOIN Items i ON sx.SKU = i.SKU")
        sb.AppendLine("WHERE Program_ID = @id ")
        sb.AppendLine("ORDER BY DisplayCode, len(DisplayOrder), DisplayOrder ")

        Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
        tA.SelectCommand.Parameters.AddWithValue("@id", id)

        Dim tSet As New DataSet
        tA.Fill(tSet)

        Dim selectedOptions As DataRow() = tSet.Tables(0).Select("Category = '" & selection & "'")
        Dim prevType As String = ""
        Dim optionFolder As String = ""
        Dim i As Integer = 0

        collname = collname.Replace("/", "")

        Dim di As New DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/"))
        Dim fiArr As DirectoryInfo() = di.GetDirectories()

        For Each folder In fiArr
            If folder.Name.Contains(selection.ToLower) Then
                optionFolder = folder.Name
            End If
        Next

        Dim tb As New System.Text.StringBuilder

        If selectedOptions.Count > 0 Then
            For Each opt In selectedOptions
                If selection = "FRAME" Then
                    tb.AppendLine("<div class='program-option-type'>" & opt("Name").ToString.ToUpper)
                    If opt("Note") <> "" Then
                        tb.AppendLine("<div style='margin:10px;font-size:11pt;color:gray;'>(" & opt("Note") & ")</div>")
                    End If
                    tb.AppendLine("</div>")


                    For Each row In tSet.Tables(1).Select("DisplayCode = '" & opt("OptionID") & "'")
                        tb.AppendLine("<div class='program-option'>")
                        If isSKU(row("SKU")) Then
                            tb.AppendLine("<a href='/Products/ProductDetails/" & row("SKU") & "'>")
                        Else
                            tb.AppendLine("<a href='/Products/ShowResults/?WildCard=" & row("SKU") & "-*'>")
                        End If
                        'tb.AppendLine("<a href='/product-detail.aspx?sku=" & row("SKU") & "&section='>")
                        If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/frames/" & row("SKU") & ".jpg")) Then
                            tb.AppendLine("<img src='/" & collname & "/images/frames/" & row("SKU") & ".jpg'/>")
                        ElseIf row("Image") <> "" Then
                            tb.AppendLine("<img src='/prod-images/" & row("Image") & "_small.jpg' height='150'/>")
                        Else
                            tb.AppendLine("<img src='/Images/layout/no_image_small.jpg' style='opacity:.35;'/>")
                        End If
                        tb.AppendLine("<div class='option-sku'>" & row("SKU") & "</div>")

                        Dim name As String = row("Name").ToString.ToUpper.Replace("MADE TO MEASURE", "M2M")

                        tb.AppendLine("<div class='option-name' style='padding:0 7px;'>" & name.Replace("M2M", "M2M®") & "</div>")
                        'tb.AppendLine("<div style='font-size:10pt;'>" & buildDimensions(row("minWidth"), row("maxWidth"), row("minDepth"), row("maxDepth"), row("minHeight"), row("maxHeight")) & "</div>")
                        tb.AppendLine("</a>")
                        If row("Note") <> "" Then
                            tb.AppendLine("<div style='margin:10px;font-size:11pt;color:gray;'>" & row("Note") & "</div>")
                        End If
                        tb.AppendLine("</div>")
                    Next
                Else
                    If prevType <> opt("Type") Then
                        tb.AppendLine("<div class='program-option-type'>" & opt("Type").ToString.ToUpper & "</div>")

                        If opt("Link") <> "" Then
                            tb.AppendLine("<a href='" & opt("Link") & "'>")
                            tb.AppendLine("<div class='program-option'>")
                            If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg")) Then
                                tb.AppendLine("<img src='/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg'/>")
                            Else
                                tb.AppendLine("<img src='/Images/layout/no_image_small.jpg' style='opacity:.35;'/>")
                            End If
                            tb.AppendLine("<div style='padding:0 7px;'>" & opt("Name") & "</div>")
                            If opt("Note") <> "" Then
                                tb.AppendLine("<div style='margin:10px;font-size:11pt;color:gray;'>" & opt("Note") & "</div>")
                            End If
                            tb.AppendLine("</div>")
                            tb.AppendLine("</a>")
                        Else
                            tb.AppendLine("<div class='program-option'>")
                            If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg")) Then
                                tb.AppendLine("<img src='/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg'/>")
                            Else
                                tb.AppendLine("<img src='/Images/layout/no_image_small.jpg' style='opacity:.35;'/>")
                            End If
                            tb.AppendLine("<div class='option-name'>" & opt("Name") & "</div>")
                            If opt("Note") <> "" Then
                                tb.AppendLine("<div style='margin:10px;font-size:11pt;color:gray;'>" & opt("Note") & "</div>")
                            End If
                            tb.AppendLine("</div>")
                        End If
                    Else
                        If i = 0 Then
                            tb.AppendLine("<div class='program-option-type' style='visibility:hidden;'>Test</div>")
                        End If
                        If opt("Link") <> "" Then
                            tb.AppendLine("<a href='" & opt("Link") & "'>")
                            tb.AppendLine("<div class='program-option'>")

                            If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg")) Then
                                tb.AppendLine("<img src='/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg'/>")
                            Else
                                tb.AppendLine("<img src='/Images/layout/no_image_small.jpg' style='opacity:.35;'/>")
                            End If
                            tb.AppendLine("<div class='option-name'>" & opt("Name") & "</div>")
                            If opt("Note") <> "" Then
                                tb.AppendLine("<div style='margin:10px;font-size:11pt;color:gray;'>" & opt("Note") & "</div>")
                            End If
                            tb.AppendLine("</div>")
                            tb.AppendLine("</a>")
                        Else
                            tb.AppendLine("<div class='program-option'>")
                            If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg")) Then
                                tb.AppendLine("<img src='/" & collname & "/images/" & optionFolder & "/" & opt("OptionID") & ".jpg'/>")
                            Else
                                tb.AppendLine("<img src='/Images/layout/no_image_small.jpg' style='opacity:.35;'/>")
                            End If
                            tb.AppendLine("<div class='option-name'>" & opt("Name") & "</div>")
                            If opt("Note") <> "" Then
                                tb.AppendLine("<div style='margin:10px;font-size:11pt;color:gray;'>" & opt("Note") & "</div>")
                            End If
                            tb.AppendLine("</div>")
                        End If
                    End If

                    i += 1
                    prevType = opt("Type")
                End If

            Next
        Else
            Dim ai As New DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("/" & collname & "/images/" & optionFolder))
            Dim aiArr As FileInfo() = ai.GetFiles()

            For Each file In aiArr
                If file.Name.Contains("Tab-Image") Then
                    'Use image naming convention with an underscore to assign a title to the image section if necessary (Tab-Image_Title Goes Here)...
                    If file.Name.Contains("_") Then
                        tb.AppendLine("<div style='margin:25px;font-size:14pt;font-weight:bold;text-align:center;'>" & file.Name.Split("_")(1).ToString.Replace(file.Extension, "") & "</div>")
                    End If
                    tb.AppendLine("<p style='width:100%;text-align:center;'><img style='max-width:100%;' src='/" & collname & "/images/" & optionFolder & "/" & file.Name & "'/></p>")
                End If
            Next
        End If


        retVal = tb.ToString

        Return retVal
    End Function

    Private Shared Function buildDimensions(ByVal minWidth As Double, ByVal maxWidth As Double, ByVal minDepth As Double, ByVal maxDepth As Double, ByVal minHeight As Double, ByVal maxHeight As Double) As String
        Dim retVal As String = ""
        Dim sb As New System.Text.StringBuilder

        Dim width As String = ""

        If minWidth <> 0 And maxWidth <> 0 And (minWidth <> maxWidth) Then
            sb.AppendLine("<span>W:" & minWidth & "-" & maxWidth & "in</span>")
        ElseIf minWidth <> 0 Then
            sb.AppendLine("<span>W:" & minWidth & "in</span>")
        ElseIf maxWidth <> 0 Then
            sb.AppendLine("<span>W:" & maxWidth & "in</span>")
        End If

        If minDepth <> 0 And maxDepth <> 0 And (minDepth <> maxDepth) Then
            sb.AppendLine("<span>D:" & minDepth & "-" & maxDepth & "in</span>")
        ElseIf minDepth <> 0 Then
            sb.AppendLine("<span>D:" & minDepth & "in</span>")
        ElseIf maxDepth <> 0 Then
            sb.AppendLine("<span>D:" & maxDepth & "in</span>")
        End If

        If minHeight <> 0 And maxHeight <> 0 And (minHeight <> maxHeight) Then
            sb.AppendLine("<span>H:" & minHeight & "-" & maxHeight & "in</span>")
        ElseIf minHeight <> 0 Then
            sb.AppendLine("<span>H:" & minHeight & "in</span>")
        ElseIf maxHeight <> 0 Then
            sb.AppendLine("<span>H:" & maxHeight & "in</span>")
        End If

        retVal = sb.ToString

        Return retVal

    End Function

    <WebMethod()>
    Public Shared Function buildProgramGallery(ByVal program As String) As String
        Dim retVal As String = ""

        Dim di As New DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("/Source/" & program.ToString.Replace(" ", "").Replace("&", "") & "/images/gallery/"))
        Dim fiArr As FileInfo() = di.GetFiles()
        Dim sb As New System.Text.StringBuilder

        If fiArr.Count > 0 Then
            For Each fi As FileInfo In fiArr
                If fi.Name.Contains("_medium") Then
                    sb.AppendLine("<div><a href='/Source/" & program & "/images/gallery/" & fi.Name.Replace("_medium", "") & "' data-fancybox='" & program & "'><img src='/Source/" & program & "/images/gallery/" & fi.Name & "' /></a></div>")
                End If
            Next
        Else
            sb.AppendLine("<div id='no-image-msg'></div>")
        End If


        retVal = sb.ToString

        Return retVal
    End Function

    <WebMethod()>
    Public Shared Function galleryCount(ByVal program As String) As Integer
        Dim retVal As Integer

        Dim di As New DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("/Source/" & program.ToString.Replace(" ", "").Replace("&", "") & "/images/gallery/"))

        retVal = di.GetFiles.Count

        Return retVal
    End Function

    Public Shared Function isSKU(ByVal SKU As String) As Boolean
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As Boolean

        Try
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT SKU FROM ItemsGoodToShowHCConsumer WHERE SKU = @sku")

            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@sku", SKU)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                retVal = True
            Else
                retVal = False
            End If

        Catch ex As Exception
            Misc.LogError(ex)
            retVal = "ERROR"
        Finally
            dbConn.Close()
        End Try

        Return retVal
    End Function

End Class
