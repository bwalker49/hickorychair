﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Web.Services
Public Class Products

    Public Property productDetail As Product
    Public Property resultList As List(Of Product)
    Public Property productDimensions As Dimensions
    Public Property documentList As List(Of Document)
    Public Property productXrefList As List(Of ProductXref)

    Public Sub New(ByVal sku As String)
        productDetail = showProduct(sku)
        productDimensions = BuildDimensions(sku)
        documentList = GetDocuments(sku)
        'productXrefList = PPTableXref()
        resultList = Nothing
    End Sub

    Public Sub New(ByVal terms As SearchTerms)
        resultList = SearchResults(terms)
        productDetail = Nothing
        productDimensions = Nothing
        documentList = Nothing
        'types = productTypeList()
        'subtypes = productSubTypeList()
    End Sub

    Public Sub New()
        resultList = showWishList()
    End Sub

    'Public Property MapRequestAO As AjaxOptions
    '    Get
    '        Dim opts = New AjaxOptions()
    '        opts.HttpMethod = "POST"
    '        opts.UpdateTargetId = "WhereToBuyMap"
    '        opts.OnSuccess = "CreateDealerMapObjects"
    '        opts.OnBegin = "return StartValidation();"
    '        Return opts
    '    End Get
    '    Set(value As AjaxOptions)

    '    End Set
    'End Property

    Private Function showProduct(ByVal sku As String) As Product
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim newProd As New Product

        Try
            Dim SQL As String = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetProductDetails.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@sku", sku)
            tA.SelectCommand.Parameters.AddWithValue("@WebsiteID", "HickoryChair")
            tA.SelectCommand.Parameters.AddWithValue("@user", HttpContext.Current.Profile.UserName)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                Dim row As DataRow = tSet.Tables(0).Rows(0)

                newProd.sku = row("SKU")
                newProd.name = row("Name")
                newProd.type = row("TypeName")
                newProd.typeID = row("WebTypeID")
                newProd.subtypeID = row("SubTypeID")
                newProd.collectionType = row("CollectionType")
                newProd.collectionNo = row("CollectionNo")
                If row("CollectionName").ToString <> "" Then
                    newProd.collectionName = row("CollectionName") & " Collection"
                Else
                    newProd.collectionName = ""
                End If
                newProd.longDesc = row("LongDesc")
                newProd.notes = row("Notes")
                newProd.dimensions = setDimensions(String.Format("{0:0.##}", row("MinWidth")), String.Format("{0:0.##}", row("MaxWidth")), String.Format("{0:0.##}", row("MinDepth")), String.Format("{0:0.##}", row("MaxDepth")), String.Format("{0:0.##}", row("MinHeight")), String.Format("{0:0.##}", row("MaxHeight")), "in")
                newProd.dimensionsMetric = setDimensions(String.Format("{0:0.##}", (row("MinWidth") * 2.54)), String.Format("{0:0.##}", (row("MaxWidth") * 2.54)), String.Format("{0:0.##}", (row("MinDepth") * 2.54)), String.Format("{0:0.##}", (row("MaxDepth") * 2.54)), String.Format("{0:0.##}", (row("MinHeight") * 2.54)), String.Format("{0:0.##}", (row("MaxHeight") * 2.54)), "cm")
                newProd.insideDimensions = setDimensions(String.Format("{0:0.##}", row("InsideWidth")), String.Format("{0:0.##}", row("InsideWidth")), String.Format("{0:0.##}", row("InsideDepth")), String.Format("{0:0.##}", row("InsideDepth")), String.Format("{0:0.##}", row("InsideHeight")), String.Format("{0:0.##}", row("InsideHeight")), "Standard")
                newProd.armHeight = row("ArmHeight")
                newProd.seatHeight = row("SeatHeight")
                newProd.imgGallery = buildImageGallery(row("SKU"))
                If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/prod-images/" & row("Image") & ".jpg")) Then
                    newProd.imgURL = "/prod-images/" & row("Image") & ".jpg"
                    newProd.hiresimgURL = "/prod-images/" & row("Image") & "_hires.jpg"
                Else
                    newProd.imgURL = "/images/no_image.jpg"
                End If
                newProd.defaultFinish = row("FINISH_TYPE_1")
                If row("UPHOLSTERY_1") <> "" And row("UPHOLSTERY_1") <> "NA" Then
                    newProd.takesFabric = True
                Else
                    newProd.takesFabric = False
                End If

                If row("inWishList") = 1 Then
                    newProd.inWishList = row("inWishList")
                Else
                    newProd.inWishList = False
                End If
                'newProd.designer = row("Designer")
                newProd.status = row("Status")
                newProd.Qty = row("QtyInStock")
                newProd.ATPQty = row("ATPQty")
                newProd.nextATPQty = row("NextATPQty")
            End If

            If tSet.Tables(1).Rows.Count > 0 Then
                Dim iconList As String = ""

                For Each icon In tSet.Tables(1).Rows
                    iconList = iconList & icon("IconID") & "/" & icon("IconName") & "~"
                Next

                newProd.icons = iconList.TrimEnd(CChar("~"))
            Else
                newProd.icons = ""
            End If

            If tSet.Tables(2).Rows.Count > 0 Then
                Dim xrefList As New List(Of ProductXref)

                For Each prod In tSet.Tables(2).Rows
                    Dim newxref As New ProductXref

                    newxref.sku = prod("AlsoAvailableAsSKU")
                    newxref.name = prod("Name")

                    xrefList.Add(newxref)
                Next

                newProd.aasItems = xrefList
            End If

        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return newProd

    End Function

    Private Function BuildDimensions(ByVal sku As String) As Dimensions
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim newDimension As New Dimensions

        Try
            Dim SQL As String = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetProductDetails.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@sku", sku)
            tA.SelectCommand.Parameters.AddWithValue("@user", HttpContext.Current.Profile.UserName)
            tA.SelectCommand.Parameters.AddWithValue("@WebsiteID", "HickoryChair")

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                Dim row As DataRow = tSet.Tables(0).Rows(0)

                If row("minWidth") <> 0 Or row("maxWidth") <> 0 Then
                    If row("minWidth") <> row("maxWidth") Then
                        newDimension.OutsideWidth = row("minWidth") & " - " & row("maxWidth")
                        newDimension.OutsideWidthM = row("minWidth") * 2.54 & " - " & row("maxWidth") * 2.54
                    Else
                        newDimension.OutsideWidth = row("minWidth").ToString()
                        newDimension.OutsideWidthM = (row("minWidth") * 2.54).ToString()
                    End If
                Else
                    newDimension.OutsideWidth = "NA"
                    newDimension.OutsideWidthM = "NA"
                End If


                If row("minDepth") <> 0 Or row("maxDepth") <> 0 Then
                    If row("minDepth") <> row("maxDepth") Then
                        newDimension.OutsideDepth = row("minDepth") & " - " & row("maxDepth")
                        newDimension.OutsideDepthM = row("minDepth") * 2.54 & " - " & row("maxDepth") * 2.54
                    Else
                        newDimension.OutsideDepth = row("minDepth").ToString()
                        newDimension.OutsideDepthM = (row("minDepth") * 2.54).ToString()
                    End If
                Else
                    newDimension.OutsideDepth = "NA"
                    newDimension.OutsideDepthM = "NA"
                End If


                If row("minDepth") <> 0 Or row("maxDepth") <> 0 Then
                    If row("minHeight") <> row("maxHeight") Then
                        newDimension.OutsideHeight = row("minHeight") & " - " & row("maxHeight")
                        newDimension.OutsideHeightM = row("minHeight") * 2.54 & " - " & row("maxHeight") * 2.54
                    Else
                        newDimension.OutsideHeight = row("minHeight").ToString()
                        newDimension.OutsideHeightM = (row("minHeight") * 2.54).ToString()
                    End If
                Else
                    newDimension.OutsideHeight = "NA"
                    newDimension.OutsideHeightM = "NA"
                End If


                newDimension.InsideWidth = row("InsideWidth")
                newDimension.InsideDepth = row("InsideDepth")
                newDimension.InsideHeight = row("InsideHeight")
                newDimension.ArmHeight = row("ArmHeight")
                newDimension.SeatHeight = row("SeatHeight")
                newDimension.InsideWidthM = row("InsideWidth") * 2.54
                newDimension.InsideDepthM = row("InsideDepth") * 2.54
                newDimension.InsideHeightM = row("InsideHeight") * 2.54
                newDimension.ArmHeightM = row("ArmHeight") * 2.54
                newDimension.SeatHeightM = row("SeatHeight") * 2.54
                'newDimension.Leather = Math.Round((row("COM") * 0.9) * 18)
                newDimension.Leather = row("Leather")
                newDimension.COM = row("COM")
                newDimension.Weight = row("Weight")
                newDimension.Volume = row("Volume")
            End If

        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return newDimension
    End Function

    Private Function GetDocuments(ByVal SKU As String) As List(Of Document)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim DocList As New List(Of Document)

        Try
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT  ")
            sb.AppendLine("DocFileName     ")
            sb.AppendLine("FROM Items_Document_Xref ")
            sb.AppendLine("WHERE SKU = @SKU ")

            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@SKU", SKU)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            For Each doc In tSet.Tables(0).Rows
                If System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("/Documents/ItemDocs/" & doc("DocFileName") & ".pdf")) Then
                    Dim newDoc As New Document
                    newDoc.Title = doc("DocFileName")
                    newDoc.Link = "/Documents/ItemDocs/" & doc("DocFileName") & ".pdf"

                    DocList.Add(newDoc)
                End If
            Next
        Catch ex As Exception
            Misc.LogError(ex)
        End Try

        Return DocList
    End Function

    Private Function SearchResults(ByVal terms As SearchTerms) As List(Of Product)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim resultList As New List(Of Product)

        Try
            Dim SQL As String

            If terms.NewIntros Then
                '    'Dim currMonth As Integer = Today.Month
                '    'Dim tYear As Integer            'Two Markets Ago 
                '    'Dim tSeason As String = ""      'Two Markets Ago
                '    'Dim mYear As Integer            'This past market
                '    'Dim mSeason As String = ""     'This past market

                '    'If currMonth >= 4 And currMonth < 10 Then
                '    '    tYear = Today.Year - 1
                '    '    tSeason = "F"
                '    '    mYear = Today.Year
                '    '    mSeason = "S"

                '    'ElseIf currMonth >= 10 And currMonth <= 12 Then
                '    '    tYear = Today.Year
                '    '    tSeason = "S"
                '    '    mYear = Today.Year
                '    '    mSeason = "F"

                '    'Else
                '    '    tYear = Today.Year - 1
                '    '    tSeason = "S"
                '    '    mYear = Today.Year - 1
                '    '    mSeason = "F"
                '    'End If

                SQL = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetSearchResults.sql"))
                SQL = SQL & " AND ( (isnull(i.MarketSeason,'') = (SELECT MarketSeason FROM New_Intro_Dates WHERE WebsiteID = 'HickoryChair' AND WebsiteType='Consumer') AND isnull(i.MarketYear,0) = (SELECT MarketYear FROM New_Intro_Dates WHERE WebsiteID = 'HickoryChair' AND WebsiteType='Consumer'))) "
                SQL = SQL & "ORDER BY Name;"
            ElseIf terms.WishList Then
                SQL = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetWishListItems.sql"))
                'If terms.PrintList <> "" Then
                '    SQL = SQL & " AND wli.SKU IN (SELECT value FROM STRING_SPLIT(@PrintList,','))"
                'End If

                '    Trace.Write(SQL)
                SQL = SQL & "ORDER BY Name;"
            ElseIf terms.CollectionID <> "" Then
                SQL = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetSearchResults.sql"))
                SQL = SQL & "ORDER BY Name;"
                SQL = SQL & System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Navigation/SQL/GetCollections.sql"))
            Else
                SQL = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetSearchResults.sql"))
                SQL = SQL & "ORDER BY Name;"
            End If



            'If terms.InStock = "Yes" Then
            '    SQL = SQL & "AND QtyInStock > 0"
            'End If


            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@TypeID", terms.TypeID)
            tA.SelectCommand.Parameters.AddWithValue("@SubTypeID", terms.SubTypeID)
            tA.SelectCommand.Parameters.AddWithValue("@CollectionID", terms.CollectionID)
            tA.SelectCommand.Parameters.AddWithValue("@RoomID", terms.RoomID)
            tA.SelectCommand.Parameters.AddWithValue("@ProductLine", terms.ProductLine)
            tA.SelectCommand.Parameters.AddWithValue("@DesignerID", terms.DesignerID)
            tA.SelectCommand.Parameters.AddWithValue("@WildCard", terms.WildCard)
            tA.SelectCommand.Parameters.AddWithValue("@PrintList", terms.PrintList)
            tA.SelectCommand.Parameters.AddWithValue("@user", HttpContext.Current.Profile.UserName)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each product In tSet.Tables(0).Rows
                    Dim newProd As New Product
                    newProd.sku = product("SKU")
                    newProd.name = StrConv(product("Name"), VbStrConv.ProperCase)
                    newProd.type = product("TypeName")
                    'newProd.subtype = product("SubtypeName")
                    'newProd.collectionNo = product("CollectionNo")
                    If terms.CollectionID <> "" Then
                        Dim collRow() As DataRow = tSet.Tables(1).Select("CollectionNo = '" & terms.CollectionID & "'")

                        If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/Images/layout/CollectionImages/" & collRow(0).Item("IntroImageName"))) And collRow(0).Item("IntroPageText") <> "" Then
                            newProd.collectionImg = "/Images/layout/CollectionImages/" & collRow(0).Item("IntroImageName")
                            newProd.collectionText = collRow(0).Item("IntroPageText")
                        End If
                    End If
                    newProd.longDesc = product("LongDesc")
                    newProd.dimensions = setDimensions(String.Format("{0:0.##}", product("MinWidth")), String.Format("{0:0.##}", product("MaxWidth")), String.Format("{0:0.##}", product("MinDepth")), String.Format("{0:0.##}", product("MaxDepth")), String.Format("{0:0.##}", product("MinHeight")), String.Format("{0:0.##}", product("MaxHeight")), "in")
                    newProd.dimensionsMetric = setDimensions(String.Format("{0:0.##}", (product("MinWidth") * 2.54)), String.Format("{0:0.##}", (product("MaxWidth") * 2.54)), String.Format("{0:0.##}", (product("MinDepth") * 2.54)), String.Format("{0:0.##}", (product("MaxDepth") * 2.54)), String.Format("{0:0.##}", (product("MinHeight") * 2.54)), String.Format("{0:0.##}", (product("MaxHeight") * 2.54)), "cm")
                    If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/prod-images/" & product("Image") & "_small.jpg")) Then
                        newProd.imgURL = "/prod-images/" & product("Image") & "_small.jpg"
                        newProd.hiresimgURL = "/prod-images/" & product("Image") & ".jpg"
                    Else
                        newProd.imgURL = "/Images/layout/no_image_small.jpg"
                    End If
                    'If product("QtyInStock") > 0 Then
                    '    newProd.inStock = "In Stock"
                    'Else
                    '    newProd.inStock = "Out of Stock"
                    'End If
                    'newProd.currQty = product("QtyInStock")
                    'If product("Designer") <> "" Then
                    '    newProd.designer = product("Designer")
                    'End If

                    resultList.Add(newProd)
                Next
            End If
        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return resultList
    End Function

    <WebMethod()>
    Public Shared Function BuildFabrics(ByVal sku As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim sb As New System.Text.StringBuilder

        Try
            Dim tb As New System.Text.StringBuilder
            tb.AppendLine("Declare @COM as varchar(50)  ")
            tb.AppendLine("Declare @COL as varchar(50)  ")
            tb.AppendLine("  ")
            tb.AppendLine("Set @COM = (select isnull(COMYardage,'') from Items where sku=@SKU)  ")
            tb.AppendLine("Set @COL = (select isnull(Leather,'') FROM Items where sku=@SKU)  ")
            tb.AppendLine(" ")
            tb.AppendLine("select * FROM (  ")
            tb.AppendLine("select   ")
            tb.AppendLine("	fs.sku,  ")
            tb.AppendLine("	fs.QtyInStock,  ")
            tb.AppendLine("	fs.Status,  ")
            tb.AppendLine("	sum(sis.INVOICED_QUANTITY) as Total_Inv_Qty,  ")
            tb.AppendLine("	z.CreatedDate,  ")
            tb.AppendLine("	z.Grade,  ")
            tb.AppendLine("	z.ClassDescription,  ")
            tb.AppendLine("	rank()  ")
            tb.AppendLine("	over (partition by z.ClassDescription  ")
            tb.AppendLine("	      order by sum(sis.INVOICED_QUANTITY) desc) as Rank  ")
            tb.AppendLine("FROM CenturyWeb.dbo.Fabrics_Status fs  ")
            tb.AppendLine("INNER JOIN SISWeb2.dbo.SIS_S815 sis on sis.Material_ID=fs.SKU  ")
            tb.AppendLine("INNER JOIN SCFIWeb.dbo.rpZFABInfo z on z.materialnumber=fs.sku and z.SOrg=fs.SOrg ")
            tb.AppendLine("where   ")
            tb.AppendLine("	fs.Status in ('CA','A')  ")
            tb.AppendLine("	and sis.SIS_WEEK > '200101'  ")
            tb.AppendLine("	and z.ClassDescription is not null  ")
            tb.AppendLine(" and fs.SOrg = 'HC01'  ")
            tb.AppendLine(" and z.SOrg = 'HC01'  ")
            tb.AppendLine(" and PlantLocation='HC01' ")
            tb.AppendLine("	and  (  ")
            tb.AppendLine("			(@COM <> '' and @COL <> '' AND MatlGroup in  ('MG17','MG54') )  ")
            tb.AppendLine("			OR  ")
            tb.AppendLine("			(@COM <> '' and MatlGroup in ('MG17') )  ")
            tb.AppendLine("			OR  ")
            tb.AppendLine("			(@COL <> '' and MatlGroup in ('MG54') )  ")
            tb.AppendLine("		 )  ")
            tb.AppendLine("Group By  ")
            tb.AppendLine("	fs.sku,  ")
            tb.AppendLine("	fs.QtyInStock,  ")
            tb.AppendLine("	fs.Status,  ")
            tb.AppendLine("	z.CreatedDate,  ")
            tb.AppendLine("	z.Grade,  ")
            tb.AppendLine("	z.ClassDescription) a  ")
            tb.AppendLine("	where a.Rank <= 10  ")
            tb.AppendLine("ORDER BY   ")
            tb.AppendLine("Case WHEN ClassDescription = 'Plain' THEN '1'  ")
            tb.AppendLine("WHEN classDescription = 'Velvets' THEN '2'  ")
            tb.AppendLine("WHEN classDescription = 'Leather' THEN '3'  ")
            tb.AppendLine("Else ClassDescription End,  ")
            tb.AppendLine("    Total_Inv_Qty desc  ")
            Dim tC As New SqlCommand(tb.ToString, dbConn)
            tC.Parameters.AddWithValue("@SKU", sku)
            Dim tR As SqlDataReader
            tR = tC.ExecuteReader

            If sku = "DNSF" Then
                sb.AppendLine("<div id='fabric-message'>This item requires you to select a fabric or multiple fabrics for our upholsterers to use when making your piece.  Hickory Chair offers many fabric options in addition to COM (Customer's Own Materials).  ")
                sb.AppendLine("You can search all of our fabrics using our <p><a href='/Resources/FabSearch' style='font-weight:bold;'>Advanced Fabric Search</a></p>.</div>")
                sb.AppendLine("<p><img src='/Content/Images/fabsearch_pd.jpg' width='100%'/></p>")
            Else
                If tR.HasRows Then
                    'sb.AppendLine("<h2>Fabric Selections</h2>")

                    sb.AppendLine("<div id='fabric-message'>This item requires you to select a fabric or multiple fabrics for our upholsterers to use when making your piece.  Hickory Chair offers many fabric options in addition to COM (Customer's Own Materials).  ")
                    sb.AppendLine("We are showing some of our best selling fabrics below. You can also search all of our fabrics using our <a href='/Resources/FabSearch' style='font-weight:bold;'>Advanced Fabric Search</a>.</div>")

                    sb.AppendLine("<ul>")
                    Dim lastClass As String = ""
                    While tR.Read
                        If lastClass <> tR("ClassDescription") Then
                            sb.AppendLine("<h3><u>" & tR("ClassDescription") & "</u></h3>")
                        End If
                        sb.AppendLine("<li class='fabric-item'>")
                        sb.AppendLine("<a href='/Resources/FabDetail?SKU=" & tR("SKU") & "' style='text-decoration:none;'>")
                        If File.Exists(HttpContext.Current.Server.MapPath("/prod-images/" & tR("SKU") & "_small.jpg")) Then
                            sb.AppendLine("<img src='/prod-images/" & tR("SKU") & "_small.jpg' alt='Image not found'>")
                        Else
                            sb.AppendLine("<img src='/Images/layout/no_image_small.jpg' alt='Image not found'>")
                        End If
                        sb.AppendLine("<div><strong>" & tR("SKU") & "</strong></div>")
                        sb.AppendLine("</a>")
                        sb.AppendLine("</li>")


                        lastClass = tR("ClassDescription")
                    End While
                    sb.AppendLine("</ul>")
                End If
            End If


        Catch ex As Exception
            Trace.Write(ex.Message)
            Trace.Write(ex.StackTrace)
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return sb.ToString
    End Function


    <WebMethod()>
    Public Shared Function addToWishlist(ByVal sku As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Try
            Dim SQL As String = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/addToWishlist.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@wID", HttpContext.Current.Profile.UserName)
            tA.SelectCommand.Parameters.AddWithValue("@sku", sku)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows(0).Item("Updated") = 1 Then
                retVal = "SUCCESS"
            Else
                retVal = "FAILURE"
            End If

        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return retVal
    End Function

    <WebMethod()>
    Public Shared Function deleteFromWishlist(ByVal sku As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Try
            Dim SQL As String = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/deleteFromWishlist.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@user", HttpContext.Current.Profile.UserName)
            tA.SelectCommand.Parameters.AddWithValue("@sku", sku)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows(0).Item("Deleted") = "Success" Then
                retVal = "SUCCESS"
            End If

        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return retVal
    End Function

    <WebMethod()>
    Public Shared Function showWishList() As List(Of Product)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim resultList As New List(Of Product)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/Products/SQL/GetWishListItems.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@user", HttpContext.Current.Profile.UserName)
            Dim tSet As New DataSet

            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each product In tSet.Tables(0).Rows
                    Dim newProd As New Product
                    newProd.sku = product("SKU")
                    newProd.name = StrConv(product("Name"), VbStrConv.ProperCase)
                    newProd.type = product("TypeName")
                    If File.Exists(System.Web.HttpContext.Current.Server.MapPath("/prod-images/" & product("Image") & "_small.jpg")) Then
                        newProd.imgURL = "/prod-images/" & product("Image") & "_small.jpg"
                    Else
                        newProd.imgURL = "/images/no_image_small.jpg"
                    End If
                    'If product("QtyInStock") > 0 Then
                    '    newProd.inStock = "In Stock"
                    'Else
                    '    newProd.inStock = "Out of Stock"
                    'End If

                    resultList.Add(newProd)
                Next
            End If


        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return resultList

    End Function

    Private Function buildImageGallery(ByVal SKU As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Try
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT  ")
            sb.AppendLine("CASE  ")
            sb.AppendLine("WHEN Image IS NOT NULL THEN ")
            sb.AppendLine("Image ")
            sb.AppendLine("WHEN ImageAlt IS NOT NULL THEN ")
            sb.AppendLine("ImageAlt ")
            sb.AppendLine("ELSE ")
            sb.AppendLine("'' ")
            sb.AppendLine("END AS Image, ")
            sb.AppendLine("isnull(ExtraImages, '') As ExtraImages ")
            sb.AppendLine("FROM Items ")
            sb.AppendLine("WHERE SKU = @sku ")

            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@sku", SKU)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                Dim tb As New System.Text.StringBuilder

                tb.AppendLine("<div id='product-main-slick-gallery'>")
                tb.AppendLine("<div>")
                tb.AppendLine("<a data-fancybox='gallery' href='/prod-images/" & tSet.Tables(0).Rows(0).Item("Image") & "_hires.jpg'>")
                tb.AppendLine("<img src='/prod-images/" & tSet.Tables(0).Rows(0).Item("Image") & ".jpg'>")
                tb.AppendLine("</a>")
                tb.AppendLine("</div>")

                If tSet.Tables(0).Rows(0).Item("ExtraImages") <> "" Then
                    Dim imgs() As String = tSet.Tables(0).Rows(0).Item("ExtraImages").ToString.Split(",")

                    For Each image In imgs
                        tb.AppendLine("<div>")
                        tb.AppendLine("<a data-fancybox='gallery' href='/prod-images/" & image & "_hires.jpg'>")
                        tb.AppendLine("<img src='/prod-images/" & image & ".jpg'>")
                        tb.AppendLine("</a>")
                        tb.AppendLine("</div>")
                    Next

                    tb.AppendLine("</div>")

                    tb.AppendLine("<div id='product-thumbnail-slick-gallery'>")
                    tb.AppendLine("<div>")
                    tb.AppendLine("<img src='/prod-images/" & tSet.Tables(0).Rows(0).Item("Image") & "_small.jpg' width='125'>")
                    tb.AppendLine("</div>")

                    For Each image In imgs
                        tb.AppendLine("<div>")
                        tb.AppendLine("<img src='/prod-images/" & image & "_small.jpg' width='125'>")
                        tb.AppendLine("</div>")
                    Next
                End If
                tb.AppendLine("</div>")

                retVal = tb.ToString

            End If
        Catch ex As Exception
            'MiscCode.LogError(ex)
            retVal = "ERROR"
        Finally
            dbConn.Close()
        End Try

        Return retVal

    End Function

    <WebMethod()>
    Public Shared Function isSKU(ByVal sku As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Try
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT SKU FROM ItemsGoodToShowHCConsumer WHERE SKU = @sku")

            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@sku", sku)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                retVal = "Valid"
            Else
                retVal = "Not Valid"
            End If

        Catch ex As Exception
            Misc.LogError(ex)
            retVal = "ERROR"
        Finally
            dbConn.Close()
        End Try

        Return retVal

    End Function

    Private Function setDimensions(ByVal minW As String, ByVal maxW As String, ByVal minD As String, ByVal maxD As String, ByVal minH As String, ByVal maxH As String, ByVal unit As String) As String
        Dim retVal As String = ""

        Dim width As String = ""
        Dim depth As String = ""
        Dim height As String = ""

        If minW = maxW Then
            width = "W " & minW & unit
        ElseIf minW <> maxW And minW <> "" And maxW <> "" Then
            width = "W " & minW & " - " & maxW & unit
        ElseIf minW <> maxW And minW <> "" Then
            width = "W " & minW & unit
        ElseIf minW <> maxW And maxW <> "" Then
            width = "W " & maxW & unit
        End If

        If minD = maxD Then
            depth = "D " & minD & unit
        ElseIf minD <> maxD And minD <> "" And maxD <> "" Then
            depth = "D " & minD & " - " & maxD & unit
        ElseIf minD <> maxD And minD <> "" Then
            depth = "D " & minD & unit
        ElseIf minD <> maxD And maxD <> "" Then
            depth = "D " & maxD & unit
        End If

        If minH = maxH Then
            height = "H " & minH & unit
        ElseIf minH <> maxH And minH <> "" And maxH <> "" Then
            height = "H " & minH & " - " & maxH & unit
        ElseIf minH <> maxH And minH <> "" Then
            height = "H " & minH & unit
        ElseIf minH <> maxH And maxH <> "" Then
            height = "H " & maxH & unit
        End If

        retVal = width & " " & depth & " " & height

        Return retVal
    End Function

    'Public Shared Function PPTableXref() As List(Of ProductXref)
    '    Dim retVal As New List(Of ProductXref)

    '    Dim HC18410 As New ProductXref
    '    HC18410.Item = "HC184-10"
    '    HC18410.XrefItems = "HC142-10~HC142-11~HC185-10~HC185-11~HC2476-11~HC5744-10~HC9844-10"

    '    retVal.Add(HC18410)

    '    Dim HC18411 As New ProductXref
    '    HC18411.Item = "HC184-11"
    '    HC18411.XrefItems = "HC142-10~HC142-11~HC185-10~HC185-11~HC2476-11~HC5744-10~HC9844-10"

    '    retVal.Add(HC18411)

    '    Dim HC245142 As New ProductXref
    '    HC245142.Item = "HC2451-42"
    '    HC245142.XrefItems = "HC142-10~HC142-11~HC185-10~HC185-11~HC1539-10~HC1542-11~HC2476-11~HC5744-10~HC9844-10~HC8644-10"

    '    retVal.Add(HC245142)

    '    Dim HC574210 As New ProductXref
    '    HC574210.Item = "HC5742-10"
    '    HC574210.XrefItems = "HC142-10~HC142-11~HC185-10~HC185-11~HC2476-11~HC5744-10~HC9844-10~HC8644-10"

    '    retVal.Add(HC574210)

    '    Dim HC154210 As New ProductXref
    '    HC154210.Item = "HC1542-10"
    '    HC154210.XrefItems = "HC142-10~HC142-11~HC182-10~HC182-11~HC185-10~HC185-11~HC744-10~HC1539-10~HC1542-11~HC2476-11~HC5342-10~HC5744-10~HC7944-10~HC9844-10~HC1346-10~HC8644-10"

    '    retVal.Add(HC154210)

    '    Dim HC574310 As New ProductXref
    '    HC574310.Item = "HC5743-10"
    '    HC574310.XrefItems = "HC142-10~HC142-11~HC182-10~HC182-11~HC185-10~HC185-11~HC744-10~HC1539-10~HC1542-11~HC2476-11~HC5342-10~HC5744-10~HC7944-10~HC9844-10~HC1346-10~HC8644-10"

    '    retVal.Add(HC574310)

    '    Dim HC984310 As New ProductXref
    '    HC984310.Item = "HC9843-10"
    '    HC984310.XrefItems = "HC142-10~HC142-11~HC185-10~HC185-11~HC1539-10~HC2476-11~HC7944-10~HC9844-10~HC1346-10~HC8644-10"

    '    retVal.Add(HC984310)

    '    Dim HC864310 As New ProductXref
    '    HC864310.Item = "HC8643-10"
    '    HC864310.XrefItems = "HC185-10~HC185-11~HC8644-10"

    '    retVal.Add(HC864310)

    '    Dim HC18110 As New ProductXref
    '    HC18110.Item = "HC181-10"
    '    HC18110.XrefItems = "HC182-10~HC182-11~HC1644-10~HC5342-10"

    '    retVal.Add(HC18110)

    '    Dim HC18111 As New ProductXref
    '    HC18111.Item = "HC181-11"
    '    HC18111.XrefItems = "HC182-10~HC182-11~HC1644-10~HC5342-10"

    '    retVal.Add(HC18111)

    '    Dim HC74310 As New ProductXref
    '    HC74310.Item = "HC743-10"
    '    HC74310.XrefItems = "HC142-10~HC142-11~HC182-10~HC182-11~HC744-10~HC1539-10~HC1542-11~HC2476-11~HC5744-10~HC7944-10~HC9844-10~HC1346-10~HC8644-10"

    '    retVal.Add(HC74310)

    '    Dim HC164310 As New ProductXref
    '    HC164310.Item = "HC1643-10"
    '    HC164310.XrefItems = "HC1644-10~HC5342-10"

    '    retVal.Add(HC164310)

    '    Dim HC534110 As New ProductXref
    '    HC534110.Item = "HC5431-10"
    '    HC534110.XrefItems = "HC1644-10~HC5432-10"

    '    retVal.Add(HC534110)

    '    Dim HC534111 As New ProductXref
    '    HC534111.Item = "HC5341-11"
    '    HC534110.XrefItems = "HC1644-10~HC5432-10"

    '    retVal.Add(HC534111)

    '    Dim HC984311 As New ProductXref
    '    HC984311.Item = "HC9843-11"
    '    HC984311.XrefItems = "HC142-10~HC142-11~HC1539-10~HC2476-11~HC7944-10~HC9844-10~HC1346-10~HC8644-10"

    '    retVal.Add(HC984311)

    '    Dim HC14110 As New ProductXref
    '    HC14110.Item = "HC141-10"
    '    HC14110.XrefItems = "HC142-10~HC142-11~HC7944-10~HC1346-10"

    '    retVal.Add(HC14110)

    '    Dim HC14111 As New ProductXref
    '    HC14111.Item = "HC141-11"
    '    HC14111.XrefItems = "HC142-10~HC142-11~HC7944-10"

    '    retVal.Add(HC14111)

    '    Dim HC244516 As New ProductXref
    '    HC244516.Item = "HC2445-16"
    '    HC244516.XrefItems = "HC185-10~HC185-11~HC742-10~HC2444-16~HC2476-11~HC5342-10~HC5744-10"

    '    retVal.Add(HC244516)

    '    Dim HC18010 As New ProductXref
    '    HC18010.Item = "HC180-10"
    '    HC18010.XrefItems = "HC185-10~HC185-11~HC2476-11~HC5744-10"

    '    retVal.Add(HC18010)

    '    Dim HC18011 As New ProductXref
    '    HC18011.Item = "HC180-11"
    '    HC18011.XrefItems = "HC185-10~HC185-11~HC2476-11~HC5744-10"

    '    retVal.Add(HC18011)

    '    Dim HC74110 As New ProductXref
    '    HC74110.Item = "HC741-10"
    '    HC74110.XrefItems = "HC742-10~HC2444-16"

    '    retVal.Add(HC74110)

    '    Dim HC864311 As New ProductXref
    '    HC864311.Item = "HC8643-11"
    '    HC864311.XrefItems = "HC185-10~HC185-11~HC8644-10~HC8644-10"

    '    retVal.Add(HC864311)

    '    Dim HC864312 As New ProductXref
    '    HC864312.Item = "HC8643-12"
    '    HC864312.XrefItems = "HC185-10~HC185-11~HC8644-10~HC8644-10"

    '    retVal.Add(HC864312)

    '    Dim HC247030 As New ProductXref
    '    HC247030.Item = "HC2470-30"
    '    HC247030.XrefItems = "HC185-10~HC185-11~HC2444-16"

    '    retVal.Add(HC247030)

    '    Dim HC246710 As New ProductXref
    '    HC246710.Item = "HC2467-10"
    '    HC246710.XrefItems = "HC185-10~HC185-11~HC2476-11~HC5744-10"

    '    retVal.Add(HC246710)



    '    Dim HC14210 As New ProductXref
    '    HC14210.Item = "HC142-10"
    '    HC14210.XrefItems = "HC184-10~HC184-11~HC2451-42~HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC743-10~HC9843-11~HC141-10~HC141-11"

    '    retVal.Add(HC14210)

    '    Dim HC14211 As New ProductXref
    '    HC14211.Item = "HC142-11"
    '    HC14211.XrefItems = "HC184-10~HC184-11~HC2451-42~HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC743-10~HC9843-11~HC141-10~HC141-11"

    '    retVal.Add(HC14211)

    '    Dim HC18210 As New ProductXref
    '    HC18210.Item = "HC182-10"
    '    HC18210.XrefItems = "HC1542-10~HC5743-10~HC181-10~HC181-11~HC743-10"

    '    retVal.Add(HC18210)

    '    Dim HC18211 As New ProductXref
    '    HC18211.Item = "HC182-11"
    '    HC18211.XrefItems = "HC1542-10~HC5743-10~HC181-10~HC181-11~HC743-10"

    '    retVal.Add(HC18211)

    '    Dim HC18510 As New ProductXref
    '    HC18510.Item = "HC185-10"
    '    HC18510.XrefItems = "HC184-10~HC184-11~HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC2445-16~HC180-10~HC180-11~HC2470-30~HC2467-10"

    '    retVal.Add(HC18510)

    '    Dim HC18511 As New ProductXref
    '    HC18511.Item = "HC185-11"
    '    HC18511.XrefItems = "HC184-10~HC184-11~HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC2445-16~HC180-10~HC180-11~HC2470-30~HC2467-10"

    '    retVal.Add(HC18511)

    '    Dim HC74210 As New ProductXref
    '    HC74210.Item = "HC742-10"
    '    HC74210.XrefItems = "HC2445-16~HC741-10"

    '    retVal.Add(HC74210)

    '    Dim HC74410 As New ProductXref
    '    HC74410.Item = "HC744-10"
    '    HC74410.XrefItems = "HC1542-10~HC5743-10~HC2467-10~HC743-10"

    '    retVal.Add(HC74410)

    '    Dim HC134610 As New ProductXref
    '    HC134610.Item = "HC1346-10"
    '    HC134610.XrefItems = "HC1542-10~HC5743-10~HC9843-10~HC743-10~HC9843-11~HC141-10~HC141-11"

    '    retVal.Add(HC134610)

    '    Dim HC864410 As New ProductXref
    '    HC864410.Item = "HC8644-10"
    '    HC864410.XrefItems = "HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC8643-10~HC743-10~HC9843-11~HC8643-11~HC8643-12"

    '    Dim HC153910 As New ProductXref
    '    HC153910.Item = "HC1539-10"
    '    HC153910.XrefItems = "HC2451-42~HC1542-10~HC5743-10~HC9843-10~HC743-10~HC9843-11"

    '    retVal.Add(HC153910)

    '    Dim HC154211 As New ProductXref
    '    HC154211.Item = "HC1542-11"
    '    HC154211.XrefItems = "HC2451-42~HC1542-10~HC5743-10~HC743-10"

    '    retVal.Add(HC154211)

    '    Dim HC164410 As New ProductXref
    '    HC164410.Item = "HC1644-10"
    '    HC164410.XrefItems = "HC181-10~HC181-11~HC1643-10~HC5341-10~HC5341-12"

    '    retVal.Add(HC164410)

    '    Dim HC244416 As New ProductXref
    '    HC244416.Item = "HC2444-16"
    '    HC244416.XrefItems = "HC2445-16~HC741-10"

    '    retVal.Add(HC244416)

    '    Dim HC247611 As New ProductXref
    '    HC247611.Item = "HC2476-11"
    '    HC247611.XrefItems = "HC184-10~HC184-11~HC2451-42~HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC2445-16~HC180-10~HC180-11~HC2470-30~HC2467-10~HC743-10~HC9843-11"

    '    retVal.Add(HC247611)

    '    Dim HC534210 As New ProductXref
    '    HC534210.Item = "HC5432-10"
    '    HC534210.XrefItems = "HC1542-10~HC5743-10~HC2445-16~HC2467-10~HC181-10~HC181-11~HC1643-10~HC5341-10~HC5341-12"

    '    retVal.Add(HC534210)

    '    Dim HC574410 As New ProductXref
    '    HC574410.Item = "HC5744-10"
    '    HC574410.XrefItems = "HC184-10~HC184-11~HC2451-42~HC5742-10~HC1542-10~HC5743-10~HC2445-16~HC180-10~HC180-11~HC2470-30~HC2467-10~HC743-10"

    '    retVal.Add(HC574410)

    '    Dim HC794410 As New ProductXref
    '    HC794410.Item = "HC9744-10"
    '    HC794410.XrefItems = "HC1542-10~HC5743-10~HC9843-10~HC743-10~HC9843-11~HC141-10~HC141-11"

    '    retVal.Add(HC794410)

    '    Dim HC984410 As New ProductXref
    '    HC984410.Item = "HC9844-10"
    '    HC984410.XrefItems = "HC184-10~HC184-11~HC2451-42~HC5742-10~HC1542-10~HC5743-10~HC9843-10~HC743-10~HC9843-11"

    '    retVal.Add(HC984410)

    '    Return retVal
    'End Function

    Public Shared Function GetItemImage(ByVal sku As String) As String
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim retVal As String = ""

        Try
            Dim sb As New System.Text.StringBuilder
            'sb.AppendLine("SELECT  ")
            'sb.AppendLine("CASE  ")
            'sb.AppendLine("WHEN @sku IN (SELECT SKU FROM ItemsGoodToShowHCConsumer) THEN ")
            'sb.AppendLine("(SELECT Name FROM Items WHERE SKU = @sku) ")
            'sb.AppendLine("ELSE ")
            'sb.AppendLine("'Not Valid' ")
            'sb.AppendLine("END AS Name ")

            sb.AppendLine("SELECT Image FROM Items WHERE SKU = @sku")

            Dim tA As New SqlDataAdapter(sb.ToString, dbConn)
            tA.SelectCommand.Parameters.AddWithValue("@sku", sku)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                retVal = tSet.Tables(0).Rows(0).Item("Image")
            Else
                retVal = "Not Valid"
            End If


        Catch ex As Exception
            Misc.LogError(ex)
            retVal = "ERROR"
        Finally
            dbConn.Close()
        End Try

        Return retVal
    End Function

    Public Class Product
        Public Property sku As String
        'Public Property sku_xref As String
        Public Property imgURL As String
        Public Property hiresimgURL As String
        Public Property imgGallery As String
        Public Property name As String
        Public Property collectionType As String
        Public Property collectionNo As String
        Public Property collectionName As String
        Public Property collectionImg As String
        Public Property collectionText As String
        Public Property shortDesc As String
        Public Property longDesc As String
        Public Property notes As String
        Public Property status As String
        Public Property type As String
        Public Property typeID As String
        Public Property subtype As String
        Public Property subtypeID As String
        Public Property grouping As String
        Public Property dimensions As String
        Public Property dimensionsMetric As String
        Public Property width As String
        Public Property depth As String
        Public Property height As String
        Public Property insideDimensions As String
        Public Property insideDimensionsMetric As String
        Public Property insideWidth As String
        Public Property insideDepth As String
        Public Property insideHeight As String
        Public Property seatHeight As String
        Public Property armHeight As String
        Public Property defaultFinish As String
        Public Property takesFabric As Boolean
        Public Property Qty As Integer
        Public Property ATPQty As Integer
        'Public Property inStock As String
        'Public Property status As String
        Public Property nextATPQty As Integer
        'Public Property sold_as As String
        'Public Property cartons As String
        'Public Property price As String
        Public Property inWishList As Boolean
        Public Property icons As String

        Public Property aasItems As List(Of ProductXref)
        'Public Property designer As String
    End Class

    Public Class SearchTerms
        Public Property TypeID As String
        Public Property SubTypeID As String
        Public Property DesignerID As String
        Public Property CollectionID As String
        Public Property RoomID As String
        Public Property ProductLine As String
        Public Property WildCard As String
        Public Property NewIntros As Boolean
        Public Property InStock As String
        Public Property WishList As Boolean
        Public Property UserID As String
        Public Property PrintList As String
    End Class

    Public Class Dimensions
        Public Property OutsideWidth As String
        Public Property OutsideDepth As String
        Public Property OutsideHeight As String
        Public Property InsideWidth As Double
        Public Property InsideDepth As Double
        Public Property InsideHeight As Double
        Public Property SeatWidth As Double
        Public Property SeatDepth As Double
        Public Property SeatHeight As Double
        Public Property ArmWidth As Double
        Public Property ArmDepth As Double
        Public Property ArmHeight As Double
        Public Property OutsideWidthM As String
        Public Property OutsideDepthM As String
        Public Property OutsideHeightM As String
        Public Property InsideWidthM As Double
        Public Property InsideDepthM As Double
        Public Property InsideHeightM As Double
        Public Property SeatWidthM As Double
        Public Property SeatDepthM As Double
        Public Property SeatHeightM As Double
        Public Property ArmWidthM As Double
        Public Property ArmDepthM As Double
        Public Property ArmHeightM As Double
        Public Property Volume As Double
        Public Property Weight As Double
        Public Property Leather As String
        Public Property COM As Double
    End Class

    Public Class Document
        Public Property Title As String
        Public Property Link As String
        Public Property Type As String
    End Class

    Public Class ProductXref
        Public Property sku As String
        Public Property name As String
    End Class

End Class
