﻿Imports Microsoft.VisualBasic
Imports SAP.Middleware.Connector

Public Class SAP_CNO_3



    Implements IDestinationConfiguration

    Public Event ConfigurationChanged(ByVal destinationName As String, ByVal args As RfcConfigurationEventArgs) Implements IDestinationConfiguration.ConfigurationChanged

    Public Function GetParameters(ByVal destinationName As String) As RfcConfigParameters Implements IDestinationConfiguration.GetParameters

        Dim parms As New RfcConfigParameters

        Select Case destinationName



            Case "D74"
                parms.Add(RfcConfigParameters.AppServerHost, "172.16.100.223")
                parms.Add(RfcConfigParameters.SystemNumber, "74")
                parms.Add(RfcConfigParameters.SystemID, "D74")
                parms.Add(RfcConfigParameters.User, Misc.scfiReadOption("D73_User", ""))
                parms.Add(RfcConfigParameters.Password, Misc.scfiReadOption("D73_Pwd", ""))
                parms.Add(RfcConfigParameters.Client, "501")
                parms.Add(RfcConfigParameters.Language, "EN")
                parms.Add(RfcConfigParameters.PoolSize, "5")
                parms.Add(RfcConfigParameters.MaxPoolSize, "10")
                parms.Add(RfcConfigParameters.IdleTimeout, "600")

            Case "Q75"
                parms.Add(RfcConfigParameters.AppServerHost, "172.16.100.223")
                parms.Add(RfcConfigParameters.SystemNumber, "75")
                parms.Add(RfcConfigParameters.SystemID, "Q75")
                parms.Add(RfcConfigParameters.User, Misc.scfiReadOption("Q71_User", ""))
                parms.Add(RfcConfigParameters.Password, Misc.scfiReadOption("Q71_Pwd", ""))
                parms.Add(RfcConfigParameters.Client, "501")
                parms.Add(RfcConfigParameters.Language, "EN")
                parms.Add(RfcConfigParameters.PoolSize, "5")
                parms.Add(RfcConfigParameters.MaxPoolSize, "10")
                parms.Add(RfcConfigParameters.IdleTimeout, "600")

            Case "PRD"
                parms.Add(RfcConfigParameters.AppServerHost, "CENTDB1")
                parms.Add(RfcConfigParameters.SystemNumber, "03")
                parms.Add(RfcConfigParameters.SystemID, "PRD")
                parms.Add(RfcConfigParameters.User, Misc.scfiReadOption("PRD_User", ""))
                parms.Add(RfcConfigParameters.Password, Misc.scfiReadOption("PRD_Pwd", ""))
                parms.Add(RfcConfigParameters.Client, "501")
                parms.Add(RfcConfigParameters.Language, "EN")
                parms.Add(RfcConfigParameters.PoolSize, "5")
                parms.Add(RfcConfigParameters.MaxPoolSize, "25")
                parms.Add(RfcConfigParameters.IdleTimeout, "600")

            Case Else

        End Select

        Return parms

    End Function

    Public Function ChangeEventsSupported() As Boolean Implements IDestinationConfiguration.ChangeEventsSupported
        Return False
    End Function


End Class

