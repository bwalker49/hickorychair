﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Services

Public Class Navigation

    Public Property CatalogList As List(Of Catalog)
    Public Property FirstLevelList As List(Of FirstLevelLink)
    Public Property SecondLevelList As List(Of SecondLevelLink)
    Public Property SecondLevelImgList As List(Of SecondLevelLink)

    Public Sub New(ByVal value As String)
        If value = "Catalog" Then
            CatalogList = GetCatalogs()
        ElseIf value = "Products" Then
            FirstLevelList = BuildProductFirstLevelList()
            BuildSecondLevel(FirstLevelList.First.ID)
        ElseIf value = "Style" Then
            FirstLevelList = BuildStyleFirstLevelList()
            BuildSecondLevel(FirstLevelList.First.ID)
        ElseIf value = "Resources" Then
            FirstLevelList = BuildResourceFirstLevelList()
            BuildSecondLevel(FirstLevelList.First.ID)
        ElseIf value = "Story" Then
            FirstLevelList = BuildStoryFirstLevelList()
            BuildSecondLevel(FirstLevelList.First.ID)
        Else
            BuildSecondLevel(value)
        End If
    End Sub

    Public Sub New(ByVal firstLevel As String, ByVal secondLevel As String)
        If firstLevel = "Catalog" Then
            CatalogList = GetCatalogs()
        ElseIf firstLevel = "Products" Then
            FirstLevelList = BuildProductFirstLevelList()
            BuildSecondLevel(secondLevel)
        ElseIf firstLevel = "Style" Then
            FirstLevelList = BuildStyleFirstLevelList()
            BuildSecondLevel(secondLevel)
        ElseIf firstLevel = "Story" Then
            FirstLevelList = BuildStoryFirstLevelList()
            BuildSecondLevel(secondLevel)
        Else
            BuildSecondLevel(secondLevel)
        End If
    End Sub

    <WebMethod()>
    Function GetCatalogs() As List(Of Catalog)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim CatalogList As New List(Of Catalog)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Navigation/SQL/GetCatalogs.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each catalog In tSet.Tables(0).Rows
                    Dim NewCatalog As New Catalog

                    NewCatalog.CatalogName = catalog("CatalogName")
                    NewCatalog.FileName = catalog("FileName")
                    NewCatalog.CoverImg = "~/Images/layout/CatalogCovers/" & catalog("CatalogName").ToString.Replace(" ", "").Replace("®", "").Replace("™", "") & ".jpg"

                    CatalogList.Add(NewCatalog)
                Next
            End If
        Catch ex As Exception
            Misc.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return CatalogList

    End Function

    '<WebMethod>
    'Function BuildFirstLevel(ByVal value As String) As String
    '    If value = "Products" Then
    '        BuildProductFirstLevelList()
    '    ElseIf value = "Style" Then
    '        BuildStyleFirstLevelList()
    '    ElseIf value = "Story" Then
    '        BuildStoryFirstLevelList()
    '    End If
    'End Function

    Public Shared Function BuildProductFirstLevelList() As List(Of FirstLevelLink)
        Dim retVal As New List(Of FirstLevelLink)

        Dim living As New FirstLevelLink
        living.ID = "Living"
        living.Title = "Living Room"

        retVal.Add(living)

        Dim dining As New FirstLevelLink
        dining.ID = "Dining"
        dining.Title = "Dining Room"

        retVal.Add(dining)

        Dim bed As New FirstLevelLink
        bed.ID = "Bed"
        bed.Title = "Bedroom"

        retVal.Add(bed)

        Dim mfy As New FirstLevelLink
        mfy.ID = "MFY"
        mfy.Title = "Made For You™"
        mfy.Clickable = False

        retVal.Add(mfy)

        Dim seating As New FirstLevelLink
        seating.ID = "Seats"
        seating.Title = "Custom Seating"
        seating.Level = "Second"

        retVal.Add(seating)

        Dim tables As New FirstLevelLink
        tables.ID = "Tables"
        tables.Title = "Custom Wood"
        tables.Level = "Second"

        retVal.Add(tables)

        Dim ni As New FirstLevelLink
        ni.ID = "NI"
        ni.Title = "New Introductions"
        ni.IsLink = True
        ni.Link = "/Products/ShowResults?NewIntros=True&SearchName=New%20Introductions"

        retVal.Add(ni)

        Return retVal
    End Function

    Public Shared Function BuildStyleFirstLevelList() As List(Of FirstLevelLink)
        Dim retVal As New List(Of FirstLevelLink)

        Dim collections As New FirstLevelLink
        collections.ID = "Collections"
        collections.Title = "Collections"

        retVal.Add(collections)

        Dim designers As New FirstLevelLink
        designers.ID = "Designers"
        designers.Title = "Designers"

        retVal.Add(designers)

        Return retVal
    End Function

    Public Shared Function BuildStoryFirstLevelList() As List(Of FirstLevelLink)
        Dim retVal As New List(Of FirstLevelLink)

        Dim past As New FirstLevelLink
        past.ID = "Past"
        past.Title = "Past & Present"

        retVal.Add(past)

        Dim craft As New FirstLevelLink
        craft.ID = "Craftsmanship"
        craft.Title = "Craftsmanship & Sustainability"

        retVal.Add(craft)

        Dim press As New FirstLevelLink
        press.ID = "Press"
        press.Title = "Seasonal News & Press"

        retVal.Add(press)

        Return retVal
    End Function

    Public Shared Function BuildResourceFirstLevelList() As List(Of FirstLevelLink)
        Dim retVal As New List(Of FirstLevelLink)

        Dim resources As New FirstLevelLink
        resources.ID = "Resources"
        resources.Title = "Design Resources"

        retVal.Add(resources)

        Return retVal
    End Function

    <WebMethod>
    Public Sub BuildSecondLevel(ByVal value As String)
        If value = "Seats" Then
            SecondLevelImgList = BuildSeatingList()
            SecondLevelList = Nothing
        ElseIf value = "Tables" Then
            SecondLevelImgList = BuildTablesList()
            SecondLevelList = Nothing
        ElseIf value = "Dining" Then
            SecondLevelImgList = BuildDiningList()
            SecondLevelList = Nothing
        ElseIf value = "Living" Then
            SecondLevelImgList = BuildLivingList()
            SecondLevelList = Nothing
        ElseIf value = "Bed" Then
            SecondLevelImgList = BuildBedList()
            SecondLevelList = Nothing
        ElseIf value = "NI" Then
            SecondLevelImgList = BuildNewIntroList()
            SecondLevelList = Nothing
        ElseIf value = "Collections" Then
            SecondLevelList = BuildCollectionList()
            SecondLevelImgList = Nothing
        ElseIf value = "Designers" Then
            SecondLevelImgList = BuildDesignerList()
            SecondLevelList = Nothing
        ElseIf value = "Resources" Then
            SecondLevelList = BuildResourceList()
            SecondLevelImgList = Nothing
        ElseIf value = "Past" Then
            SecondLevelList = BuildPastPresent()
            SecondLevelImgList = Nothing
        ElseIf value = "Craftsmanship" Then
            SecondLevelList = BuildCraftsmanship()
            SecondLevelImgList = Nothing
        ElseIf value = "Press" Then
            SecondLevelList = BuildPress()
            SecondLevelImgList = Nothing
        End If

    End Sub

    Public Shared Function BuildSeatingList() As List(Of SecondLevelLink)
        Dim retVal As New List(Of SecondLevelLink)
        Dim typeImagePath As String = "~/Images/layout/NavImages/Type/"
        Dim subtypeImagePath As String = "~/Images/layout/NavImages/Subtype/"

        Dim sil As New SecondLevelLink
        sil.ID = "SILHOUETTES"
        sil.Title = "Silhouettes"
        sil.Category = "Link"

        retVal.Add(sil)

        'Dim coh As New SecondLevelLink
        'coh.ID = "/Resources/CustomersOwn"
        'coh.Title = "Customer's Own Hardware & Customer's Own Veneer"
        'coh.Category = "Link"

        'ResourceList.Add(coh)

        Dim m2m As New SecondLevelLink
        m2m.ID = "M2MSEATING"
        m2m.Title = "M2M® Made To Measure"
        m2m.Category = "Link"

        retVal.Add(m2m)

        Dim jules As New SecondLevelLink
        jules.ID = "JULES"
        jules.Title = "Jules Chair"
        jules.Category = "Link"

        retVal.Add(jules)

        Dim vistage As New SecondLevelLink
        vistage.ID = "VISTAGE"
        vistage.Title = "Vistage Sofa/Sectional"
        vistage.Category = "Link"

        retVal.Add(vistage)

        Return retVal
    End Function

    Public Shared Function BuildTablesList() As List(Of SecondLevelLink)
        Dim retVal As New List(Of SecondLevelLink)

        Dim armoires As New SecondLevelLink
        armoires.ID = "PERSPREF"
        armoires.Title = "Personal Preference Custom Dining Tables"
        armoires.Category = "Link"

        retVal.Add(armoires)

        Dim bar As New SecondLevelLink
        bar.ID = "M2MTABLE"
        bar.Title = "M2M® Made To Measure"
        bar.Category = "Link"

        retVal.Add(bar)

        Dim mikos As New SecondLevelLink
        mikos.ID = "MIKOS"
        mikos.Title = "Mikos Cocktail"
        mikos.Category = "Link"

        retVal.Add(mikos)

        Dim textures As New SecondLevelLink
        textures.ID = "TEXTURES"
        textures.Title = "Textures Credenzas"
        textures.Category = "Link"

        retVal.Add(textures)

        Return retVal
    End Function

    Public Shared Function BuildDiningList() As List(Of SecondLevelLink)
        Dim retVal As New List(Of SecondLevelLink)

        Dim dining As New SecondLevelLink
        dining.ID = "74"
        dining.Title = "Dining Tables"
        dining.Category = "Type"

        retVal.Add(dining)

        Dim center As New SecondLevelLink
        center.ID = "942"
        center.Title = "Center Tables"
        center.Category = "Subtype"

        retVal.Add(center)

        Dim dinchr As New SecondLevelLink
        dinchr.ID = "73"
        dinchr.Title = "Dining Chairs"
        dinchr.Category = "Type"

        retVal.Add(dinchr)

        Dim setban As New SecondLevelLink
        setban.ID = "48"
        setban.Title = "Settees & Banquettes"
        setban.Category = "Type"

        retVal.Add(setban)

        'Dim accent As New SecondLevelLink
        'accent.ID = "1500"
        'accent.Title = "Accent Chairs"
        'accent.Category = "Subtype"

        'retVal.Add(accent)

        Dim bcstl As New SecondLevelLink
        bcstl.ID = "61"
        bcstl.Title = "Bar & Counter Stools"
        bcstl.Category = "Type"

        retVal.Add(bcstl)

        Dim chest As New SecondLevelLink
        chest.ID = "32"
        chest.Title = "Chests"
        chest.Category = "Type"

        retVal.Add(chest)

        Dim sbrd As New SecondLevelLink
        sbrd.ID = "61"
        sbrd.Title = "Sideboards"
        sbrd.Category = "Subtype"

        retVal.Add(sbrd)

        Dim bar As New SecondLevelLink
        bar.ID = "70"
        bar.Title = "Bar & Bar Carts"
        bar.Category = "Type"

        retVal.Add(bar)

        Dim book As New SecondLevelLink
        book.ID = "92,57"
        book.Title = "Bookcases & Display Cabinets"
        book.Category = "Subtype"

        retVal.Add(book)

        Dim enter As New SecondLevelLink
        enter.ID = "250,56"
        enter.Title = "Armoires & Entertainment Cabinets"
        enter.Category = "Subtype"

        retVal.Add(enter)

        Dim mirror As New SecondLevelLink
        mirror.ID = "17,1507,130"
        mirror.Title = "Mirrors, Trays & Accents"
        mirror.Category = "Subtype"

        retVal.Add(mirror)

        Return retVal
    End Function

    Public Shared Function BuildLivingList() As List(Of SecondLevelLink)
        Dim retVal As New List(Of SecondLevelLink)

        Dim sofas As New SecondLevelLink
        sofas.ID = "79"
        sofas.Title = "Sofas & Loveseats"
        sofas.Category = "Type"

        retVal.Add(sofas)

        Dim setban As New SecondLevelLink
        setban.ID = "48"
        setban.Title = "Settees & Banquettes"
        setban.Category = "Type"

        retVal.Add(setban)

        Dim sectionals As New SecondLevelLink
        sectionals.ID = "14"
        sectionals.Title = "Sectionals"
        sectionals.Category = "Type"

        retVal.Add(sectionals)

        Dim chrcs As New SecondLevelLink
        chrcs.ID = "81"
        chrcs.Title = "Chairs & Chaises"
        chrcs.Category = "Type"

        retVal.Add(chrcs)

        'Dim accent As New SecondLevelLink
        'accent.ID = "1500"
        'accent.Title = "Accent Chairs"
        'accent.Category = "Subtype"

        'retVal.Add(accent)

        Dim ottoman As New SecondLevelLink
        ottoman.ID = "76"
        ottoman.Title = "Ottomans & Benches"
        ottoman.Category = "Type"

        retVal.Add(ottoman)

        Dim cocktail As New SecondLevelLink
        cocktail.ID = "42"
        cocktail.Title = "Cocktail Tables"
        cocktail.Category = "Subtype"

        retVal.Add(cocktail)

        Dim side As New SecondLevelLink
        side.ID = "78"
        side.Title = "End & Side Tables"
        side.Category = "Subtype"

        retVal.Add(side)

        Dim center As New SecondLevelLink
        center.ID = "942,79"
        center.Title = "Center Tables & Game Tables"
        center.Category = "Subtype"

        retVal.Add(center)

        Dim desk As New SecondLevelLink
        desk.ID = "34,72"
        desk.Title = "Desk & Consoles"
        desk.Category = "Type"

        retVal.Add(desk)

        Dim book As New SecondLevelLink
        book.ID = "92,57"
        book.Title = "Bookcases & Display Cabinets"
        book.Category = "Subtype"

        retVal.Add(book)

        Dim enter As New SecondLevelLink
        enter.ID = "250,56"
        enter.Title = "Armoires & Entertainment Cabinets"
        enter.Category = "Subtype"

        retVal.Add(enter)

        Dim bar As New SecondLevelLink
        bar.ID = "70"
        bar.Title = "Bar & Bar Carts"
        bar.Category = "Type"

        retVal.Add(bar)

        Dim mirror As New SecondLevelLink
        mirror.ID = "17,1507,130"
        mirror.Title = "Mirrors, Trays & Accents"
        mirror.Category = "Subtype"

        retVal.Add(mirror)

        Dim lighting As New SecondLevelLink
        lighting.ID = "132"
        lighting.Title = "Lighting"
        lighting.Category = "Type"

        retVal.Add(lighting)

        Return retVal
    End Function

    Public Shared Function BuildBedList() As List(Of SecondLevelLink)
        Dim retVal As New List(Of SecondLevelLink)

        Dim beds As New SecondLevelLink
        beds.ID = "25"
        beds.Title = "Beds"
        beds.Category = "Type"

        retVal.Add(beds)

        Dim dresser As New SecondLevelLink
        dresser.ID = "29"
        dresser.Title = "Dressers"
        dresser.Category = "Type"

        retVal.Add(dresser)

        Dim chest As New SecondLevelLink
        chest.ID = "32"
        chest.Title = "Chests"
        chest.Category = "Type"

        retVal.Add(chest)

        Dim nightstand As New SecondLevelLink
        nightstand.ID = "82"
        nightstand.Title = "Nightstand & Bedside Tables"
        nightstand.Category = "Subtype"

        retVal.Add(nightstand)

        Dim side As New SecondLevelLink
        side.ID = "78"
        side.Title = "End & Side Tables"
        side.Category = "Subtype"

        retVal.Add(side)

        Dim book As New SecondLevelLink
        book.ID = "92,57"
        book.Title = "Bookcases & Display Cabinets"
        book.Category = "Subtype"

        retVal.Add(book)

        Dim enter As New SecondLevelLink
        enter.ID = "250,56"
        enter.Title = "Armoires & Entertainment Cabinets"
        enter.Category = "Subtype"

        retVal.Add(enter)

        Dim desk As New SecondLevelLink
        desk.ID = "34,72"
        desk.Title = "Desk & Consoles"
        desk.Category = "Type"

        retVal.Add(desk)

        Dim center As New SecondLevelLink
        center.ID = "942,79"
        center.Title = "Center Tables & Game Tables"
        center.Category = "Subtype"

        retVal.Add(center)

        Dim bar As New SecondLevelLink
        bar.ID = "70"
        bar.Title = "Bar & Bar Carts"
        bar.Category = "Type"

        retVal.Add(bar)

        'Dim accent As New SecondLevelLink
        'accent.ID = "1500"
        'accent.Title = "Accent Chairs"
        'accent.Category = "Subtype"

        'retVal.Add(accent)

        Dim chrcs As New SecondLevelLink
        chrcs.ID = "81"
        chrcs.Title = "Chairs & Chaises"
        chrcs.Category = "Type"

        retVal.Add(chrcs)

        Dim setban As New SecondLevelLink
        setban.ID = "48"
        setban.Title = "Settees & Banquettes"
        setban.Category = "Type"

        retVal.Add(setban)

        Dim mirror As New SecondLevelLink
        mirror.ID = "17,1507,130"
        mirror.Title = "Mirrors, Trays & Accents"
        mirror.Category = "Subtype"

        retVal.Add(mirror)

        Dim lighting As New SecondLevelLink
        lighting.ID = "132"
        lighting.Title = "Lighting"
        lighting.Category = "Type"

        retVal.Add(lighting)


        Return retVal
    End Function

    Public Shared Function BuildNewIntroList() As List(Of SecondLevelLink)
        Dim retVal As New List(Of SecondLevelLink)

        Dim living As New SecondLevelLink
        living.ID = "2"
        living.Title = "Living Room"
        living.Category = "Room"

        retVal.Add(living)

        Dim dining As New SecondLevelLink
        dining.ID = "1"
        dining.Title = "Dining Room"
        dining.Category = "Room"

        retVal.Add(dining)

        Dim bed As New SecondLevelLink
        bed.ID = "3"
        bed.Title = "Bedroom"
        bed.Category = "Room"

        retVal.Add(bed)

        Return retVal
    End Function

    Public Shared Function BuildCollectionList() As List(Of SecondLevelLink)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim CollectionList As New List(Of SecondLevelLink)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Navigation/SQL/GetCollections.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            Dim collList As New List(Of String)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each collection In tSet.Tables(0).Rows
                    collList.Add(collection("CollectionName"))
                Next
                collList.Add("Lighting & Accessories")

                collList.Sort()

                For Each coll In collList
                    Dim NewCollection As New SecondLevelLink

                    If coll = "Lighting & Accessories" Then
                        NewCollection.ID = "404,402,17,1507,130"
                        NewCollection.Title = coll
                        NewCollection.Category = "Subtype"

                        CollectionList.Add(NewCollection)
                    Else
                        Dim valrow As DataRow() = tSet.Tables(0).Select("CollectionName = '" & coll & "'")

                        NewCollection.ID = valrow(0).Item("CollectionNo")
                        NewCollection.Title = coll
                        NewCollection.Category = "Collection"

                        CollectionList.Add(NewCollection)
                    End If
                Next
            End If
        Catch ex As Exception
            'MiscCode.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return CollectionList
    End Function

    Public Shared Function BuildDesignerList() As List(Of SecondLevelLink)
        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("Century").ConnectionString.ToString)
        dbConn.Open()

        Dim DesignerList As New List(Of SecondLevelLink)

        Try
            Dim SQL As String = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("/Models/Navigation/SQL/GetDesigners.sql"))

            Dim tA As New SqlDataAdapter(SQL, dbConn)

            Dim tSet As New DataSet
            tA.Fill(tSet)

            If tSet.Tables(0).Rows.Count > 0 Then
                For Each designer In tSet.Tables(0).Rows
                    Dim NewDesigner As New SecondLevelLink

                    NewDesigner.ID = designer("Designer")
                    NewDesigner.Title = designer("Designer_Name")
                    NewDesigner.Img = "~/Images/layout/Designers/" & designer("Designer") & "_Tiny.jpg"
                    NewDesigner.Category = "Designer"

                    DesignerList.Add(NewDesigner)
                Next
            End If
        Catch ex As Exception
            'MiscCode.LogError(ex)
        Finally
            dbConn.Close()
        End Try

        Return DesignerList

    End Function

    Public Shared Function BuildResourceList() As List(Of SecondLevelLink)
        Dim ResourceList As New List(Of SecondLevelLink)

        Dim art As New SecondLevelLink
        art.ID = "/Misc/ArtistStudio"
        art.Title = "Artist's Studio"
        art.Category = "Link"

        ResourceList.Add(art)

        Dim coh As New SecondLevelLink
        coh.ID = "/Resources/CustomersOwn"
        coh.Title = "Customer's Own Hardware & Customer's Own Veneer"
        coh.Category = "Link"

        ResourceList.Add(coh)

        Dim fabrics As New SecondLevelLink
        fabrics.ID = "/Resources/FabSearch"
        fabrics.Title = "Fabrics"
        fabrics.Category = "Link"

        ResourceList.Add(fabrics)

        Dim finishes As New SecondLevelLink
        finishes.ID = "/Resources/Finishes"
        finishes.Title = "Finishes"
        finishes.Category = "Link"

        ResourceList.Add(finishes)

        Dim leather As New SecondLevelLink
        leather.ID = "/Resources/FabSearch?Type=Leather"
        leather.Title = "Leather"
        leather.Category = "Link"

        ResourceList.Add(leather)

        Dim mono As New SecondLevelLink
        mono.ID = "/Misc/Monogramming"
        mono.Title = "Monogramming"
        mono.Category = "Link"

        ResourceList.Add(mono)

        Dim nail As New SecondLevelLink
        nail.ID = "/Misc/NailTrim"
        nail.Title = "Nail Trim"
        nail.Category = "Link"

        ResourceList.Add(nail)

        'Dim ods As New SecondLevelLink
        'ods.ID = "/Resources/OnlineDesignStudio"
        'ods.Title = "Online Design Studio"
        'ods.Category = "Link"

        'ResourceList.Add(ods)

        Dim armoires As New SecondLevelLink
        armoires.ID = "/Products/CustomPrograms/PERSPREF"
        armoires.Title = "Personal Preference Custom Dining Tables"
        armoires.Category = "Link"

        ResourceList.Add(armoires)

        'Dim rp As New SecondLevelLink
        'rp.ID = "https://microd.hickorychair.com/omniplanner.aspx"
        'rp.Title = "Room Planner"
        'rp.Category = "Link"

        'ResourceList.Add(rp)

        Dim sil As New SecondLevelLink
        sil.ID = "/Products/CustomPrograms/SILHOUETTES"
        sil.Title = "Silhouettes"
        sil.Category = "Link"

        ResourceList.Add(sil)

        Dim sh As New SecondLevelLink
        sh.ID = "/Documents/Resources/StockingHardware.pdf"
        sh.Title = "Stocking Hardware"
        sh.Category = "Link"

        ResourceList.Add(sh)

        Dim sc As New SecondLevelLink
        sc.ID = "/Misc/Striping"
        sc.Title = "Striping Colors"
        sc.Category = "Link"

        ResourceList.Add(sc)

        Dim trim As New SecondLevelLink
        trim.ID = "/Resources/FabSearch?Type=Trims"
        trim.Title = "Trims"
        trim.Category = "Link"

        ResourceList.Add(trim)

        Return ResourceList
    End Function

    Public Shared Function BuildPastPresent() As List(Of SecondLevelLink)
        Dim pp As New List(Of SecondLevelLink)

        Dim cs As New SecondLevelLink
        cs.ID = "/Misc/CentennialStory"
        cs.Title = "Our Centennial Story"
        cs.Category = "Link"

        pp.Add(cs)

        Dim hs As New SecondLevelLink
        hs.ID = "/Misc/HickorySnapshot"
        hs.Title = "Historic Hickory Snapshot"
        hs.Category = "Link"

        pp.Add(hs)

        Dim thennow As New SecondLevelLink
        thennow.ID = "/Misc/ThenNow"
        thennow.Title = "Then & Now Timeline"
        thennow.Category = "Link"

        pp.Add(thennow)

        Return pp
    End Function

    Public Shared Function BuildCraftsmanship() As List(Of SecondLevelLink)
        Dim craft As New List(Of SecondLevelLink)

        Dim au As New SecondLevelLink
        au.ID = "/Misc/ArtisanalUpholstery"
        au.Title = "Artisanal Upholstery"
        au.Category = "Link"

        craft.Add(au)

        Dim ccw As New SecondLevelLink
        ccw.ID = "/Misc/CustomWoodworking"
        ccw.Title = "Classic, Custom Woodworking"
        ccw.Category = "Link"

        craft.Add(ccw)

        Dim er As New SecondLevelLink
        er.ID = "/Misc/EnvironmentalResponsibility"
        er.Title = "Environmental Responsibility"
        er.Category = "Link"

        craft.Add(er)

        Dim ee As New SecondLevelLink
        ee.ID = "/Misc/EmployeeEmpowerment"
        ee.Title = "Employee Empowerment (Edge)"
        ee.Category = "Link"

        craft.Add(ee)

        Return craft
    End Function

    Public Shared Function BuildPress() As List(Of SecondLevelLink)
        Dim press As New List(Of SecondLevelLink)

        'Dim il As New SecondLevelLink
        'il.ID = "/Resources/ImageLibrary"
        'il.Title = "Image Library"
        'il.Category = "Link"

        'press.Add(il)

        Dim mfy As New SecondLevelLink
        mfy.ID = "/Content/MadeForYou"
        mfy.Title = "Made For You™ Magazine"
        mfy.Category = "Link"

        press.Add(mfy)

        Dim pr As New SecondLevelLink
        pr.ID = "/Content/PressRelease"
        pr.Title = "Press Releases"
        pr.Category = "Link"

        press.Add(pr)

        Return press
    End Function

    'Function DoesImageExist(ByVal file As String) As Boolean
    '    If file.Exi Then
    'End Function

    Public Class Catalog
        Public Property CatalogName As String
        Public Property FileName As String
        Public Property CoverImg As String
    End Class

    Public Class FirstLevelLink
        Public Property ID As String
        Public Property Title As String
        Public Property Level As String
        Public Property Clickable As Boolean = True
        Public Property IsLink As Boolean = False
        Public Property Link As String
    End Class

    Public Class SecondLevelLink
        Public Property ID As String
        Public Property Title As String
        Public Property Img As String
        Public Property Category As String
    End Class
End Class
