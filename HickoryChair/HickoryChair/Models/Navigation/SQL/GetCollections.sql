﻿SELECT
     replace(replace(CollectionName,'Casegoods',''),'Upholstery','') as CollectionName, 
     STUFF(
         (SELECT DISTINCT ',' + CollectionNo
          FROM Collections
          WHERE replace(replace(CollectionName,'Casegoods',''),'Upholstery','') = replace(replace(a.CollectionName,'Casegoods',''),'Upholstery','') 
          FOR XML PATH (''))
          , 1, 1, '')  AS CollectionNo,
		  isnull(IntroImageName, '') AS IntroImageName,
		  isnull(IntroPageText, '') AS IntroPageText
FROM Collections AS a
INNER JOIN Items i ON i.CollectionNo = a.CollectionNo
INNER JOIN ItemsGoodToShowHCConsumer ig ON ig.SKU = i.SKU
WHERE WebsiteID = 'HickoryChair'
AND ShowConsumer = 1
AND DiscontinuedDate IS NULL
AND CollectionType <> 'FABRIC'
GROUP BY replace(replace(CollectionName,'Casegoods',''),'Upholstery',''), IntroImageName, IntroPageText
ORDER BY
replace(replace(CollectionName,'Casegoods',''),'Upholstery','')