﻿SELECT DISTINCT
	i.Designer,
	id.Designer_Name
FROM ItemsGoodToShowHCConsumer ihc
INNER JOIN ITems i on i.sku=ihc.sku
INNER JOIN Items_Designers_2 id on id.Designer_Code=i.Designer
WHERE 
i.Designer IS NOT NULL
AND id.CallOutOnWeb=1 