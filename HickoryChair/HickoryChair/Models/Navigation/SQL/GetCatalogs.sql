﻿SELECT 
CatalogName,
CASE WHEN ExternalLink IS NOT NULL THEN
ExternalLink
WHEN CatalogFile IS NOT NULL THEN
CONCAT('/Documents/Catalogs/', CatalogFile)
ELSE
''
END AS FileName,
isnull(CatalogFile, '') AS PDFFile
FROM Catalogs
WHERE WebsiteID = 'HickoryChair'
AND CatalogType NOT LIKE 'Brandbook%'
ORDER BY SortOrder