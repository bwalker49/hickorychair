﻿Imports System.Data.SqlClient
Imports System.Web.Mvc
Imports System.Web.Mvc.Ajax
Imports System.Xml

Public Class Dealers

    Public Property Zip As String
    Public Property Radius As String
    Public Property UserLatitude As Decimal
    Public Property UserLongitude As Decimal

    Public Property MapRequestAO As AjaxOptions
        Get
            Dim opts = New AjaxOptions()
            opts.HttpMethod = "POST"
            opts.UpdateTargetId = "WhereToBuyMap"
            opts.OnSuccess = "CreateDealerMapObjects"
            opts.OnBegin = "return StartValidation();"
            Return opts
        End Get
        Set(value As AjaxOptions)

        End Set
    End Property

    Public Property DealerLocationList As List(Of DealerLocation) = New List(Of DealerLocation)()
    Public Property ApiKey As String = ConfigurationManager.AppSettings("GoogleMapsKey")

    Public Sub GetSearchResults()

        GetLatLong(Me.Zip, Me.UserLatitude, Me.UserLongitude)

        Dim dbConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("HMWeb").ConnectionString.ToString)
        dbConn.Open()

        Dim pSQL As String = IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Models/WhereToBuy/GetDealersByAddress.sql"))
        Dim tA As New SqlDataAdapter(pSQL, dbConn)

        tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = UserLatitude, .ParameterName = "@UserLat", .SqlDbType = SqlDbType.Float})
        tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = UserLongitude, .ParameterName = "@UserLong", .SqlDbType = SqlDbType.Float})
        tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = Radius, .ParameterName = "@Distance", .SqlDbType = SqlDbType.Float})
        tA.SelectCommand.Parameters.Add(New SqlParameter() With {.Value = 25, .ParameterName = "@MaxDealers", .SqlDbType = SqlDbType.Int})

        Dim ds As New DataTable
        tA.Fill(ds)

        For Each item As DataRow In ds.Rows
            Dim dl = New DealerLocation()
            dl.Address = item("Address")
            dl.City = item("City")
            dl.Company = item("Company")
            dl.DistFrom = item("DistFrom")
            dl.Latitude = item("Latitude")
            dl.Longitude = item("Longitude")
            dl.Phone = item("Phone")
            dl.State = item("State")
            dl.Zip = item("Zip")
            dl.URL = item("URL")
            Me.DealerLocationList.Add(dl)
        Next

    End Sub

    Public Property RadiusSelections As List(Of SelectListItem) = New List(Of SelectListItem)({
                                New SelectListItem With {.Text = "10 miles", .Value = "10"},
                                New SelectListItem With {.Text = "25 miles", .Value = "25", .Selected = True},
                                New SelectListItem With {.Text = "50 miles", .Value = "50"},
                                New SelectListItem With {.Text = "100 miles", .Value = "100"}})

    Private Sub GetLatLong(ByVal inAddress As String, ByRef outLat As Decimal, ByRef outLong As Decimal)
        Try
            Dim webClient As New Net.WebClient
            Dim result As String = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/xml?address=" & inAddress & "&key=" + ApiKey)

            Dim xmlDoc As New XmlDocument()
            xmlDoc.LoadXml(result)
            Dim m_nodelist As XmlNodeList

            m_nodelist = xmlDoc.SelectNodes("/GeocodeResponse/result/geometry/location/lat")
            If m_nodelist.Count > 0 Then
                'Utility.Runlog(Utility.SubName,m_nodelist(0).InnerText)
                outLat = m_nodelist(0).InnerText
            End If

            m_nodelist = xmlDoc.SelectNodes("/GeocodeResponse/result/geometry/location/lng")
            If m_nodelist.Count > 0 Then
                'Utility.Runlog(Utility.SubName,m_nodelist(0).InnerText)
                outLong = m_nodelist(0).InnerText
            End If

        Catch ex As Exception

        Finally

        End Try
    End Sub

    Public Class DealerLocation
        Public Property Company As String
        Public Property Address As String
        Public Property City As String
        Public Property State As String
        Public Property Zip As String
        Public Property Phone As String
        Public Property DistFrom As Decimal
        Public Property Latitude As Decimal
        Public Property Longitude As Decimal
        Public Property URL As String

    End Class
End Class
