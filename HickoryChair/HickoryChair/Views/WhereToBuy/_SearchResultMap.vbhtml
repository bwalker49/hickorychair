﻿@ModelType Models.WhereToBuy.WhereToBuy

@Code
    Layout = Nothing
End Code

<style>
    #map {
        height: 800px;
        width: 100%;
    }

    .dealerItem {
        padding: 15px 0 15px 0;
    }

    #dealericons {
        margin:10px 0;
    }

        #dealericons div {
            margin:5px 0;
        }

    address {
        margin-bottom: 0;
    }
</style>


<div id="map"></div>

<div class="row">
    @For Each item As Models.WhereToBuy.WhereToBuy.DealerLocation In Model.DealerLocationList
        @<div Class="col-md-4 dealerItem">
            <h4>@item.Company</h4>
            <address>
                @item.Address <br />
                @If item.Address2.Length > 1 Then
                    @item.Address2
                    @<br/>
                End If
                @item.City, @item.State @item.Zip <br />
            </address>
            <b>Tel:</b> @item.Phone <br />
            <b>Distance: </b> @Decimal.Round(item.DistFrom, 2) miles<br />
            @If Not item.URL = "" Then
                @<a href='http://@item.URL' class='aboutusLink'><b>WEBSITE</b></a>@<br />
            End If
            <a href='http://maps.google.com/maps?saddr=@Model.Zip&daddr=@item.Address @item.City, @item.State @item.Zip' target='_blank'>DIRECTIONS</a>
            <div id="dealericons">
                @If item.IsTradeShowroom Then
                    @<div><img src="~/Images/layout/HCShowrooms/HC_Trade_Showroom.jpg" width="160" /></div>
                End If
                @If item.IsBoutique Then
                    @<div><img src="~/Images/layout/HCShowrooms/HC_Boutique.jpg" width="160" /></div>
                End If
                @If item.IsHCCCertified Then
                    @<div><img src="~/Images/layout/HCShowrooms/HC_University_icon_Dealer.png" width="116" /></div>
                End If
            </div>
        </div>

    Next
</div>

<script>
    function CreateDealerMapObjects() {
        var model = @Html.Raw(Json.Encode(Model));
        //alert($('#zipInput').val())
        if (model.DealerLocationList[0].IsShowroom && model.DealerLocationList[0].ShowroomZipList.indexOf($("#zipInput").val()) >= 0) {            
            getShowroomID($('#zipInput').val())
        } else {
            console.log(model);
            initMap(model);
        }        
    }

    function initMap(model) {
        var map = new google.maps.Map(
            document.getElementById('map')
            //,{center: { lat: model.UserLatitude, lng: model.UserLongitude }}
        );

        var markersArr = [];
        var infoWindows = [];

        for (var i = 0; i < model.DealerLocationList.length; i++) {
            var marker = new google.maps.Marker({
                title: model.DealerLocationList[i].Company,
                position: new google.maps.LatLng(model.DealerLocationList[i].Latitude, model.DealerLocationList[i].Longitude),
                animation: google.maps.Animation.DROP,
                map: map
            });

			var mc = "";

			mc += "<div class='locatorInfo'><h2>" + model.DealerLocationList[i].Company + "</h2><p>";
			mc += "<address>" + model.DealerLocationList[i].Address + "<br />" + model.DealerLocationList[i].City + ", " + model.DealerLocationList[i].State + " " + model.DealerLocationList[i].Zip + "</address><b>Tel:</b> " + model.DealerLocationList[i].Phone + " &nbsp&nbsp&nbsp&nbsp <b></b> </p>";
			mc += "<b>Distance:</b> " + Math.trunc(model.DealerLocationList[i].DistFrom) + " miles<p>";
			if (model.DealerLocationList[i].URL.length > 0) { mc += "<a href='http://" + model.DealerLocationList[i].URL + "' class='aboutusLink' target='_blank'>RETAILER WEBSITE</a><p>"; }
			mc += "<a href='http://maps.google.com/maps?saddr=" + model.Zip + "&daddr=" + model.DealerLocationList[i].Address + " " + model.DealerLocationList[i].City + ", " + model.DealerLocationList[i].StateProv + " " + model.DealerLocationList[i].Zip + "' class='aboutusLink' target='_blank'>DIRECTIONS</a></div>";

			var infoWindow = new google.maps.InfoWindow({ content: mc });
			markersArr.push(marker);
			infoWindows.push(infoWindow);
			listenMarker(marker, infoWindow);
		}

        var radCir = new google.maps.Circle;
        radCir.setRadius(model.Radius * 900);
        radCir.setCenter({lat: model.UserLatitude, lng: model.UserLongitude});
        map.fitBounds(radCir.getBounds());


        var marker = new google.maps.Marker({ title: "YOU", position: { lat: model.UserLatitude, lng: model.UserLongitude }, animation: google.maps.Animation.DROP, map: map });
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
        var markerContent = "Your Location: " + model.Zip;
        var infoWindow = new google.maps.InfoWindow({ content: markerContent });

        function listenMarker(marker, window) {
		    // so marker is associated with the closure created for the listenMarker function call
		    google.maps.event.addListener(marker, 'click', function () {
			    //map.setCenter(marker.position);
			    window.open(map, marker);
			    for (var i = 0; i < infoWindows.length; i++) {
				    if (infoWindows[i] != window) {
					    infoWindows[i].close();
				    }
			    }
		    });
	    }
    }
    
    function getShowroomID(zip) {
        $.ajax({
            type: "POST",
            url: '/WhereToBuy/GetShowroomID',
            data: "{zip:'" + zip + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            success: function (data) {               
                var url = window.location.origin + '/Dealer/HCShowroom/' + data                    
                window.location.replace(url)
            }
        });
    }
</script>