﻿@Code
    ViewData("Title") = "Hickory Chair Dealer Search"
    Layout = "~/Views/Shared/_Layout.vbhtml"


End Code

@ModelType Models.WhereToBuy.WhereToBuy

@section styles
    <style>
        .container-fluid {
            font-family:'Verlag-Light';
            margin-top:25px;
        }

        h4 {
            font-size:1.25em;
        }
    </style>
End Section

<div class="container-fluid">

    @*<div class="row">
            <div class="detailBox">
                <ul class="inline breadcrumb-links">
                    <li>
                        <a>WHERE TO BUY</a>
                    </li>
                </ul>
            </div>
            <div class="detailBox"></div>
            <div class="detailBox"></div>
        </div>*@


    <div class="row">
        <div class="col-md-8 offset-2">
            <p>
                We pride ourselves on the extensive customization options we offer to our customers.
                We encourage you to work closely with your local retailer or interior designer to ensure
                you are 100% satisfied with your final product. Thank you for your interest in Hickory Chair and our sister companies.
            </p>
            <p>
                Please find our U.S. and Canadian retailers near you by entering City or Zip / Postal Code.
            </p>
            <p>
                Click on GO button to access a retailer list:
            </p>
        </div>
    </div>

    @Using Ajax.BeginForm("GetSearchResultsMap", "WhereToBuy", Model, Model.MapRequestAO, New With {.id = "WhereToBuySearchForm"})
        @<div Class="row">
            <div Class="col-md-2 offset-2">
                <div class="form-group">
                    <Label for="zipInput">
                        City Or Zip/Postal:
                    </Label>
                    @Html.TextBoxFor(Function(model) model.Zip, New With {.class = "form-control", .id = "zipInput"})
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <Label for="radiusSelect">
                        Radius:
                    </Label>
                    @Html.DropDownListFor(Function(model) model.Radius, Model.RadiusSelections, New With {.class = "form-control"})

                </div>
            </div>
            <div class="col-md-2">
                <input style="margin-top: 32px;" type="submit" value="Go" id="contactUsSubmitFormButton" class="btn btn-outline-secondary">
            </div>

        </div>
    End Using
    <div class="row min-vh-100">
        <div class="col-md-8 offset-2">
            <div id="WhereToBuyMap"></div>
        </div>
    </div>
</div>

@section scripts
    <script>
        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        $(document).ready(function () {
            if(getUrlVars()['zip'].length > 0){
                $('#zipInput').val(getUrlVars()['zip'])
                $('#contactUsSubmitFormButton').click();
            }
        })
    </script>
End Section

    <script type="text/javascript">
    function StartValidation() {
        if ($("#zipInput").val() == "") {
            alert("Please enter a City Or Zip/Postal code.")
            return false;
        }
        return true;
    }

    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=@(Model.ApiKey)">
    </script>

