﻿@Code
    ViewData("Title") = "Hickory Chair Furniture Co."
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/home.css" rel="stylesheet" />
End Section

<div class="slick-gallery"></div>


<div id="block-link-wrapper">
    <div class="block-link" style="background:url('/Images/layout/HomeImages/big-block-1.jpg');background-size:100% 100%;">
        <div class="block-overlay">

        </div>
        <div class="block-overlay-text-wrapper">
            <div class="block-overlay-text">
                <p class="block-link-tagline">Silhouettes</p>
                <p class="block-link-click"><a href="@Url.Action("CustomPrograms", "Products", New With {.program = "SILHOUETTES"})">Made For You</a></p>
            </div>
        </div>
    </div>
    <div class="block-link" style="background:url('/Images/layout/HomeImages/big-block-2.jpg');background-size:100% 100%;">
        <div class="block-overlay">

        </div>
        <div class="block-overlay-text-wrapper">
            <div class="block-overlay-text">
                <p class="block-link-tagline">M2M® Made To Measure Tables</p>
                <p class="block-link-click"><a href="@Url.Action("CustomPrograms", "Products", New With {.program = "M2MTABLE"})">Made For You</a></p>
            </div>
        </div>
    </div>


    <div class="block-link-three" style="background:url('/Images/layout/HomeImages/small-block-1.jpg');background-size:100% 100%;">
        <div class="block-overlay">

        </div>
        <div class="block-overlay-text-wrapper">
            <div class="block-overlay-text">
                <p class="block-link-tagline">New Introductions</p>
                <p class="block-link-click"><a href="@Url.Action("ShowResults", "Products", New With {.NewIntros = True, .SearchName = "New Introductions"})">View New Introductions</a></p>
            </div>
        </div>
    </div>

    <div class="block-link-three" style="background:url('/Images/layout/HomeImages/small-block-2.jpg');background-size:100% 100%;">
        <div class="block-overlay">

        </div>
        <div class="block-overlay-text-wrapper">
            <div class="block-overlay-text">
                <p class="block-link-tagline">Fabrics & Leather</p>
                <p class="block-link-click"><a href="@Url.Action("FabSearch", "Resources")">View Fabrics, Leather & Trim</a></p>
            </div>
        </div>
    </div>

    <div class="block-link-three" style="background:url('/Images/layout/HomeImages/small-block-3.jpg');background-size:100% 100%;">
        <div class="block-overlay">

        </div>
        <div class="block-overlay-text-wrapper">
            <div class="block-overlay-text">
                <p class="block-link-tagline">Suzanne Kasler Collection</p>
                <p class="block-link-click"><a href="@Url.Action("ShowResults", "Products", New With {.CollectionID = "FA,FB", .SearchName = "Suzanne Kasler Collection"})">View the Suzanne Kasler Collection</a></p>
            </div>
        </div>
    </div>
</div>

@*<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-a1eaa6ed-0e94-45f0-a12b-6c5e78881259" style="margin-left:2.5%;width:95%;margin-bottom:50px;"></div>*@


@section scripts
    <script>
        function BuildGallery() {
            $.ajax({
                type: "POST",
                url: '/Misc/HomeGallery',
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (data) {
                    $('.slick-gallery').html(data);
                    $('.slick-gallery').slick({
                        arrows: false,
                        dots: true,
                        autoplay: true,
                        fade: true,
                        speed: 1750
                    });
                    if (window.outerWidth < 767) {
                        $('.gallery-sku-overlay').each(function (e) {
                            $(this).text($(this).text().split('-')[0]);
                        })
                    }
                }
            });
        }       

        $(document).ready(function () {
            BuildGallery();            
        })
    </script>
End Section

@*<div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS and JavaScript.</p>
        <p><a href="https://asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET MVC gives you a powerful, patterns-based way to build dynamic websites that
                enables a clean separation of concerns and gives you full control over markup
                for enjoyable, agile development.
            </p>
            <p><a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301865">Learn more &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.</p>
            <p><a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301866">Learn more &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>You can easily find a web hosting company that offers the right mix of features and price for your applications.</p>
            <p><a class="btn btn-default" href="https://go.microsoft.com/fwlink/?LinkId=301867">Learn more &raquo;</a></p>
        </div>
    </div>*@
