﻿@ModelType Misc

@For Each row In Model.GallerySlides
    Dim imgPath = "~/Images/layout/HomeGallery/"
    Dim imgName = row.Img

    imgPath = imgPath & imgName

    @<div>
    <img src="@Url.Content(imgPath)" />
    @If row.description <> "" Then
        @*@<a href="@Url.Action("ProductDetails", "Products", New With {.sku = row.SKU})">*@
            @<div Class="gallery-sku-overlay">@Html.Raw(row.description)</div>
        '</a>
    End If

</div>
Next
