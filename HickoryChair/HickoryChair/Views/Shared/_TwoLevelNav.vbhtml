﻿@ModelType Navigation

<div id="two-level-nav-wrapper">
    <div id="nav-left">
        @For Each link In Model.FirstLevelList
            If link.Clickable Then
                If link.IsLink Then
                    @<div class="nav-left-link"><a href="@link.Link" style="color:black;">@link.Title</a></div>
                ElseIf link.Level = "Second" Then
                    @<div class="nav-left-link indent" data-id="@link.ID">@link.Title</div>
                Else
                    @<div class="nav-left-link" data-id="@link.ID">@link.Title</div>
                End If
            Else
                @<div class="nav-left-header">@link.Title</div>
            End If
        Next
    </div>

    <div id="nav-right">
        <div id="nav-right-header">Living Room</div>
        @If Model.SecondLevelImgList IsNot Nothing Then
            @For Each link In Model.SecondLevelImgList
                Dim imgPath As String = "~/Images/layout/NavImages/" & link.Category & "/" & link.ID.ToString.Split(",")(0) & ".jpg"
                'Dim action As String = "/Products/ShowResults?" & link.Category & "=" & link.ID
                @<div class="nav-right-image-link">
    @If link.Category = "Type" Then
        @<a href="@Url.Action("ShowResults", "Products", New With {.TypeID = link.ID, .SearchName = link.Title})">
            <div><img src="@Url.Content(imgPath)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
            <div Class="nav-right-title">@link.Title</div>
        </a>
    ElseIf link.Category = "Subtype" Then
        @<a href="@Url.Action("ShowResults", "Products", New With {.SubTypeID = link.ID, .SearchName = link.Title})">
            <div><img src="@Url.Content(imgPath)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
            <div Class="nav-right-title">@link.Title</div>
        </a>
    ElseIf link.Category = "Room" Then
        @<a href="@Url.Action("ShowResults", "Products", New With {.RoomID = link.ID, .NewIntros = True, .SearchName = link.Title & " New Intros"})">
            <div><img src="@Url.Content(imgPath)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
            <div Class="nav-right-title">@link.Title</div>
        </a>
    ElseIf link.Category = "ProductLine" Then
        @<a href="@Url.Action("ShowResults", "Products", New With {.ProductLine = link.ID, .SearchName = link.Title})">
            <div><img src="@Url.Content(imgPath)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
            <div Class="nav-right-title">@link.Title</div>
        </a>
    ElseIf link.Category = "Designer" Then
        @<a href="@Url.Action("ShowDesigner", "Resources", New With {.DesignerID = link.ID})">
            <div><img src="@Url.Content(imgPath)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
            <div Class="nav-right-title">@link.Title</div>
        </a>
    ElseIf link.Category = "Link" Then
        @<a href="@Url.Action("CustomPrograms", "Products", New With {.program = link.ID})">
            <div><img src="@Url.Content(imgPath)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
            <div Class="nav-right-title">@link.Title</div>
        </a>
    End If

</div>
            Next
        ElseIf Model.SecondLevelList IsNot Nothing Then
            @<ul id="nav-right-link-list">
                @For Each link In Model.SecondLevelList
                    @<li>
                        @If link.Category = "Collection" Then
                            @<a href="@Url.Action("ShowResults", "Products", New With {.CollectionID = link.ID, .SearchName = link.Title})">@link.Title</a>
                        ElseIf link.Category = "ProductLine" Then
                            @<a href="@Url.Action("ShowResults", "Products", New With {.ProductLine = link.ID, .SearchName = link.Title})">@link.Title</a>
                        ElseIf link.Category = "Wildcard" Then
                            @<a href="@Url.Action("ShowResults", "Products", New With {.WildCard = link.ID, .SearchName = link.Title})">@link.Title</a>
                        ElseIf link.Category = "Link" Then
                            @<a href = "@Url.Content(link.ID)" >@link.Title</a>
                        ElseIf link.Category = "NewTab" Then
                            @<a href="@Url.Content(link.ID)" target="_blank">@link.Title</a>
                        ElseIf link.Category = "Subtype" Then
                            @<a href="@Url.Action("ShowResults", "Products", New With {.SubTypeID = link.ID, .SearchName = link.Title})">@link.Title</a>
                        End If
                    </li>
                Next
            </ul>
        End If

    </div>
</div>



