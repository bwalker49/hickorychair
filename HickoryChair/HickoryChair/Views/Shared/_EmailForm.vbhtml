﻿<div id="toEmail"></div>

<div id="email-form">
    <div>
        <label for="email">Your Email Address: <span>*</span></label>
        <input type="email" class="email" placeholder="EMAIL" />
    </div>

    @*<div>
        <label for="subject">Subject: <span>*</span></label>
        <input type="text" class="subject" />
    </div>*@

    <div>
        <label for="name">Name:</label>
        <input type="text" class="name" />
    </div>

    <div>
        <label for="phone">Phone Number:</label>
        <input type="text" class="phone" />
    </div>

    <div>
        <label for="message">Message:</label>
        <textarea rows="10" class="message"></textarea>
    </div>

    <div class="email-message">
        *
    </div>

    <div style="text-align:center;">       
        <input type="button" value="SUBMIT" class="submitEmail" data-email="@System.Web.Configuration.WebConfigurationManager.AppSettings("DefaultEmailAddress")"/>
    </div>
</div>

