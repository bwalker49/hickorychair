﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title</title>
    @Styles.Render("~/Content/bootstrap.min.css")
    @Styles.Render("~/Content/ConsumerSiteStyles")
    @Styles.Render("~/Content/HCFonts")
    @Scripts.Render("~/bundles/modernizr")
    @RenderSection("styles", required:=False)
</head>
<body>
    <div>
        <div id="nav-wrapper">
            <div id="header-social">
                <div><a href="http://www.facebook.com/hickorychairfurniture" target="_blank"><i class="fa fa-facebook"></i></a></div>
                <div><a href="https://www.instagram.com/hickorychair/" target="_blank"><i class="fa fa-instagram"></i></a></div>
                <div><a href="http://twitter.com/#!/hickorychairco" target="_blank"><i class="fa fa-twitter"></i></a></div>
                <div><a href="http://pinterest.com/hickorychairco/" target="_blank"><i class="fa fa-pinterest"></i></a></div>
                <div><a href="http://www.houzz.com/pro/hickorychair/the-hickory-chair-furniture-co" target="_blank"><i class="fa fa-houzz"></i></a></div>
            </div>

            <div id="header-links">
                <div><a href="@System.Web.Configuration.WebConfigurationManager.AppSettings("HomebaseLink")"><i class="fa fa-user"></i><span>Homebase</span></a></div>
                <div><a href="@Url.Action("Contact", "Misc")"><i class="fa fa-phone"></i><span>Contact Us</span></a></div>
                <div><a href="@Url.Action("Index", "WhereToBuy")"><i class="fa fa-map-marker"></i><span>Find A Dealer</span></a></div>
            </div>
        </div>

        <div id="nav-link-wrapper">
            <div id="header-logo"><a href="@Url.Action("Index", "Home")"><img src="~/Images/layout/logo-HCC_2013.jpg" /><div id="showroom-logo"></div></a></div>

            <ul>
                <li class="nav-dropdown" data-method="/Navigation/BuildTwoLevelNav" data-content="Products">Our Products</li>
                <li class="nav-dropdown" data-method="/Navigation/ShowCatalogs" data-content="Catalogs">Our Catalogs</li>
                <li class="nav-dropdown" data-method="/Navigation/BuildTwoLevelNav" data-content="Style">Our Collections</li>
                <li class="nav-dropdown" data-method="/Navigation/BuildTwoLevelNav" data-content="Resources">Design Resources</li>
                <li class="nav-dropdown" data-method="/Navigation/BuildTwoLevelNav" data-content="Story">Our Story</li>
                <li><a href="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=8435D83BD45FCA87E31726481376EAA9" target="_blank">Careers</a></li>
            </ul>

            <div id="nav-search-wrapper">
                <input id="nav-search-box" placeholder="Search Products..." />
                <span id="nav-search-btn"><i class="fa fa-search"></i></span>
            </div>
        </div>

        <div id="nav-dropdown" data-active=""></div>


        <div id="nav-link-wrapper-resp">
            <div id="menu-icon"><i class="fa fa-bars"></i></div>
            <div id="header-logo-resp"><a href="@Url.Action("Index", "Home")"><img src="~/Images/layout/logo-HCC_2013.jpg" /></a></div>
            <div id="search-resp"><i class="fa fa-search"></i></div>
        </div>

        <div id="nav-dropdown-res">
            <ul class="header-links">
                <li class="header-link" data-category="Products">Our Products</li>
                <li class="header-link" data-category="Catalogs">Our Catalogs</li>
                <li class="header-link" data-category="Style">Our Collections</li>
                <li class="header-link" data-category="Resources">Design Resources</li>
                <li class="header-link" data-category="Story">Our Story</li>
                <li class="header-link"><a href="@Url.Action("Index", "WhereToBuy")">Find A Dealer</a></li>
                <li class="header-link"><a href="@Url.Action("Contact", "Misc")">Contact Us</a></li>
                <li class="header-link"><a href="@System.Web.Configuration.WebConfigurationManager.AppSettings("HomebaseLink")">Homebase</a></li>
            </ul>
        </div>

        <div id="resp-search-wrapper">
            <input type="text" id="resp-search-box" placeholder="Search Products..." />
            <div>
                <span id="resp-search-btn">Search</span>
            </div>
        </div>


    </div>
    <div>
        @RenderBody()
        <footer>
            <div class="footer-column-wrapper">
                <div class="footer-column">
                    <div class="footer-column-title">
                        Our Products
                    </div>

                    <ul>
                        <li><a href="@Url.Action("ShowResults", "Products", New With {.RoomID = 2, .SearchName = "Living Room"})">Living Room</a></li>
                        <li><a href="@Url.Action("ShowResults", "Products", New With {.RoomID = 1, .SearchName = "Dining Room"})">Dining Room</a></li>
                        <li><a href="@Url.Action("ShowResults", "Products", New With {.RoomID = 3, .SearchName = "Bedroom"})">Bedroom</a></li>
                        <li><a href="@Url.Action("ShowResults", "Products", New With {.ProductLine = "HCC_SIL,M2MB", .SearchName = "Custom Seating"})">Custom Seating</a></li>
                        <li><a href="@Url.Action("ShowResults", "Products", New With {.ProductLine = "M2MC", .SearchName = "Custom Tables"})">Custom Tables</a></li>
                        <li><a href="@Url.Action("ShowResults", "Products", New With {.NewIntros = "True", .SearchName = "New Introductions"})">New Introductions</a></li>
                    </ul>
                </div>

                <div class="footer-column">
                    <div class="footer-column-title">
                        Our Catalogs
                    </div>

                    <div id="footer-catalogs">

                    </div>

                    @*<ul>
                            <li><a href="https://issuu.com/made.for.you/docs/2022_fine_upholstery_catalog_view_11-21?fr=sMjg3NjQyODAwMzQ" target="_blank">Upholstery</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/2022_fine_wood_catalog_view_11-21?fr=sYTNiMzQyODAwMzQ" target="_blank">Wood</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/2017_david_phoenix_catalog_10-21?fr=sNmYzYzQyODAwMzQ" target="_blank">David Phoenix</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/everett_by_skip_rumley_catalog-final?fr=sYjI4ZjQyODAwMzQ" target="_blank">Everett by Skip Rumley</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/susan_hable_catalog_view_11-21?fr=sZmNhODQyODAwMzQ" target="_blank">Hable</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/2018_midtown_collection_catalog_view_11-21?fr=sNzJmMjQyODAwMzQ" target="_blank">Midtown</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/ray_booth_catalog_view_11-21?fr=sMzc4MDQyODAwMzQ" target="_blank">Ray Booth</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/2018_suzanne_kasler_catalog_11_21?fr=sZThjYTQyODAwMzQ" target="_blank">Suzanne Kasler</a></li>
                            <li><a href="https://issuu.com/made.for.you/docs/2017_winterthur_collection_catalog_view_11-21?fr=sNjFjMTQyODAwMzQ" target="_blank">Winterthur</a></li>
                        </ul>*@
                </div>

                <div class="footer-column">
                    <div class="footer-column-title">
                        Design Resources
                    </div>

                    <ul>
                        <li><a href="/Misc/ArtistStudio">Artist's Studio</a></li>
                        <li><a href="/Resources/CustomersOwn">Customer's Own Hardware & Customer's Own Veneer</a></li>
                        <li><a href="/Resources/FabSearch">Fabrics</a></li>
                        <li><a href="/Resources/Finishes">Finishes</a></li>
                        <li><a href="/Resources/FabSearch?Type=Leather">Leather</a></li>
                        <li><a href="/Misc/Monogramming">Monogramming</a></li>
                        <li><a href="/Misc/NailTrim">Nailhead</a></li>
                        <li><a href="/Products/CustomPrograms/PERSPREF">Personal Preference Custom Dining Tables</a></li>
                        <li><a href="/Products/CustomPrograms/SILHOUETTES">Silhouettes</a></li>
                        <li><a href="/Documents/Resources/StockingHardware.pdf" target="_blank">Stocking Hardware</a></li>
                        <li><a href="/Misc/Striping">Striping Colors</a></li>
                        <li><a href="/Resources/FabSearch?Type=Trims">Trims</a></li>
                    </ul>
                </div>

                <div class="footer-column">
                    <div class="footer-column-title">
                        Our Story
                    </div>

                    <ul>
                        <li><a href="@Url.Action("ThenNow", "Misc")">Past & Present</a></li>
                        <li><a href="@Url.Action("CustomWoodworking", "Misc")">Craftsmanship</a></li>
                        <li><a href="@Url.Action("EnvironmentalResponsibility", "Misc")">Sustainability</a></li>
                        <li><a href="@Url.Action("MadeForYou", "Content")">Made For You™ Magazine</a></li>
                        <li><a href="@Url.Action("PressRelease", "Content")">Seasonal News & Press</a></li>
                    </ul>

                    <div class="footer-column-title">
                        Company
                    </div>

                    <ul>
                        <li><a href="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=8435D83BD45FCA87E31726481376EAA9" target="_blank">Careers</a></li>
                        <li><a href="@Url.Action("Contact", "Misc")">Contact Us</a></li>
                        <li><a href="/WhereToBuy">Find A Dealer</a></li>
                        <li><a href="@System.Web.Configuration.WebConfigurationManager.AppSettings("HomebaseLink")">Homebase</a></li>
                    </ul>
                </div>


                <div class="footer-column">
                    <div class="footer-column-title">Connect With Us</div>

                    <ul>
                        <li>
                            <div id="footer-social">
                                <div><a href="http://www.facebook.com/hickorychairfurniture" target="_blank"><i class="fa fa-facebook"></i></a></div>
                                <div><a href="https://www.instagram.com/hickorychair/" target="_blank"><i class="fa fa-instagram"></i></a></div>
                                <div><a href="http://twitter.com/#!/hickorychairco" target="_blank"><i class="fa fa-twitter"></i></a></div>
                                <div><a href="http://pinterest.com/hickorychairco/" target="_blank"><i class="fa fa-pinterest"></i></a></div>
                                <div><a href="http://www.houzz.com/pro/hickorychair/the-hickory-chair-furniture-co" target="_blank"><i class="fa fa-houzz"></i></a></div>
                            </div>
                        </li>
                        <li>
                            <div>Sign up for special offers and promotions from Hickory Chair!</div>
                            <div id="footer-email-wrapper"><input type="text" id="footer-email-signup" /><span>Sign Up!</span></div>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="footer-copyright-wrapper">
                <img src="~/Images/layout/txt-110yrs.gif" />
                <div>Hickory Chair, LLC &copy;@DateTime.Now.Year</div>
            </div>

            <hr />

            <div id="family-brands-wrapper">
                <div id="brand-header"><a href="http://www.rockhousefarm.com/"><img src="~/Images/layout/RHFLogos/Header.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.centuryfurniture.com" target="_blank"><img src="~/Images/layout/RHFLogos/Century.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.highlandhousefurniture.com" target="_blank"><img src="~/Images/layout/RHFLogos/HH.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.hancockandmoore.com" target="_blank"><img src="~/Images/layout/RHFLogos/HM.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.jessicacharles.com/" target="_blank"><img src="~/Images/layout/RHFLogos/JC.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.cabotwrenn.com" target="_blank"><img src="~/Images/layout/RHFLogos/CW.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.maitland-smith.com" target="_blank"><img src="~/Images/layout/RHFLogos/MS.png" /></a></div>
                <div class="brand-wrapper"><a href="http://www.pearsonco.com" target="_blank"><img src="~/Images/layout/RHFLogos/Pearson.png" /></a></div>
            </div>

            @*<p>&copy; @DateTime.Now.Year - My ASP.NET Application</p>*@

            @*<hr />*@


        </footer>
    </div>

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/jqueryui")
    @*@Scripts.Render("~/bundles/bootstrap")*@
    @Scripts.Render("~/bundles/ConsumerJS")
    @RenderSection("scripts", required:=False)  
    <script>
        function toTitleCase(str) {
            var lcStr = str.toLowerCase();
            return lcStr.replace(/(?:^|\s)\w/g, function (match) {
                return match.toUpperCase();
            });
        }

        function redirectToProductPage(sku) {
            $.ajax({
                type: "POST",
                url: '/Products/isSKU',
                data: "{\"sku\":\"" + $.trim(sku) + "\"}",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (data) {
                    if (data === 'Valid') {
                        window.location.replace("/Products/ProductDetails/" + sku)
                    } else {
                        window.location.replace("/Products/ShowResults/?WildCard=" + sku + "&SearchName=Searching For: " + toTitleCase(sku))
                    }
                }
            });
        }

        function loadCatalogs() {
            var url = '/Navigation/ShowCatalogs'

            $.post(url, function (data) {
                $('#footer-catalogs').html(data);
            })
        }

        $(document).ready(function () {
            if (localStorage.getItem('showroom') !== null && window.location.href.indexOf("/Dealer/HCShowroom") <= -1) {
                $('#showroom-logo').html(localStorage.getItem('showroom'));
            } else {
                if (window.location.href.indexOf("/Dealer/HCShowroom") > -1) {
                    $('#showroom-logo').html(window.location.pathname.split('/')[3].replace('_', ' '));
                    if (localStorage.getItem('showroom') !== window.location.pathname.split('/')[3].replace('_', ' ')) {
                        localStorage.setItem('showroom', window.location.pathname.split('/')[3].replace('_', ' '))
                    }                    
                }
            }

            loadCatalogs();

            $('#nav-search-box').on('keyup', function (e) {
                if (e.keyCode == 13) {
                    if ($(this).val() !== '') {
                        redirectToProductPage($(this).val())
                    }
                }
            })

            $('#nav-search-btn').on('click', function () {
                if ($('#nav-search-box').val() !== '' && $('#nav-search-box').is(':visible')) {
                    redirectToProductPage($('#nav-search-box').val());
                }
            })

            $('#search-resp').on('click', function () {
                if ($('#nav-dropdown-res').is(':visible')) {
                    $('#nav-dropdown-res').hide();
                    $('.first-level-links').remove();
                    $('.second-level-links').remove();
                }

                $('#resp-search-wrapper').slideToggle(function () {
                    $('#resp-search-btn').on('click', function () {
                        if ($('#resp-search-box').val() !== '') {
                            redirectToProductPage($('#resp-search-box').val());
                        }
                    })
                });
            })

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-32412099-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        })
    </script>
</body>
</html>
