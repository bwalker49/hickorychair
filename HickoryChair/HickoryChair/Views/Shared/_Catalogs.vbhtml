﻿@ModelType Navigation

<div class="first-level-links">
    @For Each c In Model.CatalogList
        @<div class="nav-catalog-wrapper">
            <a href="@c.FileName" target="_blank">
                <div><img src="@Url.Content(c.CoverImg)" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'" /></div>
                <div class="nav-catalog-name">@c.CatalogName</div>
            </a>
        </div>
    Next
</div>

