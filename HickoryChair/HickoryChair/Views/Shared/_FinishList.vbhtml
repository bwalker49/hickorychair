﻿@ModelType Models.Finishes

<div id="finishes">
    @Code
        Dim lastGroupID = ""
    End Code

    <div>
        <h3>Custom Finishes</h3>
        <div>Finish panels shown are only to give an approximate representation of the color of the finish but do not reflect the type or grain of wood used in the actual product. Please refer to the item image or your local retailer for that information. Finishes ordered at different times are not guaranteed to match. Each piece of Hickory Chair fine wood furniture is finished by hand, involving many steps. Woods have grain and color variations due to growth patterns of each tree. The result is a beautiful and unique piece. Due to these wood variations, finishes on individual pieces are not guaranteed to match. On this website, we strive to provide an accurate representation of our finishes; however, due to changes in lighting, monitors, limitations of photography, and actual wood used for product - color variation from the actual finish sample is possible. Not all finishes are available on every item.</div>
    </div>

    @For Each finish In Model.FinishList
        Dim img As String = "~/Content/finish-images/" & finish.FinishID & "_small.jpg"
        If finish.GroupID <> lastGroupID Then
            @<div class="group-title">@finish.GroupName</div>
        End If

        @<div class="finish-item">
            <a href="@Url.Action("FinishDetail", "Resources", New With {.finish = finish.FinishID})">
                <img src="@Url.Content(img)" />
                <div>@finish.FinishDesc</div>
            </a>

        </div>

        lastGroupID = finish.GroupID
    Next
</div>
