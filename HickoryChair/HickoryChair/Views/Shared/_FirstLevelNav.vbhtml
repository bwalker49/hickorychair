﻿@ModelType Navigation

<ul class="first-level-links">
    @For Each link In Model.FirstLevelList
        If link.Clickable Then
            If link.IsLink Then
                @<li class="nav-left-link"><a href="@link.Link" style="color:black;">@link.Title</a></li>
            ElseIf link.Level = "Second" Then
                @<li class="nav-left-link indent" data-id="@link.ID">@link.Title</li>
            Else
                @<li class="nav-left-link" data-id="@link.ID">@link.Title</li>
            End If
        Else
        @<li class="nav-left-header">@link.Title</li>
        End If
    Next
</ul>

