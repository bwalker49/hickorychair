﻿@ModelType Dealers

@If Model.dealerList.Count > 0 Then

    For Each dealer In Model.dealerList
        @<div Class="dealerResult" data-id="@Trim(dealer.dealerName),@Trim(dealer.dealerAddress1),@Trim(dealer.dealertCity), @Trim(dealer.dealerState)">    
            <span><strong>@dealer.dealerName</strong></span>
            <span>@dealer.dealertCity, @dealer.dealerState, @dealer.dealerZip</span>
            <span>PHONE: <a href="tel:@dealer.dealerPhone"> @dealer.dealerPhone</a></span>
        </div>
    Next
Else
    @<div Class="dealerResult active-dealer" data-id="Hickory Chair, 37 9th Street Place Southeast, Hickory, NC 28602">
        <span><strong>HICKORY CHAIR</strong></span>
        <span>37 9TH STREET PLACE SOUTHEAST, HICKORY, NC 28602</span>
        <span>PHONE: <a href="tel:8283241801">8283241801</a></span>
    </div>
    @<div style="text-align:center;font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;font-size:10pt;padding-top:15px;">Please select a location to see Hickory Chair dealers</div>
End If
