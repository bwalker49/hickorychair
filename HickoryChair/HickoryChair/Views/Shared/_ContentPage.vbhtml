﻿@ModelType Content


@section styles
    <link href="~/Content/contentpage.css" rel="stylesheet" />
End Section

    <div id="content-wrapper">
        <div id="content-image"><img src="@Url.Content(Model.CurrContentPage.PageImg)" /></div>
        <div id="content">
            <div id="content-title">
                @Html.Raw(Model.CurrContentPage.ContentTitle)
            </div>
            <div id="content-text">
                @Html.Raw(Model.CurrContentPage.ContentText)
            </div>
        </div>
    </div>
