﻿@ModelType Navigation

<ul class="second-level-links">
    @If Model.SecondLevelImgList IsNot Nothing Then
        For Each link In Model.SecondLevelImgList
            @<li class="second-level-link">                
                @If link.Category = "Type" Then
                    @<a href="@Url.Action("ShowResults", "Products", New With {.TypeID = link.ID, .SearchName = link.Title})">@link.Title</a>
                ElseIf link.Category = "Subtype" Then
                @<a href="@Url.Action("ShowResults", "Products", New With {.SubTypeID = link.ID, .SearchName = link.Title})">@link.Title</a>
                ElseIf link.Category = "Room" Then
                @<a href="@Url.Action("ShowResults", "Products", New With {.RoomID = link.ID, .NewIntros = True, .SearchName = link.Title & " New Intros"})">@link.Title</a>
                ElseIf link.Category = "ProductLine" Then
                @<a href="@Url.Action("ShowResults", "Products", New With {.ProductLine = link.ID, .SearchName = link.Title})">@link.Title</a>
                ElseIf link.Category = "Designer" Then
                @<a href="@Url.Action("ShowDesigner", "Resources", New With {.DesignerID = link.ID})">@link.Title</a>
                ElseIf link.Category = "Link" Then
                @<a href="@Url.Action("CustomPrograms", "Products", New With {.program = link.ID})">@link.Title</a>
                End If
             </li>
        Next
    ElseIf Model.SecondLevelList IsNot Nothing Then
            @For Each link In Model.SecondLevelList
                @<li class="second-level-link">
                    @If link.Category = "Collection" Then
                        @<a href="@Url.Action("ShowResults", "Products", New With {.CollectionID = link.ID, .SearchName = link.Title})">@link.Title</a>
                    ElseIf link.Category = "ProductLine" Then
                        @<a href="@Url.Action("ShowResults", "Products", New With {.ProductLine = link.ID, .SearchName = link.Title})">@link.Title</a>
                    ElseIf link.Category = "Wildcard" Then
                        @<a href="@Url.Action("ShowResults", "Products", New With {.WildCard = link.ID, .SearchName = link.Title})">@link.Title</a>
                    ElseIf link.Category = "Link" Then
                        @<a href="@Url.Content(link.ID)">@link.Title</a>
                    ElseIf link.Category = "NewTab" Then
                        @<a href="@Url.Content(link.ID)" target="_blank">@link.Title</a>
                    ElseIf link.Category = "Subtype" Then
                        @<a href="@Url.Action("ShowResults", "Products", New With {.SubTypeID = link.ID, .SearchName = link.Title})">@link.Title</a>
                    End If
                </li>
            Next
        End If
</ul>
