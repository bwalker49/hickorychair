﻿@ModelType Products

@Code
    ViewData("Title") = StrConv(Model.productDetail.name, VbStrConv.ProperCase)
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/productdetail.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="~/Content/jssocials.css"/>
    <link rel="stylesheet" href="~/Content/jssocials-theme-minima.css"/>
End Section

<div id="product-wrapper">
    <div id="product-left-section">
        <div id="product-image-gallery">
            @Html.Raw(HttpUtility.HtmlDecode(Model.productDetail.imgGallery))
        </div>
        <div id="color-note">
            Some color variation in photos of our furniture finishes and fabrics is possible due to the lighting and different color resolution on computer monitors.
        </div>
    </div>

    <div id="product-right-section">
        <div id="product-title">@Model.productDetail.name</div>
        <div id="product-collection">@Model.productDetail.collectionName</div>
        <div id="product-sku">@Model.productDetail.sku</div>
        <div id="product-dims">@Model.productDetail.dimensions <br />@Model.productDetail.dimensionsMetric</div>
        @If Model.productDetail.icons.Length > 0 Then
            @<div id="product-icons">
                @For Each icon In Model.productDetail.icons.Split("~")
                    Dim imgPath As String = "/Images/layout/ProductIcons/" & icon.Split("/")(0) & ".jpg"

                    @<div class="product-icon">
                        <img src="@Url.Content(imgPath)" width="50" title="@icon.Split("/")(1)" />
                    </div>
                Next
            </div>
        End If
        @*<div id="product-dims-metric"></div>*@
        <div id="accordion">
            <div class="accordion-section">
                <div class="accordion-title" data-id="description">
                    <span>Product Description</span><span><i data-id="description" class="fa fa-caret-down"></i></span>
                </div>
                <div class="accordion-content">
                    @If Model.productDetail.longDesc <> "" Then
                        @<div id="product-description">@Html.Raw(Model.productDetail.longDesc)</div>
                    End If

                    @If Model.productDetail.status <> "DR" Or Model.productDetail.Qty > 0 Then
                        If Model.productDetail.status = "DR" Or Model.productDetail.status = "DS" Then
                            If Model.productDetail.Qty > 0 Or Model.productDetail.ATPQty > 0 Or Model.productDetail.nextATPQty > 0 Then
                                @<br />
                                @<p style="color:red;font-weight:bold;">** Retired Item: Limited Quantities Available**</p>
                            End If
                        End If
                    End If

                    @*If itemStatus <> "DR" Or quantity > 0 Then
                    If itemStatus = "DR" Or itemStatus = "DS" Then
                    If quantity > 0 Or atpQty > 0 Or nextAtpQty > 0 Then
                    iNotes.InnerHtml &= "<p class='alert'>**Limited Quantities Available**</p>"
                    End If
                    End If

                    End If*@

                    @*@If 1 = 1 Then
                        @<div id="made-for-you"><a href="/Resources/OnlineDesignStudio?sku=@Model.productDetail.sku"><img src="~/Images/layout/ProductDetail/MadeForYou.png" /></a></div>
                    End If*@
                </div>
            </div>

            @If Model.documentList.Count > 0 Then
                @<div Class="accordion-section">
                    <div Class="accordion-title" data-id="documents">
                        <span> Documents</span><span><i data-id="documents" Class="fa fa-caret-down"></i></span>
                    </div>
                    <div Class="accordion-content">
                        @For Each doc In Model.documentList
                            @<div class="item-doc">
                                <a href="@doc.Link">
                                    <div><i class="fa fa-file"></i></div>
                                    <div>@doc.Title</div>
                                </a>
                            </div>
                        Next
                    </div>
                </div>
            End If


            <div Class="accordion-section">
                <div Class="accordion-title" data-id="dealer">
                    <span> Dealer Locator</span><span><i data-id="dealer" Class="fa fa-caret-down"></i></span>
                </div>
                <div Class="accordion-content">
                    @*<a href="@Url.Action("Index", "WhereToBuy", New With {.zip = "28601"})">Test</a>*@
                    <div style="margin-left:25px;">
                        <label for="ZipCode">City Or Zip/Postal:</label><br />
                        <input type="text" style="margin:10px 0;" id="ZipCode" /><br />
                        <input type="button" value="GO" id="submitDealerZip" />
                    </div>
                </div>
            </div>

            <div Class="accordion-section">
                <div Class="accordion-title" data-id="share">
                    <span> Print/Wishlist</span><span><i data-id="share" Class="fa fa-caret-down"></i></span>
                </div>
                <div Class="accordion-content">
                    <ul id="pew-list">
                        <li><a href="@Url.Action(" ShowResults", " Products", New With {.Wishlist = " True"})"><i class="fa fa-eye"></i>View My Wishlist</a></li>
                        @If Model.productDetail.inWishList.ToString = "False" Then
                            @<li id="wishlist" data-sku="@Model.productDetail.sku" data-user="@Profile.UserName"><i class="fa fa-star-o"></i>Add To Wish List</li>
                        Else
                            @<li id="wishlistDelete" data-sku="@Model.productDetail.sku" data-user="@Profile.UserName"><i class="fa fa-star"></i>Delete From Wish List</li>
                        End If
                        <li> <a href="@Url.Action("PrintPage", "Products", New With {.sku = Model.productDetail.sku})" target="_blank"><i Class="fa fa-print"></i>Print Page</a></li>
                        <li id="hiresdownload"><i class="fa fa-download"></i>Download Hi Res Image</li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="share" style="margin:25px 0;">

        </div>
    </div>
</div>

<div id = "product-resource-wrapper" >
    <div id="tabs">
        <ul>
            <li id="asShown">
                <a href="#asShownAs">As Shown</a>
            </li>

            @If Not Model.productDetail.aasItems Is Nothing Then
                @<li id="relatedproducts">
                <a href="#related">Related Products</a>
            </li>
            End If

            @If Model.productDetail.takesFabric Then
                @<li id="fabric">
                <a id="fabric_tab" href="#fabrics" onclick="getFabrics()" data-sku="@Model.productDetail.sku">Fabrics</a>
            </li>
            End If

            @If Model.productDetail.defaultFinish <> "" And Model.productDetail.defaultFinish <> "NA" Then
                @<li id="finish">
                        <a id="finish_tab" href="#finishes" onclick="getFinishes()" data-sku="@Model.productDetail.sku" data-collectionno="@Model.productDetail.collectionNo" data-collectiontype="@Model.productDetail.collectionType">Finishes</a>
                    </li>
            End If

            <li id="dimtab">
                        <a href="#dims">Dimensions</a>
                    </li>
        </ul>

        <div id="asShownAs">As Shown</div>

        @If Not Model.productDetail.aasItems Is Nothing Then
            @<div id="related">
                @For Each item In Model.productDetail.aasItems

                    Dim imgName As String = Html.Action("GetItemImage", "Products", New With {.sku = item.sku}).ToString


                    'If ItemName <> "Not Valid" Then
                    Dim img As String = "/prod-images/" & imgName & "_small.jpg"

                        @<a href="@Url.Action("ProductDetails", "Products", New With {.sku = item})" class="related-item">
                    <img src="@Url.Content(img)" width="150" height="150" alt="Image not found" onerror="this.src = '/Images/layout/no_image_small.jpg'"/>
                    <div class="related-item-sku">@item.sku</div>
                    <div class="related-item-name">@item.name</div>
                </a>
                    'End If

                Next
            </div>
        End If


    @If Model.productDetail.takesFabric Then
            @<div id="fabrics">Fabrics</div>
    End If

    @If Model.productDetail.defaultFinish <> "" And Model.productDetail.defaultFinish <> "NA" Then
        @<div id="finishes"></div>
    End If

                    <div id="dims">
@*<div>@Model.productDimensions.OutsideWidth</div>*@
                        <table>
                        <tr>
                            <td></td>
                            <td>Width</td>
                            <td>Depth</td>
                            <td>Height</td>
                        </tr>
    @If Model.productDimensions.OutsideWidth <> "NA" Or Model.productDimensions.OutsideDepth <> "NA" Or Model.productDimensions.OutsideHeight <> "NA" Then
            @<tr>
                                        <td>Outside</td>
                                        <td>@Model.productDimensions.OutsideWidth in</td>
                                        <td>@Model.productDimensions.OutsideDepth in</td>
                                        <td>@Model.productDimensions.OutsideHeight in</td>
                                    </tr>
            @<tr>
                                        <td></td>
                                        <td>@Model.productDimensions.OutsideWidthM cm</td>
                                        <td>@Model.productDimensions.OutsideDepthM cm</td>
                                        <td>@Model.productDimensions.OutsideHeightM cm</td>
                                    </tr>
    End If

    @If Model.productDimensions.InsideWidth <> 0 Or Model.productDimensions.InsideDepth <> 0 Or Model.productDimensions.InsideHeight <> 0 Then
        @<tr>
                                                    <td>Inside</td>
                                                    <td>@Model.productDimensions.InsideWidth in</td>
                                                    <td>@Model.productDimensions.InsideDepth in</td>
                                                    <td>@Model.productDimensions.InsideHeight in</td>
                                                </tr>
        @<tr>
                                                    <td></td>
                                                    <td>@Model.productDimensions.InsideWidthM cm</td>
                                                    <td>@Model.productDimensions.InsideDepthM cm</td>
                                                    <td>@Model.productDimensions.InsideHeightM cm</td>
                                                </tr>
    End If

    @If Model.productDimensions.SeatHeight <> 0 Then
        @<tr>
                                                        <td>Seat</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>@Model.productDimensions.SeatHeight in</td>
                                                    </tr>
        @<tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>@Model.productDimensions.SeatHeightM cm</td>
                                                    </tr>
    End If

    @If Model.productDimensions.ArmHeight <> 0 Then
        @<tr>
                                                            <td>Arm</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>@Model.productDimensions.ArmHeight in</td>
                                                        </tr>
        @<tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>@Model.productDimensions.ArmHeightM cm</td>
                                                        </tr>
    End If

                            <tr>
                                                            <td colspan="4" style="border-bottom:1px solid gainsboro;padding:10px;"></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="4" style="padding:10px 0;"></td>
                                                        </tr>

    @If Model.productDimensions.Volume > 0 Then
            @<tr>
                                                            <td>Volume</td>
                                                            <td>@Model.productDimensions.Volume cft</td>
                                                        </tr>
        End If

    @If Model.productDimensions.Weight > 0 Then
        @<tr>
                                                            <td>Weight</td>
                                                            <td>@Model.productDimensions.Weight lbs</td>
                                                        </tr>
    End If

    @If Model.productDimensions.Leather <> "" Then
        @<tr>
                                                                <td>Leather</td>
                                                                <td>@Model.productDimensions.Leather sq. ft.</td>
                                                            </tr>
    End If

    @If Model.productDimensions.COM > 0 Then
        @<tr>
                                                                <td>C.O.M *</td>
                                                                <td>@Model.productDimensions.COM yrd</td>
                                                            </tr>
        @<tr>
                                                                <td colspan="4" align="center" style="padding:10px 0;">* COM Yardage requirements may vary depending on fabric pattern and repeat.</td>
                                                            </tr>
        @<tr>
                                                                <td colspan="4" align="center" style="padding:10px 0;">* COM Yardage requirements based on plain 54in material.</td>
                                                            </tr>
    End If

                        </table>
                </div>
                </div>
                                        </div>

    @section scripts
        <script src = "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js" type="text/javascript"></script>
        <script src = "/Scripts/jssocials.js" ></script>
        <script src = "/Scripts/jssocials.shares.js" ></script>

                                        <script>
                                        function getFinishes() {
            var url = '/Resources/getFinishes',
                sku = $("#finish_tab").attr('data-sku');
            collectionno = $("#finish_tab").attr("data-collectionno");
            collectiontype = $("#finish_tab").attr("data-collectiontype");

            $.post(url, { sku: sku, collectionno: collectionno, collectiontype: collectiontype }, function(data) { $('#finishes').html(data) });

        }

        function getAsShown(img) {
            var url = '/Products/AsShown'

            $.post(url, { img: img }, function(data) { $('#asShownAs').html(data) })
        }

        function getFabrics() {
            var url = '/Products/GetSKUFabrics',
                sku = $('#fabric_tab').attr('data-sku');

            $.post(url, {sku: 'DNSF'}, function(data) { $('#fabrics').html(data) })
        }

        function addToWishlist(sku, user) {
            $.ajax({
                type: "POST",
                url: '/Products/addToWishlist',
                data: "{sku:'" + $.trim(sku) + "', user:'" + user + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (data) {
                    if (data === 'SUCCESS') {
                        alert('Successfully Added!')

                        location.reload();
                    } else {

                    }
                }
            });
        }

        function deleteFromWishlist(sku, user) {
            $.ajax({
                type: "POST",
                url: '/Products/deleteFromWishlist',
                data: "{sku:'" + $.trim(sku) + "', user:'" + user + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "html",
                success: function (data) {
                    if (data === 'SUCCESS') {
                        alert('Successfully Deleted!')

                        location.reload();
                    } else {

                    }
                }
            });
        }

        $(document).ready(function () {
            var currImg;

            setTimeout(function () {
                currImg = $('.slick-active').children().find('img').attr('src').split('/')[2].split('.')[0];
                getAsShown(currImg)
            }, 300)

            $('#product-main-slick-gallery').slick({
                //asNavFor: '#product-thumbnail-slick-gallery'
                arrows: false,
                infinite: false
            })

            $('#product-thumbnail-slick-gallery').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '#product-main-slick-gallery',
                focusOnSelect: true,
                infinite: false,
                responsive: [{
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }]
            })

            $('#product-main-slick-gallery').on('afterChange', function (event, slick, currentSlide, nextSlide) {
                currImg = $('.slick-active').children().find('img').attr('src').split('/')[2].split('.')[0]
                getAsShown(currImg);           
            })

            $('#hiresdownload').on('click', function () {
                window.location = '/Products/DownloadHiResImage?ImageName=' + currImg
            })

            $('.accordion-title').on('click', function () {
                var $caret = $(this).find('.fa[data-id=' + $(this).data('id') + ']');

                $(this).next('.accordion-content').slideToggle(function () {
                    if ($caret.hasClass('fa-caret-down')) {
                        $caret.removeClass('fa-caret-down').addClass('fa-caret-up');
                    } else {
                        $caret.removeClass('fa-caret-up').addClass('fa-caret-down');
                    }
                });
            });

            $('.accordion-title[data-id="description"]').click();

            $('#wishlist').on('click', function () {
                addToWishlist($(this).data('sku'), $(this).data('user'))
            })

            $('#wishlistDelete').on('click', function () {
                deleteFromWishlist($(this).data('sku'), $(this).data('user'))
            })

            $('#submitDealerZip').on('click', function () {
                window.location.href = '/WhereToBuy?zip=' + $('#ZipCode').val();
            })

            $('#share').jsSocials({
                shares: ["email", "twitter", "facebook", "pinterest"]
            })

            $('#tabs').tabs();
        })

        </script>
    End Section

