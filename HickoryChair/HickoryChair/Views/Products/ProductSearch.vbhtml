﻿@ModelType Products

@Code
    ViewData("Title") = Request("SearchName")
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/searchresults.css" rel="stylesheet" />
End Section

@If Model.resultList.Count > 0 Then
    @If Model.resultList.First.collectionImg <> "" And Model.resultList.First.collectionText <> "" Then
        @<div id="results-banner">
            <div id="banner-img"><img src="@Url.Content(Model.resultList.First.collectionImg)" /></div>
            <div id="banner-text">
                <div id="banner-coll-name">@Request("SearchName")</div>
                <div id="banner-desc">@Html.Raw(Model.resultList.First.collectionText)</div>
            </div>
        </div>
    Else
        @<div id="search-wrapper-title">
            @Request("SearchName")
        </div>
    End If
Else
    @<div id="search-wrapper-title">
        @Request("SearchName")
    </div>
End If


<div id="search-result-wrapper">
    @If Model.resultList.Count > 0 Then
        @For Each product In Model.resultList
            @<div class="search-item">
                <a href="@Url.Action("ProductDetails", "Products", New With {.sku = product.sku})">
                    <div><img src="@Url.Content(product.imgURL)" /></div>
                    <div class="search-item-sku">@product.sku</div>
                    <div class="search-item-name">@product.name</div>
                </a>
            </div>
        Next
    Else
        @<div style="width:100%;text-align:center;font-size:18pt;font-weight:bold;margin-top:25px;">No Items Available</div>
    End If


</div>

