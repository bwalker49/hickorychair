﻿@ModelType CustomProgram

@Code
    ViewData("Title") = HttpContext.Current.Request.Url.AbsolutePath.Split("/")(3)
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/customprogram.css" rel="stylesheet" />

        <style>
            #finishes {
                width: 95%;
                margin: 25px auto;
                font-family: 'Verlag-Light';
            }

            .group-title {
                margin: 15px 0;
                /*font-weight:bold;*/
                font-size: 15pt;
                border-bottom: 1px solid gainsboro;
            }

            .finish-item {
                text-align: center;
                width: 19.75%;
                display: inline-block;
                margin: 10px 0;
            }

            a, a:hover {
                color: black;
            }
        </style>
End Section

<div id="custom-program-wrapper">

</div>

@section scripts
    <script>
        function getFinishes() {            
            var url = '/Resources/FinishPartial';       

            $.post(url, function (data) { $('#FINISH').html(data); });

        }

        $(document).ready(function () {
            var url = '/Products/BuildCustomProgram',
                program = window.location.pathname.substring(window.location.pathname.lastIndexOf('/') + 1, window.location.pathname.length);

            $.post(url, { program: program }, function (data) { $('#custom-program-wrapper').html(data); $('#tabs').tabs({ create: function () { getFinishes(); }}); });
        })
    </script>
End Section