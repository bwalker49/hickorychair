﻿@ModelType Products

@Code
    Layout = Nothing
    If Model.productDetail IsNot Nothing Then
        ViewData("Title") = "Print " & Model.productDetail.name
    Else
        ViewData("Title") = "Print Wishlist Items"
    End If

End Code

<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width" />
    <title>@ViewBag.Title</title>
   
    <link href="~/Content/fonts.css" rel="stylesheet" />
    <link href="~/Content/printproduct.css" rel="stylesheet" />
</head>
<body>
    @If Model.resultList IsNot Nothing Then
        @<div id="content">
            <div id="logo-wrapper">
                <div><img src="/Images/layout/logo-HCC_2013.jpg" /></div>   
                <p id="wl-text-print">Wishlist Items</p>
            </div>
            @For each item In Model.resultList
                @<div class="print-product-wrapper">
                <div class="img-wrapper"><img src="@item.hiresimgURL" /></div>
                <div class="name-wrapper">@item.sku @item.name</div>
                <div id="product-collection">@Model.productDetail.collectionName</div>
                <div id="product-sku">@Model.productDetail.sku</div>
                <div class="dim-wrapper>">@item.dimensions</div>
                <div class="desc-wrapper">@item.longDesc</div>
                @*<div class="qty-wrapper"> Current Qty: <strong>@item.currQty</strong></div>*@
            </div>                
            Next
        </div>

    Else
        @<div id="logo-wrapper">
            <div><img src="/Images/layout/logo-HCC_2013.jpg" /></div>
        </div>
        @<div id="content">
             <div style="display:inline-flex;width:50%;flex-direction:column;justify-content:center;align-items:center;">
                 <div class="img-wrapper"><img src="@Model.productDetail.imgURL" /></div>
             </div>
             <div style="display:inline-flex;width:40%;flex-direction:column;margin-left:10%;text-align:left;margin-top:5vh;">
                 <div class="name-wrapper">@Model.productDetail.name</div>
                 <div id="product-collection">@Model.productDetail.collectionName</div>
                 <div id="product-sku">@Model.productDetail.sku</div>
                 <div class="dim-wrapper">@Model.productDetail.dimensions</div>
                 <div class="desc-wrapper">@Model.productDetail.longDesc</div>
                 @*<div class="qty-wrapper"> Current Qty: <strong>@Model.productDetail.currQty</strong></div>*@
             </div>
            
        </div>
    End If
</body>
</html>
