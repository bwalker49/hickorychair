﻿@ModelType Designer

@Code
    ViewData("Title") = Model.DesignerDetails.DesignerName
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/designers.css" rel="stylesheet" />
End Section

<div>
    @Code
        Dim imgPath As String = "~/Images/layout/Designers/" & Model.DesignerDetails.DesignerID & ".jpg"
    End Code
    <div id="designer-wrapper">
        <div id="designer-img-wrapper">
            <img src="@Url.Content(imgPath)" />
        </div>
        <div id="designer-content-wrapper">
            <div id="designer-name">@Model.DesignerDetails.DesignerName</div>
            <div>@Html.Raw(Model.DesignerDetails.DesignerBio)</div>
            <div class="link-wrapper"><a href="@Url.Action("ShowResults", "Products", New With {.DesignerID = Model.DesignerDetails.DesignerID, .SearchName = Model.DesignerDetails.DesignerName})">View Collection</a></div>
            @If Model.DesignerDetails.Catalog <> "" Then
                @<div class="link-wrapper"><a href="@Model.DesignerDetails.Catalog" target="_blank">View Catalog</a></div>
            End If
            @If Model.DesignerDetails.Brandbook <> "" Then
                @<div class="link-wrapper"><a href="@Model.DesignerDetails.Brandbook" target="_blank">View Brandbook</a></div>
            End If
        </div>
    </div>
</div>

