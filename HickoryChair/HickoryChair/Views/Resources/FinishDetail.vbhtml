﻿@ModelType FinishDetail

@Code
    ViewData("Title") = "FinishDetail"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@Section Styles
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="/Content/slick.css">
    <link rel="stylesheet" href="/Content/slick-theme.css">
    <style>
        .custom-bold {
            font-weight: 600;
        }

        .similar-finish {
            width: 32%;
            text-align: center;
            display: inline-block;
        }

        .extra-notes {
            margin: 0.5em 0em;
            font-size: small;
        }
    </style>
End Section

<div class="container-fluid">
    <div class="row mt-3 justify-content-center">
        <div class="col-md-6 col-lg-5 col-xl-4">
            <div class="text-center"><img class="img-fluid" src=@("/Content/finish-images/" + Model.Finish.ID + ".jpg") style="border-radius: 2em;" /></div>
            <div class="text-center mb-2">
                <a href=@("/Content/finish-images/" + Model.Finish.ID + "_hires.jpg") class="text-center" style="text-decoration: none; color: black;">View Hi-Res Image</a>
            </div>
            @If Model.Finish.Note <> "" Then
                @<div class="text-center" style="font-size: small;"><span Class="custom-bold">NOTE: </span>@Model.Finish.Note</div>
            End If
        </div>

        <div class="col-md-6 col-lg-5 col-xl-4">
            <div class="mb-3" style="font-size: x-large;">@Model.Finish.Group</div>
            <div class="mb-3 custom-bold" style="font-size: large;">@Model.Finish.Name</div>
            @If Model.Finish.ID = "HC10" Then
                @<!-- Extra Notes for Blonde -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Blonde is available ONLY on Ash and does not translate to other species. No match panel strike offs for other species will be honored.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC74" Then
                @<!-- Extra Notes for Ebony -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Ebony is a dark charcoal/brown finish that is wire-brushed to open up the grain structure in order to allow a white pumice over-rub to hang up in the open
                        grain. Ebony finish is not distressed. It is lightly wire brushed.
                    </div>
                    <div class="extra-notes">For heavier pumice hang-up, you may specify Ebony with Heavy Wire Brush technique for a 5% up-charge.</div>
                    <div class="extra-notes">The degree of hang-up will vary by species; between solids versus veneers and within each individual piece.</div>
                    <div class="extra-notes">Wide latitude is to be expected on Ebony and will not be considered a reason for return.</div>
                    <div class="extra-notes">Recommended for use on wood species such as Ash and Oak ONLY that provide a suitable open grain structure for the pumice over-rub to hang up in.</div>
                    <div class="extra-notes">Results on mahogany, primavera, walnut and other species will vary. Not available on beech, cherry or maple.</div>
                    <div class="extra-notes">
                        Not available on items crafted in beech, cherry or maple as their grain structure is too dense for the pumice over-rub to hang up in. No match panel strike
                        offs for other species will be honored.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC361" Then
                @<!-- Extra Notes for Hable Grey -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Hable Grey is a greenish-grey stain that allows the natural maple grain to be evident through the finish.
                    </div>
                    <div class="extra-notes"> Hable Grey has a low sheen and is standard with no distressing.</div>
                    <div class="extra-notes">
                        It was developed for items made in “vanilla bean ice cream” maple veneer with maple solids. Hable Grey is ONLY available on maple. No match panel
                        strike offs will be honored.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC341" Then
                @<!-- Extra Notes for Mocha -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Mocha is a light brown stain with a low sheen and no distressing.
                    </div>
                    <div class="extra-notes">
                        Mocha was developed for items made in maple. It is available on light species of wood including ash, maple, beech and beeswing primavera. Mocha is not
                        available on items made in dark species of wood including mahogany, walnut, cherry or santos rosewood. You may request a match panel strike off for all
                        other species.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC328" Then
                @<!-- Extra Notes for River Rock -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        River Rock is a light brown stain with a low sheen and light distressing.
                    </div>
                    <div class="extra-notes">
                        River Rock was developed for items made in walnut. You may request a match panel strike off for all other species.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC331" Then
                @<!-- Extra Notes for Saddle -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Saddle is a light brown stain with a low sheen and light distressing.
                    </div>
                    <div class="extra-notes">
                        Saddle was developed for items made in walnut. You may request a match panel strike off for all other species.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC135" Then
                @<!-- Extra Notes for Artisan Birch -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Executed with a technique to make them look older, more irregular and worn.
                    </div>
                    <div class="extra-notes">
                        An emulsion coat is applied under the paint, which gives these finishes a somewhat textured, irregular surface.
                    </div>
                    <div class="extra-notes">
                        Gold paint is hand applied in an item specific pattern that gives the appearance of gold trim that has been worn off. It has a much more worn,
                        intermittent pattern than our Antique Rub striping.
                    </div>
                    <div class="extra-notes">
                        Surfaces with carvings, moldings or raised areas lend themselves naturally to the gold highlights.
                    </div>
                    <div class="extra-notes">
                        Unless specific written instructions and a sketch accompany your order, we will apply the gold highlighting at our discretion.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC562" Then
                @<!-- Extra Notes for Heritage White -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        White paint over a heavy emulsion layer which is then glazed over.
                    </div>
                    <div class="extra-notes">
                        Heritage White is highlighted with a beige contrast banding on applicable areas.
                    </div>
                    <div class="extra-notes">
                        Intended to look old and somewhat “gloppy” and may have some areas that are worn through.
                    </div>
                    <div class="extra-notes">
                        Unless specific written instructions and a sketch accompany your order, we will apply the beige banding at our discretion.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC615" Then
                @<!-- Extra Notes for Patina -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        Silver leaf finish with an amber over-glaze.
                    </div>
                    <div class="extra-notes">
                        Warm, aged metallic finish that’s neither silver nor gold.
                    </div>
                    <div class="extra-notes">
                        Light spatter pattern is applied and the outlines of leaf sheets will be visible.
                    </div>
                    <div class="extra-notes">
                        Scratch pattern may also be visible in areas.
                    </div>
                </div>
            End If
            @If Model.Finish.ID = "HC173" Then
                @<!-- Extra Notes for Venetian Blue -->
                @<div class="mb-3">
                    <div class="extra-notes">
                        A blue paint that is glazed over to mute and antique the color.
                    </div>
                    <div class="extra-notes">
                        Intended to look layered with remnants of prior gold finish appearing in random worn-through areas.
                    </div>
                    <div class="extra-notes">
                        Heavy physical distressing.
                    </div>
                </div>
            End If
            <div class="mb-3">@Model.Finish.Status</div>

            @*@If Model.Finish.AvailableOnlineToOrder Then
                @<div Class="mb-1">
                    <Button Class="btn btn-warning addfabcart" data-sku="@Model.Finish.ID" data-type="finish" data-description="@Model.Finish.Name">Add to Cart</Button>
                </div>
                @<div Class="mb-4" style="font-size: small;">* To order a finish chip on an alternate wood species, contact your representative.</div>
            End If*@

            @If Model.Finish.HandRubbingAvailable Or Model.Finish.LeafingBrushingAvailable Or Model.Finish.Sheen <> "" Or Model.Finish.LongDescription <> "" Then
                @<div Class="pb-1 mb-2 custom-bold" style="border-bottom: 1px solid lightgray;">Finish Characteristics</div>
                @If Model.Finish.LongDescription = "Distressing Level: None" Or Model.Finish.LongDescription = "Distressing Level: Light" Or Model.Finish.LongDescription = "Distressing Level: Medium" Or Model.Finish.LongDescription = "Distressing Level: Heavy" Then
                    @<!-- Distressing Level -->
                    @<div class="row mb-4">
                        <div Class="col-3 mb-2">DISTRESS</div>
                        @If Model.Finish.LongDescription = "Distressing Level: None" Then
                            @<div Class="col-5 mb-2">None</div>
                        End If
                        @If Model.Finish.LongDescription = "Distressing Level: Light" Then
                            @<div Class="col-6 mb-2">Light</div>
                        End If
                        @If Model.Finish.LongDescription = "Distressing Level: Medium" Then
                            @<div Class="col-6 mb-2">Medium</div>
                        End If
                        @If Model.Finish.LongDescription = "Distressing Level: Heavy" Then
                            @<div Class="col-6 mb-2">Heavy</div>
                        End If
                        @*<div class="col-3 mb-2" style="text-align: right;">
                                <a data-bs-toggle="collapse" href="#distressing-level"><i class="fa fa-chevron-down" style="color: black;"></i></a>
                            </div>
                            <div id="distressing-level" class="col-12 collapse show" style="line-height: normal; font-size: small;">
                                <span class="custom-bold">Distressing Level</span> - *description...*
                            </div>*@
                    </div>
                End If
                @If Model.Finish.HandRubbingAvailable Then
                    @<!-- Hand Waxing -->
                    @<div class="row mb-4">
                        <div class="col-3 mb-2">
                            <i class="fa fa-hand-paper-o"></i>
                        </div>
                        <div class="col-5 mb-2">Hand Waxing</div>
                        <div class="col-3 mb-2" style="text-align: right;">
                            <a data-bs-toggle="collapse" href="#hand-waxing"><i class="fa fa-chevron-down" style="color: black;"></i></a>
                        </div>
                        <div id="hand-waxing" class="col-12 collapse show" style="line-height: normal; font-size: small;">
                            <span class="custom-bold">Hand Waxing</span> - brings out the luster in fine wood finishes. A skilled artisan applies a special formulation of wax and rubs it to a high sheen. The formula, which contains beeswax and carnauba, produces a long-lasting shine which brings out the depth of complex multi-step finishes.
                        </div>
                    </div>
                End If
                @If Model.Finish.LeafingBrushingAvailable Then
                    @<!-- Dry Brushing/Leafing -->
                    @<div Class="row mb-4">
                        <div Class="col-3 mb-2">
                            <i Class="fa fa-paint-brush"></i>
                        </div>
                        <div Class="col-5 mb-2">Dry Brushing/Leafing</div>
                        <div Class="col-3 mb-2" style="text-align: right;">
                            <a data-bs-toggle="collapse" href="#dry-brushing-leafing"><i Class="fa fa-chevron-down" style="color: black;"></i></a>
                        </div>
                        <div id="dry-brushing-leafing" Class="col-12 collapse show" style="line-height: normal; font-size: small;">
                            <div Class="mb-1"><span class="custom-bold">Dry Brushing</span> - A gold or silver paint is made by combining a mixture of gold or silver powder to a clear glaze or gloss lacquer. The brush is dipped in the mixture, then is brushed on an absorbent surface until it is nearly dry. Only then is it applied to furniture. As a result, it is more subtle than leafing.</div>
                            <div> <span Class="custom-bold">Leafing</span> - Thin foil sheets of silver or gold leaf are applied by hand to accent design details such as carvings and mouldings. They are worn through and burnished to a fine patina to simulate aging.</div>
                        </div>
                    </div>
                End If
                @If Model.Finish.Sheen <> "" Then
                    @<!-- Sheen -->
                    @<div Class="row mb-4">
                        <div Class="col-3 mb-2">SHEEN</div>
                        <div Class="col-5 mb-2">@Model.Finish.Sheen</div>
                        <div Class="col-3 mb-2" style="text-align: right;">
                            <a data-bs-toggle="collapse" href="#sheen"><i Class="fa fa-chevron-down" style="color: black;"></i></a>
                        </div>
                        <div id="sheen" Class="col-12 collapse show" style="line-height: normal; font-size: small;">
                            <div Class="mb-1"><span class="custom-bold">Sheen Level</span> - refers to the degree of 'shine' that appears on a piece of furniture. Each finish has a pre-determine standard sheen level. The varying degrees of sheen are Low (5,10), Medium (20,30), and High (50,70). For some finishes the top of an item is rubbed by hand or machine to reach an even higher degree of shine.</div>
                            @If Model.Finish.Sheen = "High" Then
                                @<div Class="custom-bold">NOTE: A high sheen finish is not recommended on heavily distressed items.</div>
                            End If
                        </div>
                    </div>
                End If
            End If

            @If Model.SimilarFinishes.Count > 0 Then
                @<div Class="pb-1 mb-3 mt-5 custom-bold" style="border-bottom: 1px solid lightgray;">Similar Finishes</div>
                @<div style="max-height: 210px; overflow: hidden; outline: none;">
                    <!-- Similar Finishes -->
                    @For Each similarFinish In Model.SimilarFinishes
                        @<div Class="similar-finish">
                            <a href="#" style="text-decoration: none; color: black;">
                                <img src=@("~/Content/finish-images/" + similarFinish.ID + "_small.jpg") style="border-radius: .5em;" />
                                <p>@similarFinish.Name</p>
                            </a>
                        </div>
                    Next
                </div>
            End If
        </div>

        @If Model.ItemsFinishedIn.Count > 0 Then
            @<div Class="col-12 text-center mt-4 pt-2" style="border-top: 1px solid lightgray;">
                <div class="row justify-content-center">
                    <div style="font-size: large;">Items Finished In <span class="custom-bold">@Model.Finish.Name</span></div>
                    <div id="gallery" style="width: 90%;">
                        @For Each img In Model.ItemsFinishedIn
                            @Code
                                ' Split img string to get only SKU to send to /Products/ShowItem
                                Dim imgString As String = img.Split("~")(1)
                                'Dim imgStringArray() As String = imgString.Split("_"c)
                                Dim sku As String = img.Split("~")(0)
                            End Code
                            @<div><a href=@("/Products/ProductDetails/" + sku) target="_blank"><img src=@("/Content/prod-images/" + imgString + "_small.jpg") class="mx-auto d-block" /></a></div>
                                Next
                        @*<div><img src="https://www.centuryfurniture.com/prod-images/LR_18230_0908_small.jpg" /></div>
                            <div><img src="https://www.centuryfurniture.com/prod-images/lr_18246_fm13_small.jpg" /></div>
                            <div><img src="https://www.centuryfurniture.com/prod-images/lr_7600_13_ps13_small.jpg" /></div>
                            <div><img src="https://www.centuryfurniture.com/prod-images/369_301_FM10_fpo_small.jpg" /></div>*@
                    </div>
                </div>
            </div>
        End If
    </div>
</div>

@Section Scripts
    <!-- Slick Carousel -->
    <script src="~/Scripts/slick.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#gallery').slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 4,
                autoplay: true,
                autoplaySpeed: 2500,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            centerMode: false
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            centerMode: false
                        }
                    }
                ]
            });
        });
    </script>
End Section

