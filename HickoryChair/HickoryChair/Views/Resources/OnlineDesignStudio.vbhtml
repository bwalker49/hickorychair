﻿@Code
    ViewData("Title") = "Hickory Chair Upholstery Design Studio"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <style>
        iframe {
            width:100%;
            margin:0 auto;
        }
    </style>
End Section

<iframe id="ifrmMain" width="1000" height="1100" scrolling="no" src="https://microd.hickorychair.com/ItemInformation.aspx?catalog=768&action=selectcover&sku=@Request("SKU")"></iframe>

