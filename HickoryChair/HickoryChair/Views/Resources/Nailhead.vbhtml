﻿@ModelType Models.NailTrim
@Code
    ViewData("Title") = "Nail Trim List"
End Code

@section styles
    <style>
        .bg-info {
            background-color: #EDEDED!important;
        }        
    </style>
End Section

<div class="row py-3">
    <div class="col-12 text-center"><h4>Nail Trim List</h4></div>
    @Code
        Dim lastPriceGroup As String = ""
    End Code
    @For each nI In Model.NailTrimList
        @Code
            Dim imgSize As Double = nI.ImgSize.Split("/")(0) / nI.ImgSize.Split("/")(1)
            Dim css As String = "style=zoom:" & imgSize & ";position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);"
            Dim imgRatio As Integer = 100 * imgSize
            Dim imgWrapper As String = "style=height:125px;position:relative;"
        End Code

        @If lastPriceGroup <> nI.NailGroup Then
            @<h5 Class="col-12 bg-info text-center">@nI.NailGroup</h5>
        End If
        @<div class="col-xs-12 col-sm-6 col-md-3 text-center py-3"> 
             <div @imgWrapper>
                 <img src="~/Content/nail-trim-images/@(nI.NailID).jpg" @css />
             </div>            
            <div>@nI.NailDesc</div>                       
        </div>
        @Code
            lastPriceGroup = nI.NailGroup
        End Code
    Next
</div>

