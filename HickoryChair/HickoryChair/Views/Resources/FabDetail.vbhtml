﻿@ModelType Models.FabricDetail

@Code
    ViewData("Title") = "FabDetail"
End Code

@section styles
    <style>
        .row {
            margin: 50px 0;
            font-family: 'Verlag-Light';
        }

        .grade-note {
            color:red;
            font-size:12pt;
        }

        /*.img-fluid {
            max-width:75%;
        }*/
    </style>
End Section

    @If Not IsNothing(Model.FDetail.SKU) Then
        @<div Class="row">

            @*<h2 Class="text-center">Fabric Detail</h2>*@

            <div Class="col-lg-6 text-center">
                <img src="@Url.Content(Model.FDetail.FabImage)" alt="Fabric Image" data-route='/Webfolio/imgPopup' class="img-fluid img-modal" data-id="@Url.Content(Model.FDetail.FabImageHiRes)" />

                <div class="panel panel-default" style="margin-top:20px;">
                    <div class="panel-heading">Disclaimer:</div>
                    <div class="panel-body">
                        We have made every effort to display the colors of products appearing on the web site as accurately as possible.
                        Due to the differences in monitor displays, we cannot guarantee that the colors displayed will be 100% accurate.
                        We recommend obtaining fabric swatches before making a selection.
                    </div>
                </div>

            </div>
            <div class="col-lg-5 offset-lg-1 text-left">
                <div class="col-lg-11">
                    <h2>@Model.FDetail.SKU - @Model.FDetail.Name</h2>
                    <h4>Status: @Model.FDetail.Status</h4>
                    <div>Grade: @Model.FDetail.Grade</div>
                    @If Model.FDetail.NewGrade <> "" And Model.FDetail.NewGradeDate <> "" And Model.FDetail.NewGradeDate > Today Then
                        @<div class="grade-note">Grade change to @Model.FDetail.NewGrade on @Model.FDetail.NewGradeDate</div>
                    End If
                    <div>Color : @Model.FDetail.ColorGroup - @Model.FDetail.ColorGroupDescription</div>
                    <div>@Model.FDetail.Durability</div>
                    @If Model.FDetail.DoubleRubs > 0 Then
                        @<div>Double Rubs: @Model.FDetail.DoubleRubs</div>
                    End If
                    @If Model.FDetail.WidthOfRoll > 0 Then
                        @<div>Width Of Roll: @Model.FDetail.WidthOfRoll inches</div>
                    End If
                    <div>Repeat H:  @Model.FDetail.HorizontalRepeat  V:  @Model.FDetail.VerticalRepeat</div>
                    <div>Fabric Div: @Model.FDetail.FabricDivision</div>
                    <div>Orientation: @Model.FDetail.Orientation</div>

                    @If Model.FDetail.FabricRestrictions <> "" Then
                        @<div>Restrictions: @Model.FDetail.FabricRestrictions</div>
                    End If

                    @if Model.FDetail.FabricFinish <> "" Then
                        @<div>Fabric Finish: @Model.FDetail.FabricFinish - @Model.FDetail.FabricFinishDesc</div>
                    End If
                    @if Model.FDetail.FringeHeight <> "" Then
                        @<div>Fringe Height: @Model.FDetail.FringeHeight</div>
                    End If
                    @*<div>
            @if Model.FDetail.FabClass = "LTHR" Then
                @<div>Cleaning Instructions: <a href='/Content/PDF/CareAndMaintenance/Leather.pdf'><img src='~/Content/Images/RHF/FabSearch/img-pdf-icon.png' width='24' /></a></div>
            Else
                @<div>Cleaning Instructions: <a href='/Content/PDF/CareAndMaintenance/Upholstery.pdf'><img src='~/Content/Images/RHF/FabSearch/img-pdf-icon.png' width='24' /></a></div>
            End If
            <div>Cleaning Code: @Model.FDetail.CleaningCode</div>
        </div>*@
                    @If Model.FDetail.PerformanceCode <> "" Then
                        @<div>Performance Code: @Model.FDetail.PerformanceCode</div>
                    End If
                    @If Model.FDetail.DesignerCode <> "" Then
                        @<div>Designer: @Model.FDetail.DesignerCode - @Model.FDetail.DesignerDesc</div>
                    End If
                    @If Model.FDetail.PerformanceDescription <> "" Then
                        @<div>Performance Description: @Model.FDetail.PerformanceDescription</div>
                    End If
                    @If Model.FDetail.FabContent.Count > 0 Then
                        @<div style="margin-top:10px;">
                            <Table Class="table table-responsive table-condensed">
                                <tr>
                                    <th>Fiber</th>
                                    <th>Percent</th>
                                    <th>Description</th>
                                </tr>
                                @for Each fp In Model.FDetail.FabContent
                                    @<tr>
                                        <td>@fp.Fiber</td>
                                        <td>@fp.FiberPercent</td>
                                        <td>@fp.FiberDescription</td>
                                    </tr>
                                Next

                            </Table>
                        </div>
                    End if

                    @*@Code
            Html.RenderPartial("~/Views/Products/_FabCartDetail.vbhtml", New Models.FabCart(Model.FDetail.SKU))
        End code*@




                </div>

                <div Class="col-lg-6">


                    @If User.IsInRole("Wholesale") Or User.IsInRole("Retail") Then
                        @<div Class="card my-3">
                            <div Class="card-header">Pricing</div>
                            <div Class="card-body">
                                <table class="table table-responsive">
                                    @If User.IsInRole("Wholesale") Then
                                        @<tr>
                                            <td>Wholesale</td>
                                            <td>@FormatCurrency(Model.FDetail.Wholesale) per @Model.FDetail.UOM</td>
                                        </tr>
                                    End If
                                    @if User.IsInRole("Retail") Then
                                        @<tr>
                                            <td>Retail</td>
                                            <td>@FormatCurrency(Model.FDetail.Retail) per @Model.FDetail.UOM</td>
                                        </tr>
                                    End If
                                </table>
                            </div>
                        </div>
                    End if

                    @*<div class="card my-3">
                        <div class="card-header">Availability</div>
                        <div class="card-body">
                            <h4>Total Available: @Model.FDetail.QtyInStock @Model.FDetail.UOM</h4>
                            @if Model.FDetail.NextATPQty > 0 And Not IsNothing(Model.FDetail.AvailDate) Then
                                @<h4>with @Model.FDetail.NextATPQty @Model.FDetail.UOM expected on @Model.FDetail.AvailDate.ToShortDateString()</h4>
                            End If
                        </div>
                    </div>*@

                    @If Model.FDetail.ColorWays.Count > 0 Then
                        @<div class="card my-3">
                            <div class="card-header">Other Color Ways</div>
                            <div class="card-body row">

                                @for each cw In Model.FDetail.ColorWays
                                    @<div class="col-lg-2">
                                        <a href="@Url.Action("FabDetail", "Resources", New With {.SKU = cw.SKU})">
                                            <img src="@Url.Content(cw.ColorWayImage)" class="img-fluid" />
                                        </a>
                                        @cw.SKU
                                    </div>
                                Next
                            </div>
                        </div>
                    End If


                </div>
            </div>



        </div>
        @*@If User.IsInRole("SeeFabricStorageBins") And Model.FDetail.FabStock.Count > 0 Then
            @<div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Fabric Roll Information with quantity on roll. (This does not reflect availability)</div>
                    <div class="card-body">
                        <table class="table table-responsive table-condensed">
                            <tr>
                                <th>Type</th>
                                <th>Stg Bin</th>
                                <th>Quantity</th>
                            </tr>
                            @for each fs In Model.FDetail.FabStock
                                @<tr>
                                    <td>001</td>
                                    <td>@fs.StorageBin</td>
                                    <td>@fs.AvailStock  @fs.Unit</td>
                                </tr>
                            Next
                        </table>
                    </div>
                </div>
            </div>
        End If*@

        @*@<div class="col-lg-12 text-center">
            <h6>
                * This report is intended as a reference only. This amount can change based on orders
                received during the day. As of @Model.FDetail.reportStatusDate  this is the Total Available to Promise after current order requirements.
            </h6>
        </div>*@


    Else
        @<div class="col-lg-12" style="margin-top:30px;">
            <div class="col-lg-6 offset-3">
                <div class="card">
                    <div class="card-header">We're Sorry But ...</div>
                    <div class="card-body text-center">

                        <h2>Fabric not Found.</h2>
                        <h4>Please click the back button or perform your fabric search again.</h4>
                    </div>
                </div>
            </div>
        </div>
    End If






