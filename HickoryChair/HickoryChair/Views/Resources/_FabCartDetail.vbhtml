﻿
@ModelType Models.FabCart

<div id="fabCartDiv" class="py-3">
    @Using (Ajax.BeginForm("_FabCartDetail", "Products", New AjaxOptions With {.UpdateTargetId = "fabCartDiv"}))
        @<div Class="card">
            <div Class="card-header">Fabric Basket</div>
            <input type="hidden" name="SKU" id="SKU" value="@Model.SKU" />
            <div Class="card-body">
                @Model.FabCartList.Count Item/s in Your Basket.
                @*@For Each fab In Model.FabCartList
                    @<b>@fab</b>
                Next*@

                <div class="container">
                    @If Model.FabCartList.Contains(Model.SKU) Then
                        @<button name="ActionType" type="submit" id="Remove" value="Remove" class="btn btn-info btn-xs" onclick="return confirm('Are you sure?');">
                            <span class="fa-trash fa"></span> Remove
                        </button>
                    Else
                        @<button name="ActionType" type="submit" id="Add" value="Add" class="btn btn-info btn-xs">
                            <span class="fa fa-plus"></span> Add
                        </button>
                    End If
                    @If Model.FabCartList.Count > 0 Then
                        @<button name="ActionType" type="submit" id="Clear" value="Clear Cart" class="btn btn-info btn-xs" onclick="return confirm('Are you sure? This will remove all fabrics from your basket.');">
                            <span class="fa fa-trash"></span> Clear
                        </button>
                    End If
                    <a href="@Url.Action("ViewFabBasket", "RHF")" class="btn btn-info btn-xs">
                        <span class="fa fa-ey"></span> View
                    </a>

                    <a href="@Url.Action("RequestFabBasket", "RHF")" class="btn btn-info btn-xs">
                        <span class="fa -tags"></span> Request Memos
                    </a>                   
                </div>
            </div>
        </div>
    End Using
</div>



