﻿@ModelType Models.Finishes

@Code
    ViewData("Title") = "Finishes"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <style>
        #finishes {
            width: 95%;
            margin: 25px auto;
            font-family: 'Verlag-Light';
        }

        .group-title {
            margin: 15px 0;
            /*font-weight:bold;*/
            font-size: 15pt;
            border-bottom: 1px solid gainsboro;
        }

        .finish-item {
            text-align: center;
            width: 19.75%;
            display: inline-block;
            margin: 10px 0;
        }

        a, a:hover {
            color: black;
        }
    </style>
End Section

@Html.Action("FinishPartial", "Resources")
