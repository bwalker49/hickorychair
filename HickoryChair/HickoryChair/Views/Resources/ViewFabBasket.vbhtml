﻿@ModelType Models.FabCartView
@Code
    ViewData("Title") = "View Fabric Basket"
End Code


<container>
    <div Class="col-lg-12">
        <h2 class="text-center">View Fabric Basket</h2>


        <div class="col-lg-8 col-lg-offset-2">
            @If Model.FabViewList.Count > 0 Then
            @<Table Class="table table-responsive">
                <tr>
                <th>&nbsp;</th>
                    <th>Fabric</th>
                    <th>Qty</th>
                    <th>Division</th>
                    <th>&nbsp;</th>
                </tr>
                @for Each fab In Model.FabViewList
                @<tr>
                    <td>
                        <a href="@Url.Action("FabDetail", "Products", New With {.SKU = fab.SKU})">
                            <img src="@Url.Content(fab.FabImage)" class="img-responsive" alt="@fab.SKU" />
                        </a>
                    </td>
                    <td>@fab.SKU</td>
                    <td>@fab.QtyInStock  @fab.UOM</td>
                    <td>@fab.MPG - @fab.MPG_Description</td>
                    <td>@Html.ActionLink("Delete", "FabCartDelete", New With {.SKU = fab.SKU}, New With {.onclick = "return confirm('Are you sure you wish to delete this item?');"})</td>
                </tr>
                Next
            </table>

            Else
                @<div>
                    <h3 class="text-center">Your Fabric Cart is Empty.</h3>
                </div>

            End If

            <div class="text-center">
                <a href="javascript:window.print()" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-print"></span> Print
                </a>
                <a href="@Url.Action("RequestFabBasket", "RHF")" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-tags"></span> Request Memos
                </a> 
            </div>

        </div>


    </div>

</container>

