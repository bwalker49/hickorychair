﻿
@ModelType Models.FabCart

<div id="fabCartDiv" class="py-4">
    <div Class="card">
        <div Class="card-header">Fabric Basket</div>
        <input type="hidden" name="SKU" id="SKU" value="@Model.SKU" />
        <div Class="card-body">
            <div style="padding-bottom:10px;">
                @Model.FabCartList.Count Item/s in Your Basket.
                <button name="ActionType" type="submit" id="Refresh" value="Refresh" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-refresh"></span></button>
            </div>
            <div class="container">
                @If Model.FabCartList.Count > 0 Then
                    @<button name="ActionType" type="submit" id="Clear" value="Clear Cart" class="btn btn-info btn-xs" onclick="return confirm('Are you sure? This will remove all fabrics from your basket.');">
                        <span class="fa fa-trash"></span> Clear
                    </button>
                End If
                <a href="@Url.Action("ViewFabBasket", "Products")" class="btn btn-info btn-xs">
                    <span class="fa fa-eye"></span> View
                </a>
                <a href="@Url.Action("RequestFabBasket", "Products")" class="btn btn-info btn-xs">
                    <span class="fa fa-tags"></span> Request Memos
                </a>
            </div>
        </div>
    </div>
</div>

