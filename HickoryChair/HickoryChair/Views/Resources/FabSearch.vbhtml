﻿@ModelType Models.FabricSearch

@Code
    ViewData("Title") = "Hickory Chair Fabrics"
End Code

@section styles
    <style>
        .row {
            margin-left: 0;
            margin-right: 0;
        }

        .btn-info, .btn-info:hover {
            background-color: gainsboro;
            border-color: gainsboro;
        }

        .bg-info {
            background-color: gainsboro !important;
            margin-bottom: 25px;
        }

        .grade-note {
            font-size: 8.5pt;
            color: red;
        }
    </style>
End Section


@Using Html.BeginForm("FabSearch", "Resources", FormMethod.Post, New With {.class = "form-horizontal", .id = "fabSearchForm"})
    @Html.AntiForgeryToken()

    @<div class="row py-3">
        <div class="col-3">
            <div>                           
                <div class="form-group py-3">
                    @Html.LabelFor(Function(m) m.MatlBox)
                    @Html.TextBoxFor(Function(m) m.MatlBox, New With {.class = "form-control", .PlaceHolder = "Use * for Wildcard Search."})
                </div>

                <div Class="form-group">
                    @Html.LabelFor(Function(m) m.TypeList)
                    @Html.DropDownListFor(Function(m) m.TypeList_Selected, Model.TypeList, New With {.class = "form-select"})
                </div>

                <div Class="form-group">
                    @Html.LabelFor(Function(m) m.GradeList)
                    @Html.DropDownListFor(Function(m) m.GradeList_Selected, Model.GradeList, New With {.class = "form-select"})
                </div>

                <div Class="form-group">
                    @Html.LabelFor(Function(m) m.ColorList)
                    @Html.DropDownListFor(Function(m) m.ColorList_Selected, Model.ColorList, New With {.class = "form-select"})
                </div>

                <div Class="form-group">
                    @Html.LabelFor(Function(m) m.PatternList)
                    @Html.DropDownListFor(Function(m) m.PatternList_Selected, Model.PatternList, New With {.class = "form-select"})
                </div>

                <div Class="form-group">
                    @Html.CheckBoxFor(Function(m) m.OnlyShowFabsWithImages)
                    @Html.LabelFor(Function(m) m.OnlyShowFabsWithImages)
                </div>

                <div Class="form-group">
                    @Html.CheckBoxFor(Function(m) m.OnlyShowNewIntros)
                    @Html.LabelFor(Function(m) m.OnlyShowNewIntros)
                </div>

                <div Class="form-group text-center">
                    <Button id="subButton" name="subButton" Class="btn btn-info" onclick="$('#PageIndex').val(1);">Search</Button>
                </div>
            </div>
        </div>

        @Code
            Dim lastClass As String = ""
        End Code

        <div class="col-9">
           <div class="row">               

                @If IsNothing(Model.FabSearchItems) Then
                    @<div class="col-12  text-center" style="min-height:500px;background-image: url('/Content/Images/fabsearch_bg.jpg')">
                        <div style="background-color:white;min-height:200px;margin-top:100px;padding-top:75px;border:2px dashed black;opacity:0.6;filter: alpha(opacity=80);">
                            <h3>Use the selection criteria to the left to search for fabrics.</h3>
                        </div>
                    </div>
                ElseIf Model.FabSearchItems.Count > 0 Then
                    @For Each fab In Model.FabSearchItems
                        @*If fab.FabClass <> lastClass Then
                            @<div class="col-lg-12 text-center bg-info">@fab.FabClassDescription</div>
                        End If*@
                        @<div class="col-lg-3 col-sm-1 py-3">
                            <div class="text-center">
                                <a href="@Url.Action("FabDetail", "Resources", New With {.sku = fab.MaterialNumber})">
                                    <img src="@Url.Content(fab.FabImage)" alt="Image" class="image-fluid" />
                                </a>
                            </div>
                            <div class="text-center">@fab.MaterialNumber</div>
                            <div class="text-center">@fab.MaterialDescription</div>
                            <div class="text-center">Grade: @fab.Grade</div>
                            @If fab.NewGrade <> "" And fab.NewGradeDate > Today Then
                                @<div class="text-danger text-center" style="font-size: small;">Grade change to: @fab.NewGrade on @fab.NewGradeDate</div>
                            End If
                            @If fab.IsNewIntro Then
                                @<div class="text-center bg-warning">New Intro</div>
                            Else
                                @<div>&nbsp;</div>
                            End If                            
                        </div>
                        lastClass = fab.FabClass
                    Next

                    @<div class="clearfix"></div>
                    @<div class="row">
                        <div Class="col-lg-2 offset-4  ">
                            @Html.LabelFor(Function(m) m.PageList)
                            @Html.DropDownListFor(Function(m) m.PageIndex, Model.PageList, New With {.class = "form-select", .onchange = "document.forms[""fabSearchForm""].submit();"})
                        </div>
                    </div>

                Else
                    @<div class="col-lg-12" style="margin-top:30px;">

                        <div class="card ">
                            <div class="card-header bg-info">We're Sorry But ...</div>
                            <div class="card-body text-center">

                                <h2>No Items Found.</h2>
                                <h4>Please click the back button or perform your fabric search again.</h4>
                            </div>
                        </div>

                    </div>
                End If
            </div>
        </div>
    </div>
            End Using

@section scripts
    <script>
        $(document).ready(function () {
            if ($('#TypeList_Selected').val() === 'Leather') {
                $('#subButton').one('click', function (e) {

                });
            }
        })
    </script>
End Section




