﻿@ModelType PressRelease

@Code
    ViewData("Title") = "PressRelease"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <style>
        #press-release-wrapper {
            width: 70%;
            margin: 25px auto;
            text-align: center;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }

        .pr-date {
            font-size:15pt;
            font-weight:bold;
            text-decoration:underline;
            margin-top:25px;
        }

        .press-release {
            margin:5px 0;
        }

            .press-release a {
                color:black;
            }
    </style>
End Section

@Code
    Dim lastMonth As String = ""
    Dim lastYear As String = ""
End Code

<div id="press-release-wrapper">
    @For Each pr In Model.PressReleases
        If pr.Month <> lastMonth Then
            @<div class="pr-date">@pr.Month @pr.Year</div>
        End If
        @<div class="press-release"><a href="@Url.Content(pr.Link)" target="_blank">@pr.Title</a></div>

        @Code
            lastMonth = pr.Month
            lastYear = pr.Year
        End Code
            Next
</div>

