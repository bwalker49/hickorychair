﻿@ModelType Careers

@Code
    ViewData("Title") = "ShowCareer"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <style>
        #application-wrapper {
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            width:50%;
            margin:0 auto;
        }

        #application-title {
            font-size:15pt;
            font-weight:bold;
            text-align:center;
            margin:50px 0;
        }

        #application-date, #application-id, #job-title {
            margin:10px 0;
            font-size:13pt;
        }

        #form-wrapper {
            border-top:1px solid gainsboro;
            padding-top:25px;
            margin-top:25px;
        }

        .form-input {
            margin-bottom:15px;
        }

        /*.input-title, .input {
            display:inline-block;
        }

        .input-title {
            width:20%;
        }*/

        .input {        
            width:100%;
        }

        #hc-number, #hc-email {
            text-align:center;font-size:15pt;margin:10px 0;
        }

        #btn-wrapper {
            text-align:center;
            margin:25px 0;
        }
    </style>
End Section

    <div id="application-wrapper">
        <div id="application-title">Job Interest Questionaire</div>
        <div id="application-date">Date: @System.DateTime.Now.ToString("d")</div>
        <div id="application-id">JobID: @Model.CareerList.First.JobID</div>
        <div id="job-title">Job Title: @Model.CareerList.First.Title</div>

        <div id="form-wrapper">
            <div class="form-input">
                <label for="name" class="input-title">Full Name:</label>
                <input class="input" type="text" id="name" />
            </div>
            <div class="form-input">
                <label for="address" class="input-title">Address:</label>
                <input class="input" type="text" id="address" />
            </div>
            <div class="form-input">
                <label for="city" class="input-title">City:</label>
                <input class="input" type="text" id="city" />
            </div>
            <div class="form-input">
                <label for="state" class="input-title">State:</label>
                <input class="input" type="text" id="state" />
            </div>
            <div class="form-input">
                <label for="zip" class="input-title">ZipCode:</label>
                <input class="input" type="text" id="zip" />
            </div>
            <div class="form-input">
                <label for="telephone" class="input-title">Telephone:</label>
                <input class="input" type="text" id="telephone" />
            </div>
            <div class="form-input">
                <label for="email" class="input-title">Email Address:</label>
                <input class="input" type="text" id="email" />
            </div>
            <div class="form-input">
                <label for="ptinterest" class="input-title">Part Time Interest:</label>
                <input class="input" type="text" id="ptinterest" />
            </div>
            <div class="form-input">
                <label for="ftinterest" class="input-title">Full Time Interest:</label>
                <input class="input" type="text" id="ftinterest" />
            </div>

            <div class="form-input">
                <label for="past" class="input-title">Have you worked at Hickory Chair in the past? If so when?:</label>
                <input class="input" type="text" id="past" />
            </div>

            <div class="form-input">
                <label for="employees" class="input-title">Do you know any Hickory Chair employees? If yes, list their names:</label>
                <input class="input" type="text" id="employees" />
            </div>

            <div class="form-input">
                <label for="refer" class="input-title">Did a Hickory Chair employee refer you? If yes, please list their name:</label>
                <input class="input" type="text" id="refer" />
            </div>

            <div class="form-input">
                <label for="experience" class="input-title">Do you have experience in the position for which you are applying? If yes, please describe your experience level:</label>
                <input class="input" type="text" id="experience" />
            </div>

            <div class="form-input">
                <label for="position" class="input-title">How did you learn about our position?:</label>
                <input class="input" type="text" id="position" />
            </div>

            <div id="hc-number">
                Call 828-679-2690
            </div>

            <div id="hc-email">
                Email us at <a href="mailto:jobs@hickorychair.com">jobs@hickorychair.com</a>
            </div>

            <div id="btn-wrapper">
                <input type="button" id="application-submit" value="Submit Form" data-email="@System.Web.Configuration.WebConfigurationManager.AppSettings("ApplicationEmailAddress")"/>
            </div>
        </div>
    </div>


@Section scripts
    <script>
        @*function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }*@

        function setupEmail(toEmail) {
            var FromEmail = $('#email').val(),
                Subject = 'New Job Application Received For: ' + $('#application-id').text().split(':')[1] + ' - ' + $('#job-title').text().split(':')[1] 
                Body = '';

                if ($('#name').val() !== '') {
                    Body += 'Name:' + '\n' +  $('#name').val();
                } else {
                    alert('Please enter a name.')
                }

                if ($('#address').val() !== '') {
                    Body += '\n\nAddress:' + '\n' + $('#address').val();
                } else {
                    alert('Please enter a address.')
                }

                if ($('#city').val() !== '') {
                    Body += '\n\nCity:' + '\n'  + $('#city').val();
                } else {
                    alert('Please enter a city.')
                }

                if ($('#state').val() !== '') {
                    Body += '\n\nState:' + '\n' + $('#state').val();
                } else {
                    alert('Please enter a state.')
                }

                if ($('#zip').val() !== '') {
                    Body += '\n\nZip::' + '\n' + $('#zip').val();
                } else {
                    alert('Please enter a zip.')
                }

                if ($('#telephone').val() !== '') {
                    Body += '\n\nTelephone' + '\n' + $('#telephone').val();
                } else {
                    alert('Please enter a telephone number.')
                }

                if ($('#email').val() !== '' && validateEmail($('#email').val())) {
                    Body += '\n\nEmail Address:' + '\n' + $('#email').val();
                } else {
                    alert('Please enter a valid email address.')
                }

                if ($('#ptinterest').val() !== '') {
                    Body += '\n\n\n\nPart Time Interest:\n\n' + $('#ptinterest').val()
                }

                if ($('#ftinterest').val() !== '') {
                    Body += '\n\n\n\nFull Time Interest:\n\n' + $('#ftinterest').val()
                }

                if ($('#past').val() !== '') {
                    Body += '\n\n\n\nHave you worked at Hickory Chair in the past?:\n\n' + $('#past').val()
                }

                if ($('#employees').val() !== '') {
                    Body += '\n\n\n\nDo you know any Hickory Chair employees?:\n\n' + $('#employees').val()
                }

                if ($('#refer').val() !== '') {
                    Body += '\n\n\n\nDid a Hickory Chair employee refer you?:\n\n' + $('#refer').val()
                }

                if ($('#experience').val() !== '') {
                    Body += '\n\n\n\nDo you have experience in the position for which you are applying?:\n\n' + $('#experience').val()
                }

                if ($('#position').val() !== '') {
                    Body += '\n\n\n\nDo you know any Hickory Chair employees?:\n\n' + $('#position').val()
                }

            if (FromEmail !== '' && Subject !== '') {
                if (validateEmail(FromEmail)) {
                    sendEmail(toEmail, FromEmail, Subject, Body)
                } else {
                    alert('Please enter a valid email address.')
                }
            } else {
                alert('Please make sure you enter all required information.')
            }
        }

        function sendEmail(ToEmail, FromEmail, Subject, Body) {
            $.ajax({
                type: "POST",
                url: '/Misc/SendEmail',
                data: "{ToEmail:'" + ToEmail + "', FromEmail:'" + FromEmail + "', Subject:'" + Subject + "', Body:'" + Body + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: 'html',
                success: function (data) {
                    alert('Application Successfully Submitted!')

                    setTimeout(function () {
                        window.location.href = '/Misc/Careers'
                    }, 1500)
                }
            })
        }

        $(document).ready(function () {
            $('#application-submit').on('click', function () {
                setupEmail($(this).attr('data-email'))
            })
        })
    </script>
End Section
