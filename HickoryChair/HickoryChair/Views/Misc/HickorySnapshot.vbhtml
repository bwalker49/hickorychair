﻿@Code
    ViewData("Title") = "Historic Hickory Snapshot"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link rel="stylesheet" href="~/Content/contentpage.css" />
End Section

@Html.Action("GetContentPage", "Content", New With {.page = "Snapshot"})

