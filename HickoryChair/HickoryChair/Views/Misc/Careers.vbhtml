﻿@ModelType Careers

@Code
    ViewData("Title") = "Hickory Chair Careers"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <style>
    #career-wrapper { width: 90%; margin: 0 auto; font-family: 'Verlag-Light'; }

    #career-title { text-align: center; font-size: 15pt; font-weight: bold; margin: 30px 0; font-family: 'MillerText-Roman'; }

        #career-img-wrapper {
            width:45%;
            text-align:right;
            display:inline-block;
            vertical-align:top;
        }

        #career-text-wrapper {
            width:45%;
            display:inline-block;
            margin-left:5%;
        }

        #internship-program {
            font-size:14pt;
        }

            #internship-program a {
                color:black;
                text-decoration:underline;
            }

        #job-opening-title {
            font-size:17pt;
            font-weight:bold;
            text-align:center;
            margin-top:25px;
        }

        #job-openings {
            width:70%;margin:25px auto;
        }

            #job-openings table {
                width:100%;
            }

        #job-disclaimer {
            text-align:center;
            width:70%;
            margin:25px auto;
        }

        #job-openings a {
            color:black;
            font-weight:bold;
            text-decoration:underline;
        }
    </style>
End Section

<div id="career-wrapper">
    <div id="career-title">Come Join Our <i>Hickory Chair Family</i></div>
    <div id="career-img-wrapper">
        <img src="~/Images/layout/ContentPageImages/Career.jpg" />
    </div>
    <div id="career-text-wrapper">
        <p>For 110 years, Hickory Chair has been a destination for not only talented artisans and craftsmen, but also an inviting company eager to teach new skills to those who were looking for more than a job. A place for those who wanted a great place to work that would be supportive and collaborative where they could make a great living, advance in their career and at the same time lead a happy and fulfilling life. This is what we are known for and this is what we will always be.</p>
        <p>We hope you will find a role below that entices you. <b>If you love working with your hands we can provide opportunities to train you with valuable new skills for one of the positions making fine upholstery and wood products listed below.</b> If you prefer, we have opportunities to join our office staff. Unlike most companies in our industry, we offer both Part-Time and Full-Time positions. We have an On-Site Doctor and Nurse, offer a Competitive Salary with great benefits including Paid Time Off, Paid Holidays and 401K. Some positions carry a Sign-On Bonus as well. Perhaps you are a student eager to learn more about working in our industry. One of our hands-on internships could be the perfect opportunity for you to start your career. We have our eyes on the future and look forward to meeting you.</b> </p>
        <p>We invite you to review the following list of current openings. Each has a specific job description for for your review. If you are interested in the position, simply complete the job interest form. For further assistance, you may call us at 828-679-2690 or email us at jobs@hickorychair.com.</p>
        <p id="internship-program">If you are interested in our Internship Program please <a href="~/Documents/Careers/Internship_Questionaire_2021.pdf" target="_blank">Click Here</a></p>
    </div>

    <div id="job-opening-title">
        Current Job Openings
    </div>
    <div id="job-openings">
        <table>
            <tr>
                <td>Job Number</td>
                <td>Openings</td>
                <td>Current Position</td>
                <td></td>
                <td></td>
            </tr>
            @For Each career In Model.CareerList
                @<tr>
                    <td>@career.JobID</td>
                    <td>@career.Qty</td>
                    <td>@career.Title</td>
                    <td><a href="@Url.Content(career.Doc)">View Job Description</a></td>
                    <td><a href="@Url.Action("ShowCareer", "Misc", New With {.JobID = career.JobID})">Apply</a></td>
                </tr>
            Next
        </table>        
    </div>
    <div id="job-disclaimer">
        Hickory Chair seeks the most qualified employees available to help ensure growth, progress and success for the Company. The Company does not, and shall not, discriminate against a person because of his or her race, color, religion, sex, national origin, citizenship, disability, veteran status, age or other protected status in any aspect of employment opportunity. The Company's policy of equal employment applies to all aspects of the employment process, including but not limited to hiring, advancement and promotion, compensation and benefits administration, training and development, and other personnel actions.
    </div>
</div>

