﻿@ModelType PressRelease

@Code
    ViewData("Title") = "Made For You Magazine"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/madeforyou.css" rel="stylesheet" />
End Section

<div id="made-for-you-wrapper">
    @For Each pr In Model.PressReleases
        @<div class="pr-wrapper">
            <div id="imgwrapper">
                <img src="@Url.Content("~/Images/layout/MFYCovers/" & pr.CoverImg & ".jpg")" />
            </div>
            <div id="copywrapper">
                <div class="title">@pr.Title</div>
                <div class="text">@pr.Text</div>
                <div class="link"><a href="@Url.Content(pr.Link)" target="_blank">View @pr.Title</a></div>
            </div>
        </div>
    Next
</div>


