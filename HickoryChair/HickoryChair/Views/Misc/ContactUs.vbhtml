﻿
@Code
    ViewData("Title") = "Contact Us"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

@section styles
    <link href="~/Content/contactus.css" rel="stylesheet" />   
    
    <style>
        #header-social i {
            color:black!important;
        }
    </style>
End Section

    <div id="page-wrapper">
        <div class="title">CONTACT US</div>



        @*<div class="block">
            <div class="title">MAILING:</div>
            <div class="block-content">
                <div>P.O. Box 3444</div>
                <div>Hickory, NC 28603</div>
            </div>
        </div>

        <div class="block">
            <div class="title">SHOWROOM:</div>
            <div class="block-content">
                <div>Steele Building, 3rd Floor</div>
                <div>200 Steele Street</div>
                <div>High Point, NC 27260</div>
                <div><i>Only open during Spring & Fall Furniture Market</i></div>
            </div>
        </div>

        <div class="block">
            <div class="title">GENERAL MANAGER:</div>
            <div class="block-content">
                <div>Federico Contigiani</div>
            </div>
        </div>*@

        <div id="email-form-wrapper">
            <div class="title">SEND US AN EMAIL:</div>
            <div>@Html.Partial("~/Views/Shared/_EmailForm.vbhtml")</div>
        </div>

        <div class="block">
            <div class="title">MAIN OFFICE:</div>
            <div class="block-content">
                <div>37 9th St Pl SE</div>
                <div>Hickory, NC 28602</div>
                <div>(828) 324-1801</div>
            </div>

            <div class="title">CONNECT WITH US:</div>
            <div class="block-content">
                <div id="header-social">
                    <div><a href="http://www.facebook.com/hickorychairfurniture" target="_blank"><i class="fa fa-facebook"></i></a></div>
                    <div><a href="https://www.instagram.com/hickorychair/" target="_blank"><i class="fa fa-instagram"></i></a></div>
                    <div><a href="http://twitter.com/#!/hickorychairco" target="_blank"><i class="fa fa-twitter"></i></a></div>
                    <div><a href="http://pinterest.com/hickorychairco/" target="_blank"><i class="fa fa-pinterest"></i></a></div>
                    <div><a href="http://www.houzz.com/pro/hickorychair/the-hickory-chair-furniture-co" target="_blank"><i class="fa fa-houzz"></i></a></div>
                </div>
            </div>
        </div>

        <div id="dialog" style="display:none;">
            <div id="dialog-content">

            </div>
        </div>
    </div>
