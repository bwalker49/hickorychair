﻿Imports System.Web.Optimization

Public Module BundleConfig
    ' For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)

        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryui").Include(
                    "~/Scripts/jquery-ui.js"))

        ' Use the development version of Modernizr to develop with and learn from. Then, when you're
        ' ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"))

        bundles.Add(New ScriptBundle("~/bundles/bootstrap").Include(
                  "~/Scripts/bootstrap.js"))

        bundles.Add(New StyleBundle("~/Content/ConsumerSiteStyles").Include(
                  "~/Content/layout.css",
                  "~/Content/navigation.css",
                  "~/Content/font-awesome.min.css",
                  "~/Content/slick.css",
                  "~/Content/slick-theme.css",
                  "~/Content/jquery-ui.css",
                  "~/Content/jquery-ui.theme.css"))

        bundles.Add(New StyleBundle("~/Content/HCFonts").Include(
                  "~/Content/fonts/fonts.css"))

        bundles.Add(New ScriptBundle("~/bundles/ConsumerJS").Include(
                "~/Scripts/Hickory.js",
                "~/Scripts/jquery.unobtrusive-ajax.js",
                "~/Scripts/slick.min.js"))
    End Sub
End Module

