﻿function imgerror() {
    alert('itls')
}

function buildEmailForm(toEmail) {
    $.ajax({
        type: "POST",
        url: '/Misc/ShowEmailForm',
        contentType: "application/json; charset=utf-8",
        dataType: "html",
        success: function (data) {
            $('#dialog-content').html(data);
            $('#dialog').dialog({
                open: function () {
                    $('#dialog-content').find('.submitEmail').attr('data-email', toEmail);

                    $('.submitEmail').on('click', function () {
                        setupEmail($(this).attr('data-email'), $(this));
                    })
                },
                width: '50%',
                modal: true,
                position: { my: 'center', at: 'center', of: window, collison: 'fit' }
            });
        }
    });
}

function setupEmail(toEmail, selector) {
    var currentForm = selector.closest('#email-form').children(),
        FromEmail = currentForm.find('.email').val(),
        Subject = currentForm.find('.subject').val(),
        Name = currentForm.find('.name').val(),
        Phone = currentForm.find('.phone').val(),
        Message = currentForm.find('.message').val(),
        Body;

    if (Name !== '' && Phone !== '') {
        Body = "Contact Name: " + Name + "\n\n" + 'Contact Phone: ' + Phone + '\n\n' + Message
    }
    else if (Name !== '') {
        Body = "Contact Name: " + Name + "\n\n" + Message
    }
    else if (Phone !== '') {
        Body = 'Contact Phone: ' + Phone + '\n\n' + Message
    } else {
        Body = Message;
    }

    if (FromEmail !== '' && Subject !== '') {
        if (validateEmail(FromEmail)) {
            sendEmail(toEmail, FromEmail, Subject, Body)
        } else {
            alert('Please enter a valid email address.')
        }
    } else {
        alert('Please make sure you enter all required information.')
    }
}

function sendEmail(ToEmail, FromEmail, Subject, Body) {
    $.ajax({
        type: "POST",
        url: '/Misc/SendEmail',
        data: "{ToEmail:'" + ToEmail + "', FromEmail:'" + FromEmail + "', Subject:'" + Subject + "', Body:'" + Body + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: 'html',
        success: function (data) {
            $('.email-message').text(data).css('visibility', 'visible');

            setTimeout(function () {
                $('.email-message').css('visibility', 'hidden');
                $('.email').val('');
                $('.subject').val('');
                $('.name').val('')
                $('.phone').val('');
                $('.message').val('');
            }, 1500)
        }
    })
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

$(document).ready(function () {    
    

    $('.nav-dropdown').on('click', function () {
        var $btn = $(this)
        $selected = $(this).data('content'),
        $dd = $('#nav-dropdown'),
        $active = $('#nav-dropdown').attr('data-active')
        $dd.css('top', $('#nav-link-wrapper').height() + $('#nav-wrapper').height() + 32);

        if ($btn.attr('data-content') === $active) {            
            $dd.slideToggle(function () {
                $('#nav-dropdown').attr('data-active', '');
            });
        } else {           
            $.ajax({
                type: 'POST',
                url: $btn.attr('data-method'),
                data: "{value:'" + $btn.attr('data-content') + "'}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'html',
                success: function (data) {
                    $dd.hide();
                    $dd.html(data);
                    if ($btn.attr('data-content') === 'Products') {
                        $('#nav-right-header').show();
                    }
                    $dd.slideToggle(function () {
                        $('#nav-dropdown').attr('data-active', $btn.attr('data-content'));                        

                        $('.nav-left-link').on('click', function () {
                            var headerVal

                            if ($(this).parentsUntil('#nav-dropdown').parent().attr('data-active') === 'Products') {
                                headerVal = $(this).text();
                            } else {
                                headerVal = 'NA'
                            }
                             
                            $.ajax({
                                type: 'POST',
                                url: '/Navigation/SecondLevel',
                                data: "{value:'" + $(this).attr('data-id') + "'}",
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'html',
                                success: function (data) {                                    
                                    $('#nav-right').html('').html(data);
                                    if (headerVal !== 'NA') {
                                        $('#nav-right-header').text('').text(headerVal).show();
                                    }                                    
                                }
                            })                            
                        })
                    });
                }
            })
        }        
    })

    $('html').click(function (e) {
        if ($('#nav-dropdown').is(':visible')) {
            if (!$(e.target).hasClass('nav-dropdown') && !$(e.target).hasClass('nav-left-link') && !$(e.target).hasClass('nav-left-header')) {
                $('#nav-dropdown').slideToggle();
            }
        }
        
    })

    $('#menu-icon').on('click', function () {
        if ($('#resp-search-wrapper').is(':visible')) {
            $('#resp-search-wrapper').hide();
        }

        $('#nav-dropdown-res').slideToggle(function () {
            if ($('#nav-dropdown-res').is(':visible')) {
                $('.header-link').unbind().on('click', function (target) {                                       
                    var $this = $(this);

                    if ($(this).next('.first-level-links').length !== 0) {
                        if ($this.next('.first-level-links').is(':visible')) {
                            $this.next('.first-level-links').slideUp();
                            $this.next('.second-level-links').slideUp();
                        } else {
                            $('.first-level-links').slideUp();
                            $('.second-level-links').slideUp();
                            $this.next('.first-level-links').slideDown(function () {                            
                            });
                        }
                    } else {
                        var url;

                        if ($this.attr('data-category') === 'Catalogs') {
                            url = '/Navigation/ShowCatalogs'
                        } else {
                            url = '/Navigation/FirstLevel'
                        }

                        $.ajax({
                            type: 'POST',                            
                            url: url,
                            data: "{value:'" + $(this).attr('data-category') + "'}",
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'html',
                            success: function (data) {                             
                                $this.after(data);
                                $('.first-level-links').slideUp();
                                $this.next('.first-level-links').slideToggle(function () {
                                    $('.nav-left-link').unbind().on('click', function () {
                                        var $this = $(this);

                                        if ($(this).next('.second-level-links').length !== 0) {
                                            if ($this.next('.second-level-links').is(':visible')) {
                                                $this.next('.second-level-links').slideUp();
                                            } else {
                                                $('.second-level-links').slideUp();
                                                $this.next('.second-level-links').slideDown(function () {
                                                });
                                            }
                                        } else {

                                            $.ajax({
                                                type: 'POST',
                                                url: '/Navigation/SecondLevelResp',
                                                data: "{value:'" + $(this).attr('data-id') + "'}",
                                                contentType: 'application/json; charset=utf-8',
                                                dataType: 'html',
                                                success: function (data) {
                                                    $this.after(data);
                                                    $('.second-level-links').slideUp();
                                                    $this.next('.second-level-links').slideToggle(function () {

                                                    });
                                                }
                                            })
                                        }
                                    })                                    
                                });
                            }
                        })
                    }                                        
                })
            } else {                
                $('.first-level-links').remove();
                $('.second-level-links').remove();
            }
            
        })
    })

    $('.submitEmail').on('click', function () {
        setupEmail($(this).attr('data-email'), $(this));
    })
})