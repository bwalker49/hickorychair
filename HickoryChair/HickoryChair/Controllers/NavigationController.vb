﻿Public Class NavigationController
    Inherits System.Web.Mvc.Controller

    <HttpPost>
    Function ShowProducts() As ActionResult
        Dim retVal As New Navigation("Products")

        Return PartialView("~/Views/Shared/_NavProducts.vbhtml", retVal)
    End Function

    <HttpPost>
    Function ShowCatalogs() As ActionResult
        Dim retVal As New Navigation("Catalog")

        Return PartialView("~/Views/Shared/_Catalogs.vbhtml", retVal)
    End Function

    <HttpPost>
    Function ShowStyle() As ActionResult
        Dim retVal As New Navigation("Style")

        Return PartialView("~/Views/Shared/_NavStyles.vbhtml", retVal)
    End Function

    <HttpPost>
    Function ShowStory() As ActionResult
        Dim retVal As New Navigation("Story")

        Return PartialView("~/Views/Shared/_NavStory.vbhtml", retVal)
    End Function

    <HttpPost>
    Function BuildTwoLevelNav(ByVal value As String) As ActionResult
        Dim retVal As New Navigation(value)

        Return PartialView("~/Views/Shared/_TwoLevelNav.vbhtml", retVal)
    End Function

    <HttpPost>
    Function FirstLevel(ByVal value As String) As ActionResult
        Dim retVal As New Navigation(value)

        Return PartialView("~/Views/Shared/_FirstLevelNav.vbhtml", retVal)
    End Function

    <HttpPost>
    Function SecondLevel(ByVal value As String) As ActionResult
        Dim retVal As New Navigation(value)

        Return PartialView("~/Views/Shared/_SecondLevelNav.vbhtml", retVal)
    End Function

    <HttpPost>
    Function SecondLevelResp(ByVal value As String) As ActionResult
        Dim retVal As New Navigation(value)

        Return PartialView("~/Views/Shared/_SecondLevelRespNav.vbhtml", retVal)
    End Function

    'Function GetSearchName(Optional ByVal TypeID As String = "") As ActionResult
    '    If TypeID <> "" Then
    '        Dim retVal As 
    '    End If
    'End Function
End Class
