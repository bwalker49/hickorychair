﻿Imports System.Net
Imports System.Web.Http
Imports WebSupergoo.ABCpdf9
Imports System.IO

Public Class MiscController
    Inherits System.Web.Mvc.Controller

    'Function Index() As ActionResult
    '    Return View()
    'End Function

    Function About() As ActionResult
        ViewData("Message") = "Your application description page."

        Return View()
    End Function

    Function FAQ() As ActionResult
        Return View("~/Views/Misc/FAQ.vbhtml")
    End Function

    Function Contact() As ActionResult
        Return View("~/Views/Misc/ContactUs.vbhtml")
    End Function

    Function Careers() As ActionResult
        Dim newCareer As New Careers

        Return View("~/Views/Misc/Careers.vbhtml", newCareer)
    End Function

    Function ShowCareer(ByVal jobid As String) As ActionResult
        Dim newCareer As New Careers(jobid)

        Return View("~/Views/Misc/ShowCareer.vbhtml", newCareer)
    End Function

    Function CentennialStory() As ActionResult
        Return View("~/Views/Misc/CentennialStory.vbhtml")
    End Function

    Function HickorySnapshot() As ActionResult
        Return View("~/Views/Misc/HickorySnapshot.vbhtml")
    End Function

    Function ThenNow() As ActionResult
        Return View("~/Views/Misc/ThenNow.vbhtml")
    End Function

    Function ArtisanalUpholstery() As ActionResult
        Return View("~/Views/Misc/ArtisanalUpholstery.vbhtml")
    End Function

    Function CustomWoodworking() As ActionResult
        Return View("~/Views/Misc/CustomWoodworking.vbhtml")
    End Function

    Function EnvironmentalResponsibility() As ActionResult
        Return View("~/Views/Misc/EnvironmentalResponsibility.vbhtml")
    End Function

    Function EmployeeEmpowerment() As ActionResult
        Return View("~/Views/Misc/EmployeeEmpowerment.vbhtml")
    End Function

    Function Monogramming() As ActionResult
        Return View("~/Views/Misc/Monogramming.vbhtml")
    End Function

    Function MadeForYou() As ActionResult
        Return View("~/Views/Misc/MadeForYou.vbhtml")
    End Function

    Function ArtistStudio() As ActionResult
        Return View("~/Views/Misc/ArtistStudio.vbhtml")
    End Function

    Function NailTrim() As ActionResult
        Return View("~/Views/Misc/NailTrim.vbhtml")
    End Function

    Function Striping() As ActionResult
        Return View("~/Views/Misc/Striping.vbhtml")
    End Function

    Function PressRelease() As ActionResult
        Return View("~/Views/Misc/PressRelease.vbhtml")
    End Function

    Function HomeGallery() As ActionResult
        Dim misc As New Misc

        Return PartialView("~/Views/Home/_HomeGallery.vbhtml", misc)
    End Function

    <HttpPost>
    Function SendEmail(ByVal ToEmail As String, ByVal FromEmail As String, ByVal Subject As String, ByVal Body As String, Optional ByVal isHtml As Boolean = False) As String
        Dim retVal As String = ""

        retVal = Misc.SendEmail(ToEmail, FromEmail, "Hickory Chair Furniture Co. Question Submission", Body, isHtml)

        Return retVal
    End Function


    Function ProcessPDF2Thumbnail(ByVal FilePath As String, ByVal FileName As String, ByVal DPI As String) As ActionResult
        '*********************************************************************************************
        'Author:    Joe Kelly
        'Date:      1/28/2013
        'Purpose:   Take a PDF and return a png thumbnail of the first page.
        '*********************************************************************************************
        Dim retStream As New MemoryStream
        Try
            Dim pdfPath As String = FilePath & FileName
            Dim theDoc As Doc = New Doc()
            theDoc.Read(Server.MapPath(pdfPath))
            theDoc.Rendering.DotsPerInch = DPI
            theDoc.PageNumber = 1
            theDoc.Rect.String = theDoc.CropBox.String
            theDoc.Rendering.Save("test.jpg", retStream)
            theDoc.Dispose()
        Catch ex As Exception
            Misc.LogError(ex)
        End Try
        Return File(retStream.ToArray, "image/jpeg")
    End Function
End Class
