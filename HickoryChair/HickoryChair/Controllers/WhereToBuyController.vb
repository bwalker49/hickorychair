﻿Imports System.Net
Imports System.Web.Mvc
Imports Antlr.Runtime


Namespace Controllers
    Public Class WhereToBuyController
        Inherits Controller

        ' GET: WhereToBuy
        Function Index() As ActionResult
            Try
                Return View(New Models.WhereToBuy.WhereToBuy())
            Catch ex As Exception
                Misc.LogError(ex)
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message)
            End Try
        End Function


        Function GetSearchResultsMap(searchForm As Models.WhereToBuy.WhereToBuy) As ActionResult
            Try
                searchForm.GetSearchResults()
                Return View("_SearchResultMap", searchForm)
            Catch ex As Exception
                Misc.LogError(ex)
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message)
            End Try
        End Function

        <HttpPost>
        Function GetShowroomID(ByVal zip As String) As String
            Dim retVal As String = ""

            retVal = Models.WhereToBuy.WhereToBuy.GetShowroomID(zip)

            Return retVal
        End Function
    End Class
End Namespace