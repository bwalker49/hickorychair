﻿Imports System.Web.Mvc
Imports System.Net

Namespace Controllers
    Public Class ProductsController
        Inherits Controller

        ' GET: Products
        Function Index() As ActionResult
            Return View()
        End Function

        <Route("Products/ProductDetails/{sku}")>
        Function ProductDetails(ByVal SKU As String) As ActionResult
            Dim product As New Products(SKU)

            Return View("~/Views/Products/ProductDetails.vbhtml", product)
        End Function

        <Route("Products/ShowResults/{TypeID?}/{SubTypeID?}/{CollectionID?}/{DesignerID?}/{WildCard?}/{NewIntros?}/{InStock?}/{Wishlist?}")>
        Function ShowResults(Optional ByVal TypeID As String = "", Optional ByVal SubTypeID As String = "", Optional ByVal CollectionID As String = "", Optional ByVal RoomID As String = "", Optional ByVal ProductLine As String = "", Optional ByVal DesignerID As String = "", Optional ByVal WildCard As String = "", Optional ByVal NewIntros As Boolean = False, Optional ByVal InStock As String = "", Optional ByVal WishList As Boolean = False, Optional ByVal UserID As String = "", Optional ByVal PrintList As String = "") As ActionResult
            Dim terms As New Products.SearchTerms

            If TypeID IsNot Nothing Then
                terms.TypeID = TypeID
            End If
            If SubTypeID IsNot Nothing Then
                terms.SubTypeID = SubTypeID
            End If
            If DesignerID IsNot Nothing Then
                terms.DesignerID = DesignerID
            End If
            If CollectionID IsNot Nothing Then
                terms.CollectionID = CollectionID
            End If
            If RoomID IsNot Nothing Then
                terms.RoomID = RoomID
            End If
            If ProductLine IsNot Nothing Then
                terms.ProductLine = ProductLine
            End If
            If WildCard IsNot Nothing Then
                terms.WildCard = WildCard
            End If

            If NewIntros Then
                terms.NewIntros = NewIntros
            End If

            If InStock IsNot Nothing Then
                terms.InStock = InStock
            End If

            If WishList Then
                terms.WishList = WishList
            End If

            If PrintList IsNot Nothing Then
                terms.PrintList = PrintList
            End If

            terms.UserID = Profile.UserName

            Dim results As New Products(terms)

            Return View("~/Views/Products/ProductSearch.vbhtml", results)
        End Function

        <HttpPost>
        Function isSKU(ByVal sku As String) As String
            Dim retVal As String = ""

            retVal = Products.isSKU(sku)

            Return retVal
        End Function

        <HttpPost>
        Function AsShown(ByVal img As String) As ActionResult
            Dim misc As New Misc(img)

            Return Content(misc.AsShownData)
        End Function

        <HttpPost>
        Function GetSKUFabrics(ByVal sku As String) As String
            Dim retVal As String = ""

            retVal = Products.BuildFabrics(sku)

            Return retVal
        End Function

        <Route("Products/PrintPage/{sku}")>
        Function PrintPage(ByVal sku As String) As ActionResult
            Dim retVal As New Products(sku)

            Return View("~/Views/Products/PrintPage.vbhtml", retVal)
        End Function

        <HttpPost>
        Function addToWishlist(ByVal sku As String) As String
            Dim retVal As String = ""

            retVal = Products.addToWishlist(sku)

            Return retVal
        End Function

        <HttpPost>
        Function deleteFromWishlist(ByVal sku As String) As String
            Dim retVal As String = ""

            retVal = Products.deleteFromWishlist(sku)

            Return retVal
        End Function

        Function showWishList() As ActionResult
            Dim retVal As New Products()

            Return View("~/Views/Products/ShowResults.vbhtml", retVal)
        End Function

        <Route("Products/CustomPrograms/{program}")>
        Function CustomPrograms() As ActionResult
            Return View("~/Views/Products/CustomPrograms.vbhtml")
        End Function

        <HttpPost>
        Function BuildCustomProgram(ByVal program As String) As String
            Dim retVal As String = ""

            retVal = CustomProgram.buildSourceProgram(program)

            Return retVal
        End Function

        Function GetItemImage(ByVal sku As String) As ContentResult
            Dim retVal As String = ""

            retVal = Products.GetItemImage(sku)

            Return Content(retVal)
        End Function

        Function DownloadHiResImage(ByVal ImageName As String) As ActionResult
            Try
                Response.ContentType = "image/jpeg"
                Response.AppendHeader("Content-Disposition", "attachment; filename=" & ImageName & "_hires.jpg")
                Response.TransmitFile(Server.MapPath("/Content/prod-images/" & ImageName & "_hires.jpg"))
                Response.Flush()
                Response.Close()
                Response.End()
            Catch ex As Exception
                Misc.LogError(ex)
            End Try

            Return Nothing
        End Function
        'Function GetSearchResultsMap(searchForm As Models.WhereToBuy.WhereToBuy) As ActionResult
        '    Try
        '        searchForm.GetSearchResults()
        '        Return View("_SearchResultMap", searchForm)
        '    Catch ex As Exception
        '        Misc.LogError(ex)
        '        Return New HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message)
        '    End Try
        'End Function
    End Class
End Namespace