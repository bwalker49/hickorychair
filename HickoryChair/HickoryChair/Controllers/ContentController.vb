﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class ContentController
        Inherits Controller

        ' GET: Content
        Function Index() As ActionResult
            Return View()
        End Function

        Function GetContentPage(ByVal page As String) As ActionResult
            Dim contentpage As New Content(page)

            Return PartialView("~/Views/Shared/_ContentPage.vbhtml", contentpage)
        End Function

        Function MadeForYou() As ActionResult
            Dim presspage As New PressRelease("MadeForYou")

            Return View("~/Views/Misc/MadeForYou.vbhtml", presspage)
        End Function

        Function PressRelease() As ActionResult
            Dim presspage As New PressRelease("PressRelease")

            Return View("~/Views/Misc/PressRelease.vbhtml", presspage)
        End Function
        'Function PressReleases(ByVal presstype As String) As ActionResult
        '    Dim presspage As New PressRelease(presstype)

        '    If presstype = "MadeForYou" Then
        '        Return View("~/Views/Misc/MadeForYou.vbhtml", presspage)
        '    ElseIf presstype = "PressRelease" Then
        '        Return View("~/Views/Misc/PressRelease.vbhtml", presspage)
        '    End If
        'End Function
    End Class
End Namespace