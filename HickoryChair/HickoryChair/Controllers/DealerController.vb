﻿Imports System.Net
Imports System.Web.Mvc
Imports Antlr.Runtime

Namespace Controllers
    Public Class DealerController
        Inherits Controller

        ' GET: Dealer
        Function Index() As ActionResult
            Try
                Return View(New Dealers)
            Catch ex As Exception
                Misc.LogError(ex)
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message)
            End Try
        End Function

        Function GetSearchResultsMap(searchForm As Dealers) As ActionResult
            Try
                searchForm.GetSearchResults()
                Return View("_SearchResultMap", searchForm)
            Catch ex As Exception
                Misc.LogError(ex)
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message)
            End Try
        End Function

        <Route("Dealer/HCShowroom/{showroom}")>
        Function GetShowroom(ByVal showroom As String) As ActionResult
            Dim shwrm As New Showroom(showroom, "")

            Return View("~/Views/Dealer/HCShowroom.vbhtml", shwrm)
        End Function

        <Route("Dealer/HCShowroom/{showroom}/{id}")>
        Function GetDesignerInfo(ByVal showroom As String, ByVal id As String) As ActionResult
            Dim shwrm As New Showroom(showroom, id)

            Return View("~/Views/Dealer/ShowroomDesigner.vbhtml", shwrm)
        End Function
    End Class
End Namespace