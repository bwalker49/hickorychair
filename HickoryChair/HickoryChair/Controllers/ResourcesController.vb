﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class ResourcesController
        Inherits Controller

        ' GET: Resource
        Function Index() As ActionResult
            Return View()
        End Function

        Function ArtistStudio() As ActionResult
            Return View("~/")
        End Function

        Function CustomersOwn() As ActionResult
            Return View("~/Views/Resources/CustomersOwn.vbhtml")
        End Function

        Function getFinishes(ByVal sku As String, ByVal CollectionNo As String, ByVal CollectionType As String) As ActionResult
            Dim finStr As String = Models.Finishes.ShowFinishesInTableResponsive(sku, CollectionNo, CollectionType)
            Return Json(finStr)
        End Function
        Function Finishes() As ActionResult
            Return View("~/Views/Resources/Finishes.vbhtml")
        End Function

        Function FinishPartial() As ActionResult
            Dim finishList As New Models.Finishes
            Return PartialView("~/Views/Shared/_FinishList.vbhtml", finishList)
        End Function
        <Route("Resources/FinishDetail/{finish}")>
        Function FinishDetail(finish) As ActionResult
            Dim findet As New FinishDetail(finish)
            Return View("~/Views/Resources/FinishDetail.vbhtml", findet)
        End Function

        <HttpGet>
        Function FabSearch() As ActionResult
            If Request("Type") = "Leather" Then
                Dim FabInModel As New Models.FabricSearch()
                FabInModel.TypeList_Selected = "Leather"

                Dim resultModel As New Models.FabricSearch(FabInModel)
                Return View(resultModel)
            ElseIf Request("Type") = "Trims" Then
                Dim FabInModel As New Models.FabricSearch()
                FabInModel.TypeList_Selected = "Trims"

                Dim resultModel As New Models.FabricSearch(FabInModel)
                Return View(resultModel)
            Else
                Dim FabModel As New Models.FabricSearch()
                Return View(FabModel)
            End If
        End Function

        <HttpPost>
        Function FabSearch(ByVal fabModel As Models.FabricSearch) As ActionResult

            Dim resultModel As New Models.FabricSearch(fabModel)
            Return View(resultModel)
        End Function

        <HttpGet>
        Function FabDetail(ByVal SKU As String) As ActionResult

            Dim fabDetModel As New Models.FabricDetail(SKU)

            Return View(fabDetModel)
        End Function

        Function Leather() As ActionResult
            Return View("~/Views/Resources/Leather.vbhtml")
        End Function

        Function Nailhead() As ActionResult
            Dim nt As New Models.NailTrim()

            Return View(nt)
        End Function

        Function OnlineDesignStudio() As ActionResult
            Return View("~/Views/Resources/OnlineDesignStudio.vbhtml")
        End Function

        Function PersonalPreferences() As ActionResult
            Return View("~/Views/Resources/PersonalPreferences.vbhtml")
        End Function

        Function StockingHardware() As ActionResult
            Return View("~/Views/Resources/StockingHardware.vbhtml")
        End Function

        Function ImageLibrary() As ActionResult
            Return View("~/Views/Resources/ImageLibrary.vbhtml")
        End Function

        Function ShowDesigner(ByVal DesignerID As String) As ActionResult
            Dim retVal As New Designer(DesignerID)

            Return View("~/Views/Resources/Designer.vbhtml", retVal)
        End Function
    End Class
End Namespace