﻿Imports System.Web.Optimization
Imports SAP.Middleware.Connector
Imports WebSupergoo.ABCpdf9

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
        RfcDestinationManager.RegisterDestinationConfiguration(New SAP_CNO_3)
        XSettings.InstallRedistributionLicense("X/VKS08wmMhAtn4jNv3DOcikae8bCdcYlqznwb2yVhxskQjDfDm +CqofKHk / hAUJCckx9iNHpUQVzqgiTb5/y00mZ2SfBnlV42ERQ8qoKE7g5QnEoIE6faKxXECzZmgzRqd0V89ZskpwdhxxryqQbGhYcSuWVW7()qn7QV8VMuS4at5Lsu6Bfo6mLRsaoFryNKbx8O6Xt()YrBupGhB4LZReiDJyoVLpA +sCPeb7VxPkTYDtFVNI")
    End Sub

    Protected Sub Application_BeginRequest()
        If HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("default.aspx") Then
            handle301Redirect("/")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("raybooth") Then
            handle301Redirect("/Resources/ShowDesigner?DesignerID=RB")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("hable") Then
            handle301Redirect("/Resources/ShowDesigner?DesignerID=HC")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("mariettehimesgomez") Then
            handle301Redirect("/Resources/ShowDesigner?DesignerID=MHG")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("davidphoenix") Then
            handle301Redirect("/Resources/ShowDesigner?DesignerID=DP")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("suzannekasler") Then
            handle301Redirect("/Resources/ShowDesigner?DesignerID=SK")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("skiprumley") Then
            handle301Redirect("/Resources/ShowDesigner?DesignerID=SR")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/atlanta") Then
            handle301Redirect("/Dealer/HCShowroom/ATLANTA")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/buckhead") Then
            handle301Redirect("/Dealer/HCShowroom/BUCKHEAD")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/chicago") Then
            handle301Redirect("/Dealer/HCShowroom/CHICAGO")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/dallas") Then
            handle301Redirect("/Dealer/HCShowroom/DALLAS")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/denver") Then
            handle301Redirect("/Dealer/HCShowroom/DENVER")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/houston") Then
            handle301Redirect("/Dealer/HCShowroom/HOUSTON")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/minneapolis") Then
            handle301Redirect("/Dealer/HCShowroom/MINNEAPOLIS")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/naples") Then
            handle301Redirect("/Dealer/HCShowroom/NAPLES")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("neworleans") Then
            handle301Redirect("/Dealer/HCShowroom/NEW_ORLEANS")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("newyork") Then
            handle301Redirect("/Dealer/HCShowroom/NEW_YORK")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("palmbeach") Then
            handle301Redirect("/Dealer/HCShowroom/PALM_BEACH")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/sarasota") Then
            handle301Redirect("/Dealer/HCShowroom/SARASOTA")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith(".com/troy") Then
            handle301Redirect("/Dealer/HCShowroom/TROY")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("washingtondc") Then
            handle301Redirect("/Dealer/HCShowroom/WASHINGTON_DC")
        ElseIf HttpContext.Current.Request.Url.ToString.ToLower.EndsWith("pearson") Then
            handle301Redirect("/Products/ShowResults?CollectionID=FP&SearchName=Pearson®")
        End If
    End Sub

    Private Sub handle301Redirect(ByVal redirect As String)
        Dim qs As String = ""
        If Request.QueryString.Count > 0 Then
            qs = "?" & Request.QueryString.ToString()
        End If

        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Status = "301 Moved Permanently"
        HttpContext.Current.Response.AddHeader("Location", redirect & qs)
    End Sub
End Class
